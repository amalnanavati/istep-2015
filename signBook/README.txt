SignBook README

How to start SignBook Locally

On Windows: Double click 'run' (the application 'run.exe').
On Macs: Double click 'runOnMacs.command'.  Make sure you have Chrome installed.  It may open an extra window, just close that one.

Troubleshooting - if SignBook doesn't work locally

1. Make sure you have the browser Chrome installed (should only matter on Macs).
2. Make sure the www folder in signBook has a db folder with five empty folders, except for the objs folder with two default json files (that is, if you are using a fresh, blank dictionary).
3. See if you can gain information from any errors that pop up, ask someone who's familiar with computers, and if it still is giving you issues, email one of the Contact emails below.

Online Demo

erikpintar.com/signBookDemo
(use on Chrome!)
(somewhat compatible with Firefox too)

Basic Info

SignBook was developed by Erik Pintar (erikpintar.com),
for iSTEP 2015 (istep2015.techbridgeworld.org),
in conjunction with TechBridgeWorld (techbridgeworld.org),
to assist the deaf and hard of hearing school at the Mathru Center for the Differently-Abled in Bangalore, India, which is run by the Mathru Educational Trust for the Blind (mathrublindschool.org).

Contact

If you are interested in learning more about the project or continuing development, contact TechBridgeWorld (info@techbridgeworld.org) or Erik Pintar (elpintar@gmail.com).

User Guide

For more on how to use SignBook, see 'SignBook User Guide.pdf'.

Developer Guide

For more on how to continue development of SignBook, in the 'www' folder (where you would be coding) see the file 'documentation.txt'.

Open Source Libraries Used In SignBook

JQuery (jquery.com)
	for better Javascript
Angular (angularjs.org)
	for way easier web app making
FontAwesome (fortawesome.github.io/Font-Awesome/)
	for great and scalable icons
RecordRTC (recordrtc.org)
	also jsgif (github.com/antimatter15/jsgif)
	     omggif (github.com/deanm/omggif)
	for video/image capture and gif/webm creation
Dropzone.js (dropzonejs.com)
	for image uploading
nestedSortable (github.com/ilikenwf/nestedSortable)
	for drag and drop in the Rearrange feature
angular-treeRepeat (github.com/tchatel/angular-treeRepeat)
	for automatically generating the folder sidebar list from a json object via angular directives
jquery.autosize.input (github.com/MartinF/jQuery.Autosize.Input)
	for nice automatically resizing text boxes
PHP Desktop (code.google.com/p/phpdesktop/)
	for making it all work locally on Windows and look like an application (literally saved this project - many thanks!!!)

