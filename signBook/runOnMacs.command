#!/bin/sh
cd `dirname $0` # change to current directory
cd www # and then inside www folder
sleepTime=1
sleep $sleepTime && # sleep to give localhost server time to start
# open google chrome to localhost
/usr/bin/open -a "/Applications/Google Chrome.app" 'http://localhost:8000' &
#start localhost server
php -S localhost:8000/

# check out http://sveinbjorn.org/platypus to make this an application