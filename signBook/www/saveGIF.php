<?php
  // saveGIF.php
  // saves a gif image from a dataURL and filename
  // requires php5
  $upload_dir = $_POST['uploadDir'];
  $filetype = '.gif';
  $img = $_POST['imgBase64'];
  $img = str_replace('data:image/gif;base64,', '', $img);
  $img = str_replace(' ', '+', $img);
  $data = base64_decode($img);
  $filename = $_POST['filename'];
  $filepath = $upload_dir . $filename . $filetype;
  
  $i = 2; // A counter for the tail to append to the filename
  $tail = "";
  while (file_exists($filepath)) {
    $tail = (string) $i;
    $filepath = $upload_dir . $filename . $tail . $filetype;
    $i++;
  }

  $success = file_put_contents($filepath, $data);
  //print $success ? $file : 'Unable to save the file.';
  echo json_encode(array('filename' => $filename . $tail));

?>