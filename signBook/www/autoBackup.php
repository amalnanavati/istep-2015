<?php

date_default_timezone_set("Asia/Kolkata"); 
$datetime = new DateTime();
$timestampNow = $datetime->getTimestamp();
$day = 60*60*24;

$ds = DIRECTORY_SEPARATOR;
$objFolder = 'db' . $ds . 'objs' . $ds;
$objDir = dirname(__FILE__) . $ds . $objFolder;
$backupFolder = 'db' . $ds . 'backupObjs' . $ds;
$backupDir = dirname(__FILE__) . $ds . $backupFolder;
$dirIter = new DirectoryIterator($backupDir);

$recentTimestamp = array(
    "entries" => 0,
    "folders" => 0
);

/* find the most recent timestamp from the backup files */
foreach ($dirIter as $fileinfo) {
  if (!$fileinfo->isDot()) {
    $curFilename = $fileinfo->getFilename();
    $splitParts = explode("-", $curFilename);
    $jsonName = $splitParts[0];
    $oldTimestamp = $splitParts[1];
    $curNewest = $recentTimestamp[$jsonName];
    if ($oldTimestamp > $curNewest) {
      $recentTimestamp[$jsonName] = $oldTimestamp;
    }
  }
}

/* backup files if we need to */
$success = true;
$count = 0;
foreach ($recentTimestamp as $jsonName => $oldTimestamp) {
  /* backup once a day */
  if ($oldTimestamp + $day < $timestampNow) {
    $backupFilename = $jsonName . "-" . $timestampNow . "-.json";
    $backupPath = $backupDir . $backupFilename;
    $sourceFilename = $jsonName . ".json";
    $sourcePath = $objDir . $sourceFilename;
    //echo "\ncopying:\n" . $sourcePath . "\n to \n" . $backupPath . "\n ";
    /* make the backup copy here */
    $success = $success && copy($sourcePath, $backupPath);
    $count++;
  }
}

if ($success) {
  echo $count;
}
else {
  echo -1;
}

?>