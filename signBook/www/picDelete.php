<?php
  $ds = DIRECTORY_SEPARATOR;
  $storeFolder = 'db/imgs';
  $targetDirPath = dirname( __FILE__ ) . $ds . $storeFolder . $ds;
  $filename = $_POST['filename'];
  $targetFilePath = $targetDirPath . $filename;

  unlink($targetFilePath);
  echo json_encode(array('filename' => $filename));
?>