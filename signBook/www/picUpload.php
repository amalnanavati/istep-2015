<?php

    $ds = DIRECTORY_SEPARATOR;
    $storeFolder = 'db' . $ds . 'imgs';
    $printStr = "";

if (!empty($_FILES)) {

  $filename = $_FILES['file']['name'];
  $tempFile = $_FILES['file']['tmp_name'];
    $targetDirPath = dirname( __FILE__ ) . $ds . $storeFolder . $ds;
  $targetFilePath = $targetDirPath . $_FILES['file']['name'];

    $i = 2; // A counter for the tail to append to the filename
    while(file_exists($targetFilePath)) {
        $printStr = $printStr . "file_exists:" . $targetFilePath;
    $tail = (string) $i;
    $fileinfos = pathinfo($targetFilePath);
    if ($i > 2) { 
      $previous_tail = (string) $i-1;
      $filename = str_replace($previous_tail, "", $filename);
    }
    $filename = str_replace('.' . $fileinfos['extension'], "", $filename);
    $filename = $filename . $tail . '.' . $fileinfos['extension'];
    $targetFilePath = $targetDirPath . $filename;
    $i++;
    }

    // really nice exception class
    class UploadException extends Exception 
    { 
    public function __construct($code) { 
        $message = $this->codeToMessage($code); 
        parent::__construct($message, $code); 
    } 

    private function codeToMessage($code) 
    { 
        switch ($code) { 
            case UPLOAD_ERR_INI_SIZE: 
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = "The uploaded file was only partially uploaded"; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = "No file was uploaded"; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = "Missing a temporary folder"; 
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = "Failed to write file to disk"; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = "File upload stopped by extension"; 
                break; 

            default: 
                $message = "Unknown upload error"; 
                break; 
        } 
        return $message; 
    } 
    } 

    $success = move_uploaded_file($tempFile, $targetFilePath);
    $printStr = $printStr . ($success ? "successfully uploaded picture: " . $targetFilePath : 'Unable to save the file: '.$targetFilePath . "  ".$targetDirPath."  ".$tempFile."  ".$filename);
  
    // // Use 
    // if ($_FILES['file']['error'] === UPLOAD_ERR_OK) { 
 //     echo $filename;
    // } else { 
    //  throw new UploadException($_FILES['file']['error']); 
    // }

    echo json_encode(array('filename' => $filename, 
        'error' => $_FILES['file']['error']));

}

?>