// modified from https://www.webrtc-experiment.com/RecordRTC
// Erik Pintar, June 2015

// TODO - this is mostly NOT angular, so should be changed

var app = angular.module('createContent', []);

app.controller('CreateController', function($scope) {

  var vm = $scope;

  // query params
  vm.params = getQueryParams(document.location.search);

  vm.vidMode = (vm.params.vidMode === "true");
  vm.areCapturing = true;
  vm.areEditing = false;
  // mutex to save only one gif at a time
  vm.savingGif = false;

  vm.makingGif = !vm.vidMode;

  var GifAPI = new MyGifAPI(vm.makingGif);

  function getByID(id) {
    return document.getElementById(id);
  }

  var videoElem = $("#video-record"),
    editorCanvas = $("#editor-canvas");
    cameraOnBtn = $('#camera-btn'),
    recordBtn = $('#record-btn'),
    photoBtn = $('#take-photo-btn'),
    overlayText = $("#overlay-text"),
    stopBtn = $('#stop-btn'),
    spinnerBtn = $('#spinner-btn'),
    rerecordBtn = $('#re-record-btn'),
    playBtn = $('#play-btn'),
    pauseBtn = $('#pause-btn'),
    checkBtn = $('#check-btn'),
    buttonBar = $("#button-bar"),
    editorSlider = $('#editor-slider'),
    wordInputDOM = $('#word-input'),
    backBtn = $("#back-button");

  var canvasWidth_input = 320,
    canvasHeight_input = 240;
  var frameRate = 100; // ms

  var leftStartFrame = 0;
  var rightEndFrame = 1;
  var curFrame = 0;
  var isPlaying = true;
  var setUpRecorder = false;
  var isRecording = false;
  var recordTimeLimit = 30000; // 30 sec
  var warningTimeLeft = 5000; // 5 sec before, warn

  var gifPreview = getByID('gif-preview-img');

  function nodeIdToEntry(nodeId) {
    // what separates an entry id and the copy number of it
    // this is weird because id name are finicky with nestedSortable
    // has to be "category_name" with category and name only being [A-Za-z0-9]
    nodeIdSep = "0v";
    // folder entry id is like "entry_id_with_underscores-12"
    var dashIndex = nodeId.indexOf(nodeIdSep);
    if (dashIndex !== -1) {
      return nodeId.substring(0, dashIndex);
    }
    else return nodeId;
  }

  // see if there's a URL parameter for the current word
  var params = getQueryParams(document.location.search);
  var paramNodeId = params.nodeId;
  var entryId = nodeIdToEntry(paramNodeId);
  var paramEntryEng = params.eng;
  if (paramNodeId) {
    vm.wordInput = paramEntryEng;
  }
  var curNodeId = paramNodeId;

  /* CHECK BROWSER COMPATIBILITY */

  // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
  var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
  // At least Safari 3+: "[object HTMLElementConstructor]"
  var isSafari = Object.prototype.toString
           .call(window.HTMLElement).indexOf('Constructor') > 0;
  var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
  var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

  if (isSafari || isIE) {
    setHelpText("This webpage will not work in your browser. <br>"+
        "Please visit this site using Google Chrome or Firefox.");
    var crossOff = $('<i class="fa fa-ban fa-stack-2x text-danger red-font" '+
             'style="top: 0.15em"></i>');
    cameraOnBtn.addClass("fa-stack fa-2x");
    $("#camera-btn i").removeClass("fa-5x").addClass("fa-stack-1x").css("top", "0.3em");
    cameraOnBtn.append(crossOff);
  }

  /* HELPER FUNCTIONS */

  function setHelpText(helpStr) {
    $("#help-text").html(helpStr);
  }

  function setOverlayText(textStr) {
    videoElem.addClass("opacity-half");
    editorCanvas.addClass("opacity-half");
    overlayText.show();
    overlayText.html(textStr);
  }

  function hideOverlayText() {
    videoElem.removeClass("opacity-half");
    editorCanvas.removeClass("opacity-half");
    overlayText.hide();
  }

  $scope.$watch('wordInput', function() {
    hideOverlayText();
    if (vm.areEditing) checkBtn.show();
    if (!vm.areCapturing)
      vm.areEditing = true;
  });

  function updateShownSliderValues() {
    function roundToOneDig(num) {
      return +(Math.round(num + "e+1")  + "e-1");
    }
    function convertToMs(frame) {
      var t = frame/(1000/frameRate);
      t = roundToOneDig(t);
      return t.toString() + "s";
    }
    $(".ui-slider-handle").each(function(i) {
      if (i === 0) {
        var leftLabel = $("<span>");
        leftLabel.attr("id", "left-slider-label");
        leftLabel.html(convertToMs(leftStartFrame));
        $(this).empty();
        $(this).append(leftLabel);
      }
      else if (i === 1) {
        var rightLabel = $("<span>");
        rightLabel.attr("id", "right-slider-label");
        rightLabel.html(convertToMs(rightEndFrame));
        $(this).empty();
        $(this).append(rightLabel);
      }
    });
  }

  function resetShownSliderValues() {
    // change the slider max to numFrames-1
    var numFrames = GifAPI.getNumFrames();
    $("#editor-slider").slider("option", "min", 0);
    $("#editor-slider").slider("option", "max", numFrames-1);
    $("#editor-slider").slider("values", [0, numFrames-1] );
    leftStartFrame = 0;
    rightEndFrame = numFrames-1;
    updateShownSliderValues();
  }

  // make the gray bar stay in place when right slider is moved
  // by playing around with margin-right
  function updatePlayBar(newL, newR) {
    var playBar = $("#play-bar");
    if (playBar.width() > 0) {
      var totalFrameGaps = GifAPI.getNumFrames() - 1;
      var totalW = editorSlider.width();
      var wPerFrameGap = totalW / totalFrameGaps;
      var rMarginMove = newR - rightEndFrame;
      var curMarginR = parseFloat(playBar.css("margin-right"));
      var newMarginR = curMarginR + rMarginMove*wPerFrameGap;
      playBar.css("margin-right", newMarginR);
    }
  }

  /* BUTTON FUNCTIONS */

  cameraOnBtn.click(function(event) {
    setHelpText("Allow the camera to be used.");
    try {
      GifAPI.turnCameraOn(canvasWidth_input, canvasHeight_input,
                          function(newRecorder) {
        if (newRecorder) {
          console.log("newRecorder set up");
          setUpRecorder = true;
          buttonBar.show();
          recordBtn.show();
          photoBtn.show();
          cameraOnBtn.hide();
          if (vm.vidMode) {
            setOverlayText("Press record to begin...");
            setHelpText("Press record "+
              "and then sign a word or phrase.");
          }
          else {
            setOverlayText("Click the camera to take a photo!");
            setHelpText("Click the camera "+
              "and then you will have three seconds "+
              "before a picture is taken.");
          }
        }
        else {
          console.log("ERROR: recorder not set up");
        }
      });
    }
    catch (e) {
      setHelpText("Video capture does not work in your browser. <br>"+
        "Please visit this site using Google Chrome or Firefox.");
    }
    event.preventDefault();
  });

  recordBtn.click(function() {
    if (setUpRecorder) {
      // counts down from i to 0 then calls a callback
      function countDownForVid(i, callback) {
        if (i > 0) {
          overlayText.addClass("big-overlay-text");
          var newText = "";
          if (i === 3) newText = "Prepare";
          if (i === 2) newText = "Ready";
          if (i === 1) newText = "Go";
          overlayText.html(newText);
          setTimeout(function() {
            countDownForVid(i-1, callback);
          }, 1000);
        }
        else {
          overlayText.removeClass("big-overlay-text");
          hideOverlayText();
          if (callback) callback();
        }
      }
      countDownForVid(2, function() {
        // re-assign recorder in case it started over
        GifAPI.startRecording();
        recordBtn.hide();
        stopBtn.show();
        hideOverlayText();
        setHelpText("Press stop when you are done.");
        isRecording = true;
        // warn about recording time limit if we get there
        setTimeout(function() {
          if (isRecording) {
            setOverlayText("Finish soon!");
          }
        }, recordTimeLimit - warningTimeLeft);
        // when time is up, force stop recording
        setTimeout(function() {
          if (isRecording) {
            stopBtn.click();
          }
        }, recordTimeLimit);
      });
    }
  });

  function stopGif() {
    spinnerBtn.show();
    GifAPI.initGif(function() {
      // set up the slider
      resetShownSliderValues();
      // update the DOM
      spinnerBtn.hide();
      hideOverlayText();
      rerecordBtn.show();
      playBtn.show();
      checkBtn.show();
      editorSlider.show();
      wordInputDOM.show();
      editorCanvas.show();
      videoElem.hide();
      setHelpText("Trim your video with the sliders,<br>"+
        "then press save to download your gif.");
      // turn off camera
      GifAPI.turnCameraOff();
      vm.areCapturing = false;
      vm.areEditing = true;
      $scope.$apply();
    });
  }

  function stopWebm() {
    // update the DOM
    hideOverlayText();
    rerecordBtn.show();
    checkBtn.show();
    wordInputDOM.show();
    setHelpText("Press check to save your video,<br>"+
      "or X to record again.");
    // turn off camera
    GifAPI.turnCameraOff();
    vm.areCapturing = false;
    vm.areEditing = true;
    $scope.$apply();
  }

  stopBtn.click(function() {
    if (setUpRecorder) {
      stopBtn.hide();
      setOverlayText("Please wait...");
      setHelpText("Your video is processing...");
      isRecording = false;
      GifAPI.stopRecording(function(url) {
        if (vm.makingGif) stopGif();
        else stopWebm();
      });
    }
  });

  photoBtn.click(function() {
    if (setUpRecorder) {
      // changes the background to be all white for 50 ms
      function flashBody() {
        var oldBackgroundColor = $("body").css("background-color");
        $("body").css("background-color", "white");
        setTimeout(function() {
          $("body").css("background-color", oldBackgroundColor);
        }, 1000);
      }
      // counts down from i to 0 then calls a callback
      function countDown(i, callback) {
        if (i === 1) flashBody();
        if (i > 0) {
          overlayText.addClass("big-overlay-text");
          overlayText.html(i.toString());
          setTimeout(function() {
            countDown(i-1, callback);
          }, 1000);
        }
        else {
          overlayText.removeClass("big-overlay-text");
          hideOverlayText();
          if (callback) callback();
        }
      }
      // re-assign recorder in case it started over
      photoBtn.attr("disabled", true);
      videoElem.removeClass("opacity-half");
      setHelpText("Get ready!");
      GifAPI.startRecording(function() {
        countDown(3, function() {
          GifAPI.stopRecording(function(url, storedFrameObjList) {
            GifAPI.initGif(function() {
              // set up the slider
              resetShownSliderValues();
              // update the DOM
              photoBtn.hide();
              rerecordBtn.show();
              checkBtn.show();
              wordInputDOM.show();
              editorCanvas.show();
              videoElem.hide();
              setHelpText("Press check to save your picture,<br>"+
                "or press X to take another picture.");
              // turn off camera
              GifAPI.turnCameraOff();
              GifAPI.showFrame(GifAPI.getNumFrames()-1);
              vm.areCapturing = false;
              vm.areEditing = true;
              $scope.$apply();
            });
          });
        });
      });
    }
  });

  playBtn.click(function() {
    function updatePlayBar() {
      var totalTime = rightEndFrame - leftStartFrame;
      var timeLeft = rightEndFrame - curFrame;
      var timeLeftRatio = timeLeft / totalTime;
      var fullW = sliderRange.width();
      var w = fullW * timeLeftRatio;
      $("#play-bar").width(parseInt(w));
    }
    function playAFrame() {
      GifAPI.showFrame(curFrame);
      updatePlayBar();
      if (curFrame < rightEndFrame &&
        isPlaying) {
        setTimeout(function() {
          curFrame = curFrame + 1;
          playAFrame();
        }, frameRate);
      }
      else {
        playBtn.show();
        pauseBtn.hide();
        if (playBar.width() === 0) {
          sliderRange.empty();
        }
      }
    }
    curFrame = leftStartFrame;
    isPlaying = true;
    pauseBtn.show();
    playBtn.hide();
    var sliderRange = $("#editor-slider .ui-slider-range");
    sliderRange.empty();
    var playBar = $("<div>").attr("id", "play-bar");
    sliderRange.append(playBar);
    playAFrame();
  });

  pauseBtn.click(function() {
    isPlaying = false;
    playBtn.show();
    pauseBtn.hide();
  });

  rerecordBtn.click(function() {
    // refresh page with same parameters
    updatePage({nodeId: params.nodeId, 
                eng: params.eng,
                editMode: true,
                vidMode: vm.vidMode}, 
                '/createVid.html');
  });

  editorSlider.slider({
    range: true,
    //disabled: true,
    min: 0,
    max: 100,
    values: [0, 100],
    slide: function( event, ui ) {
      hideOverlayText();
      vm.areEditing = true;
      if (vm.areEditing) checkBtn.show();
      updatePlayBar(ui.values[0], ui.values[1]);
      leftStartFrame = ui.values[0];
      rightEndFrame = ui.values[1];
      updateShownSliderValues()
      GifAPI.showFrame(ui.value);
      curFrame = ui.value;
      $scope.$apply();
    }
  });

  /* SAVING GIFS */

  function getEntriesAndFolders(callback) {
    var dbAPI = MyDbAPI();
    var entriesFile = "entries.json";
    var foldersFile = "folders.json";
    dbAPI.readJsonFile(entriesFile, function(entriesResult) {
      dbAPI.readJsonFile(foldersFile, function(foldersResult) {
        if (callback) callback(entriesResult, foldersResult);
      });
    });
  }

  function writeEntriesAndFolders(entries, folders, callback) {
    var dbAPI = MyDbAPI();
    var entriesFile = "entries.json";
    var foldersFile = "folders.json";
    dbAPI.writeJsonFile(entriesFile, entries, function() {
      dbAPI.writeJsonFile(foldersFile, folders, function() {
        if (callback) callback();
      });
    });
  }

  function addEntry(safeGifName, inputEntryId, entries, folders, callback) {
    var gifFileName = safeGifName + ".gif";
    if (!vm.makingGif) gifFileName = safeGifName + ".webm";
    var isNewEntry = true;
    // if the url parameter word is the one we want
    if (vm.wordInput === paramEntryEng) {
      isNewEntry = false;
      var entryObj = entries[entryId];
      var id = entryId;
      var eng = vm.wordInput;
    }
    // if we are doing a random new word
    else {
      // use gif name as new entry id
      // already checked prior that it is not taken
      var entryObj = entries[inputEntryId];
      var id = inputEntryId;
      var eng = vm.wordInput;
      curNodeId = id;
    }
    // create new entry
    if (isNewEntry || !entryObj) {
      if (vm.vidMode) {
        entries[id] = {
          english: [eng],
          videos: [gifFileName],
          images: []
        };
      }
      else {
        entries[id] = {
          english: [eng],
          videos: [],
          images: [gifFileName]
        };
      }
    }
    // entry already exists, update entry object
    else {
      var entry = entries[id];
      if (entry.english.indexOf(eng) === -1)
        entry.english.push(eng);
      if (vm.vidMode)
        entry.videos.push(gifFileName);
      else
        entry.images.push(gifFileName);
    }
    // add word to folder scheme if no entry exists yet
    var node = findNodeWithId(id, folders);
    // need to create new node
    if (!node) {
      var newWordsFolderObj = findNodeWithId("newWords", folders);
      if (!newWordsFolderObj) {
        console.error("a folder called 'New Words' must exist!");
        return;
      }
      if (!newWordsFolderObj.children) {
        newWordsFolderObj.children = [];
      }
      var newWordData = {id: id, name: eng};
      newWordsFolderObj.children.push(newWordData);
    }
    if (callback) callback(isNewEntry);
  }

  // assumes you already read from the db
  // and want to add a new gif of name safeGifName
  // to entry of inputEntryId
  function updateDb(safeGifName, inputEntryId, entries, folders, callback) {
    addEntry(safeGifName, inputEntryId, entries, folders, 
         function(isNewEntry) {
      writeEntriesAndFolders(entries, folders, 
                   function() {
        console.log("updated database!");
        console.log("entries:", entries);
        console.log("folders:", folders);
        if (callback) callback(isNewEntry);
      });
    });
  }

  function saveGifType(safeInput, entries, folders, saveActionType) {
    var saveDir = 'db/gifs/';
    var itemStr = vm.vidMode ? "video" : "picture";
    // grab trim positions
    var startFrame = $("#editor-slider").slider("values", 0);
    var endFrame = $("#editor-slider").slider("values", 1);
    // for pic only get one frame
    if (!vm.vidMode) {
      startFrame = GifAPI.getNumFrames()-1;
      endFrame = startFrame;
      saveDir = 'db/imgs/';
    }
    // trim and save the gif
    try {
      GifAPI.trimGif(startFrame, endFrame, function(trimEditorOut) {
        console.log("trimmed gif", trimEditorOut);
        GifAPI.saveGif(trimEditorOut, saveDir, safeInput, 
                      function(possiblyNewFilename) {
          console.log("saved gif", possiblyNewFilename);
          safeGifName = possiblyNewFilename;
          updateDb(safeGifName, safeInput, entries, folders,
                  function(isNewEntry) {
            console.log("updated db. entries:", entries, "folders", folders);
            var word = vm.wordInput;
            if (isNewEntry && saveActionType !== "addToExistingEntry") {
              setHelpText("New word added for '" + word + "' in "+
                "the dictionary.<br>" +
                "<br>Go back to the dictionary to see your " +
                "changes,<br>or create more words!");
              setOverlayText("Saved new "+itemStr+"<br>"+
                "and created a new entry for<br>'"+word+"'!");
            }
            else {
              setHelpText("Added "+itemStr+" for '" + word + "'.<br>" +
                "<br>Go back to the dictionary to see your " +
                "changes,<br>or create more "+itemStr+"s.");
              setOverlayText("Saved new "+itemStr+" for '" + word + "'!");
            }
            vm.savingGif = false;
            vm.areEditing = false;
            $scope.$apply();
          });
        });
      }, false);
    }
    catch (e) {
      console.error(e);
      vm.savingGif = false;
      vm.areEditing = false;
    }
  }

  function saveWebmType(safeInput, entries, folders, saveActionType) {
    var saveDir = 'db/webm/';
    var itemStr = vm.vidMode ? "video" : "picture";
    // trim and save the gif
    try {
      var fileName = safeInput+Date.now();
      var fileFullName = fileName + ".webm";
      var blob = GifAPI.getBlobFromRecorder();
      //GifAPI.saveWebm(blob, saveDir, fileName, function() {
      var formData = new FormData();
      formData.append('video-filename', fileFullName);
      formData.append('video-blob', blob);
      function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
          if (request.readyState == 4 && request.status == 200) {
            callback(location.href + request.responseText);
          }
        };
        request.open('POST', url);
        request.send(data);
      }
      xhr('saveWebm.php', formData, function (response) {
        var fileSaved = response.split(' ')[1] === 'OK';
        if (!fileSaved) {
          alert("The video was too long to save. "+
            "Try making a shorter video.");
          return;
        }
        else {
          console.log("saved gif", fileFullName);
          updateDb(fileName, safeInput, entries, folders,
                  function(isNewEntry) {
            console.log("updated db. entries:", entries, "folders", folders);
            var word = vm.wordInput;
            if (isNewEntry && saveActionType !== "addToExistingEntry") {
              setHelpText("New word added for '" + word + "' in "+
                "the dictionary.<br>" +
                "<br>Go back to the dictionary to see your " +
                "changes,<br>or create more words!");
              setOverlayText("Saved new "+itemStr+"<br>"+
                "and created a new entry for<br>'"+word+"'!");
            }
            else {
              setHelpText("Added "+itemStr+" for '" + word + "'.<br>" +
                "<br>Go back to the dictionary to see your " +
                "changes,<br>or create more "+itemStr+"s.");
              setOverlayText("Saved new "+itemStr+" for '" + word + "'!");
            }
          });
        }
        vm.savingGif = false;
        vm.areEditing = false;
        $scope.$apply();
      });
    }
    catch (e) {
      console.error(e);
      vm.savingGif = false;
      vm.areEditing = false;
    }
  }

  // trims, then saves the gif, then updates the database
  checkBtn.click(function() {
    // check if file has a name
    if (!vm.wordInput || vm.wordInput.length === 0) {
      alert("Please enter the word.");
      return;
    }
    var safeInput = makeWordHtmlSafe(vm.wordInput);
    getEntriesAndFolders(function(entries, folders) {
      // if we are creating a new entry
      // or adding to an existing entry,
      // (not adding to the intended entry)
      // see which one it is
      // and make sure it's not a folder name
      var saveActionType = "";
      var itemStr = vm.vidMode ? "video" : "picture";
      if (vm.wordInput !== paramEntryEng) {
        // check if propsed entryid is a folder name (nodeId) already!
        if (nodeIdExists(safeInput, folders) && 
            !entryIdExists(safeInput, entries)) {
          var alertStr = vm.wordInput + " already exists as a folder "+
            "name, please choose another name.";
          alert(alertStr);
          return;
        }
        else if (!entryIdExists(safeInput, entries)) {
          // creating a new entry
          var itemStr = vm.vidMode ? "video" : "picture";
          var confirmStr = "This will create a new entry for '"+
              vm.wordInput + "' with your new " + itemStr + "."; 
          var confirmCreateNewEntry = confirm(confirmStr);
          if (confirmCreateNewEntry) {
            saveActionType = "createNewEntry";
          }
          else {
            return;
          }
        }
        else {
          // adding to an existing entry
          var confirmStr = "This will add your " + itemStr +
              " to the existing entry for " + vm.wordInput + "."; 
          var confirmAddToExistingEntry = confirm(confirmStr);
          if (confirmAddToExistingEntry) {
            saveActionType = "addToExistingEntry";
          }
          else {
            return;
          }
        }
      }
      else {
        saveActionType = "addToCurEntry";
      }
      if (vm.savingGif) return;
      vm.savingGif = true;
      vm.areEditing = false;
      checkBtn.hide();

      if (vm.makingGif) {
        saveGifType(safeInput, entries, folders, saveActionType);
      }
      else {
        saveWebmType(safeInput, entries, folders, saveActionType);
      }
    });
  });

  // go back to the dictionary viewer
  backBtn.click(function() {
    if (!vm.savingGif)
      updatePage({nodeId: curNodeId, editMode: true}, "/");
    else
      alert("Please wait.  Saving still in progress.");
  });

});
