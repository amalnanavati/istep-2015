function getParamStr(paramObj) {
  var l = window.location;

  /* build params */
  var params = {};        
  // for including old params
  // var x = /(?:\??)([^=&?]+)=?([^&?]*)/g;        
  // var s = l.search;
  // for(var r = x.exec(s); r; r = x.exec(s))
  // {
  //     r[1] = decodeURIComponent(r[1]);
  //     if (!r[2]) r[2] = '%%';
  //     params[r[1]] = r[2];
  // }

  /* set param */
  for (var name in paramObj) {
    var value = paramObj[name];
    console.log(name, value, paramObj);
    params[name] = encodeURIComponent(value);
  }

  /* build search */
  var search = [];
  for (var i in params) {
    var p = encodeURIComponent(i);
    var v = params[i];
    if (v != '%%') p += '=' + v;
    search.push(p);
  }
  search = search.join('&');

  /* execute search */
  return search;
}

function updatePage(paramObj, htmlPage) {
  if (!htmlPage) htmlPage = window.location.pathname;
  var params = getParamStr(paramObj);
  if (params) params = "?" + params;
  // navigates to page
  window.location.href = (window.location.origin + 
                          htmlPage + params);
}

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}
//var query = getQueryParams(document.location.search);

function makeWordHtmlSafe(word) {
  var replaceChar = "";
  var newWord = word.replace(/[^A-Za-z0-9]/gi, replaceChar).toLowerCase();
  return newWord;
}

// traverses a list of node objects with ids and children arrays
// looking for an id match
// returns a node object
function findNodeWithId(id, folderObj) {
  var folderList = folderObj.children;
  if (folderObj.id === id) return folderObj;
  for (var i = 0; i < folderList.length; i++) {
    var curNode = folderList[i];
    if (curNode.id == id) {
      return curNode;
    }
    else if (curNode.isFolder && curNode.children && 
             curNode.children.length > 0) {
      var result = findNodeWithId(id, curNode);
      if (result) return result;
    }
  }
  return null;
}

// traverses a list of node objects with ids and children arrays
// looking for an id match
// returns a node object
function findParentNodeOfNodeWithId(id, folderObj) {
  var folderList = folderObj.children;
  for (var i = 0; i < folderList.length; i++) {
    var curNode = folderList[i];
    if (curNode.id == id) {
      return folderObj;
    }
    else if (curNode.isFolder && curNode.children && 
             curNode.children.length > 0) {
      var result = findParentNodeOfNodeWithId(id, curNode);
      if (result !== null) return result;
    }
  }
  return null;
}

// check if an id exists
function entryIdExists(id, entries) {
  return (id in entries);
}
// checks all node ids - folders and all copies of entries
function nodeIdExists(id, folders) {
  var contents = folders.children;
  for (var i = 0; i < contents.length; i++) {
    var curNode = contents[i];
    if (curNode.id == id) {
      return true;
    }
    else if (curNode.isFolder && curNode.children && 
             curNode.children.length > 0) {
      var result = nodeIdExists(id, curNode);
      if (result === true) return result;
    }
  }
  return false;
}
function idExistsInEntriesOrFolders(id, entries, folders) {
  return entryIdExists(id, entries) || nodeIdExists(id, folders);
}

function getFileType(filename) {
  return filename.split(".")[1];
}
