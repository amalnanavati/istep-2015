/* FOLDER CONTROLLER */

app.controller('FolderController', function($scope,$http) {
  
  var vm = $scope;

  var objPath = "db/objs/";
  vm.dbAPIFolderC = new MyDbAPI(); // from my-db-api.js

  // load folders from database, init view
  vm.initFolders = function() {
    var randomParam = new Date().getTime() + Math.random();
    var getUrl = objPath+vm.foldersFile+"?randomParam="+randomParam;
    // TODO - why doesn't the api work?
    //vm.dbAPIFolderC.readJsonFile(vm.foldersFile, function(data) {
    $http.get(getUrl)
      .success(function (data) {
        vm.data.folders = data;
        vm.data.treeData = vm.data.folders.children;
        // intialize folders
        console.log("folders", vm.data.folders);
        vm.loadFolder();
      })
      .error(function (data, status, headers, config) {
        console.error("JSON read Error data:", data);
        // if we want folders.json and the file doesn't exist,
        // read from backup
        if (status === 404) {
          // read from backup
          console.info(objPath+"folders.json "+" file doesn't exist, reading from backup");
          $http.get(objPath+"folders.default.json").success(function (data) {
            vm.data.folders = data;
            vm.data.treeData = vm.data.folders.children;
            // intialize folders
            console.log("folders", vm.data.folders);
            vm.loadFolder();
          })
          return;
        }
        $("button").attr("disabled",true);
        $('.folder-icon').unbind("click");
        self.doNotUpdate = true;
        alert("Database is corrupt - contact TechBridgeWorld to "+
          "try to recover from a backup.");
      });
  }

  // if you click on anything in the left sidebar: folder or entry
  vm.action = function(node, parentNode) {
    if (node.isFolder) {
      if (!vm.areRearranging)
        vm.updatePageToNode(node.id);
      // else don't do anything in rearranging mode
    }
    // clicked an entry
    else {
      var entry = vm.nodeIdToEntry(node.id);
      if ((vm.isFolderView && vm.addingWordsToFolder) ||
          vm.areRearranging) {
        // do nothing on entry click when rearranging left sidebar
      }
      else {
        vm.updatePageToNode(node.id);
      }
    }
  };
  
  vm.clickFolder = function(nodeId) {
    $("#li_"+nodeId).toggleClass(vm.closedFolderClass)
                    .toggleClass(vm.openFolderClass);
    $("#folder_icon_"+nodeId).toggleClass('fa-folder')
                             .toggleClass('fa-folder-open');
    if (!vm.areRearranging && !vm.addingWordsToFolder)
      vm.updateFoldersFromView();
  }

  function checkAllowedPosition(placeholderParent, currentItem) {
    var childIsFolder = $(currentItem).hasClass("folder");
    var parentIsFolder = $(placeholderParent).hasClass("folder");
    var newWordsFolderHtmlId = "li_" + vm.newWordsFolderId;
    var parentIsNewWordsFolder = 
      $(placeholderParent).attr("id") === newWordsFolderHtmlId;
    var childIsNewWordsFolder =
      $(currentItem).attr("id") === newWordsFolderHtmlId;
    // root node case, only folders allowed to be dropped
    if (!placeholderParent) {
      return childIsFolder;
    }
    // newWords cannot be moved in any subfolder, only root
    // (and dropping in root is covered in case above)
    else if (childIsNewWordsFolder) {
      return false;
    }
    // folders can be placed in other folders, but not newWords
    else if (childIsFolder) {
      return (parentIsFolder && !parentIsNewWordsFolder);
    }
    // entries can be placed in any folder
    else {
      return parentIsFolder;
    }
  }

  vm.initSortable = function() {
    $("#tree-root").nestedSortable({
      handle: 'div',
      items: 'li',
      toleranceElement: '> div',
      tabSize: 10,
      cursor: "move",
      cursorAt: {top: 8},
      revert: 200,
      isTree: true,
      expandOnHover: 700,
      activate: function(event, ui) {
        ui.item.addClass("dragging");
      },
      deactivate: function(event, ui) {
        ui.item.removeClass("dragging");
      },
      // tell where something is allowed
      isAllowed: function(placeholder, placeholderParent, currentItem) {
        //console.log(placeholder, placeholderParent, currentItem);
        return checkAllowedPosition(placeholderParent, currentItem);
      },
      // other options
      forcePlaceholderSize: true,
      helper: 'clone',
      expandedClass: vm.openFolderClass,
      collapsedClass: vm.closedFolderClass,
      //opacity: .6,
      placeholder: 'placeholder',
      tolerance: 'pointer',
      maxLevels: 0,
      disabled: true    // starts out disabled
    });
  }

  // initialization
  vm.initFolders();
  vm.initSortable();

});