// my-db-api.js
// hacked together by Erik Pintar in July 2015

function MyDbAPI() {
  var self = this;
  var objPath = "db/objs/";
  self.doNotUpdate = false;

  // NEVER cache the entries or folders objects!!
  // (was a bug sometimes on Windows executable)
  $.ajaxSetup({ cache: false });

  self.readJsonFile = function(filename, callback) {
    var randomParam = new Date().getTime() + Math.random();
    $.ajax({
      type: "GET",
      url: objPath+filename+"?randomParam="+randomParam,
      dataType: 'json'
    }).done(function (data) {
      if (callback) callback(data);
    }).fail(function (err) {
      console.error("JSON read Error:", err);
      // if we want folders.json or entries.json and the file doesn't exist,
      // read from backup
      if (err.status === 404 && 
        (filename === "folders.json" || filename === "entries.json")) {
        // if those files don't exist, read from backup
        console.info(filename+" file doesn't exist, reading from backup");
        var filenameName = filename.substr(0, filename.indexOf("."));
        self.readJsonFile(filenameName+".default.json", callback);
        return;
      }
      $("button").attr("disabled",true);
      $('.folder-icon').unbind("click");
      self.doNotUpdate = true;
      alert("Database is corrupt - contact TechBridgeWorld to "+
        "try to recover from a backup.");
    });
  }

  self.writeJsonFile = function(filename, data, callback) {
    if (self.doNotUpdate) return;
    $.ajax({
      type: "POST",
      url: "saveJSON.php",
      //dataType: 'json',
      data: {
         jsonStr: JSON.stringify(data),
         filename: filename
      }
    }).done(function(o) {
      if (callback) callback();
    }).fail(function (err) {
      console.error("JSON write Error:", err);
      $("button").attr("disabled",true);
      $('.folder-icon').unbind("click");
      self.doNotUpdate = true;
      alert("Database is corrupt - contact TechBridgeWorld to "+
        "try to recover from a backup.  Error: "+err);
    });
  }

  self.makeObjBackups = function(callback) {
    if (self.doNotUpdate) return;
    $.ajax({
      type: "POST",
      url: "autoBackup.php",
    }).done(function(result) {
      if (callback) callback(result);
    }).fail(function (err) {
      console.error("Database Error in makeObjBackups:", err);
    });
  }

  return self;
}
