// main.js
// SignBook core javascript file
// Erik Pintar, July 2015

var app = angular.module('signBook', ['app.directives']);

app.controller('MainController', function($scope) {
  // view-model as shortcut for $scope / this
  var vm = $scope;

  // query params
  vm.params = getQueryParams(document.location.search);

  // constants
  vm.entriesFile = "entries.json";
  vm.foldersFile = "folders.json";
  vm.gifDir = "db/gifs/";
  vm.webmDir = "db/webm/";
  vm.imgDir = "db/imgs/";
  vm.objDir = "db/objs/";
  vm.newWordId = "newWord";
  vm.newWordsFolderId = "newWords";
  vm.newWordsFolderName = "Unsorted Words";
  vm.newFolderId = "newFolder";
  vm.newVidPage = "/createVid.html";
  // what separates an entry id and the copy number of it
  // this is weird because id name are finicky with nestedSortable
  // has to be "category_name" with category and name only being [A-Za-z0-9]
  vm.nodeIdSep = "0v";
  // DOM constants
  vm.openFolderClass = 'open-folder';
  vm.closedFolderClass = 'closed-folder';

  // dictionary data
  vm.data = {
    // objects from database of all entries and folder structure
    entries: null,
    folders: null,
    // nodeId is id in folders structures
    // (nodeIds have numbers after an nodeIdSep for duplicate words)
    nodeId: null,
    // entry is id in entry structure
    entry: null,
    // these are the retrieved objects of the current entry/folder
    // only one is set, depending if we are looking at an entry or a folder
    entryObj: null,
    folderObj: null,
    // retrieved object of parent folder
    parentFolderObj: null,
    // treeData is for the folder left sidebar view
    treeData: null
  };
  vm.dbAPI = MyDbAPI(); // from my-db-api.js
  vm.legalNodeAttrs = [
    "id",
    "children",
    "name",
    "isFolder",
    "collapsed"
  ];

  // DOM variables
  vm.vidUrl = "";
  vm.numVids = 1;
  vm.picUrl = "";
  vm.numPics = 1;
  vm.englishInputText = "";
  vm.curVidIndex = 0;
  vm.curPicIndex = 0;
  vm.curEngIndex = 0;

  // DOM status
  vm.isEditMode = (vm.params.editMode === "true");
  vm.isFolderView = false; // will be changed later
  vm.vidElemShow = true;
  vm.playingVid = false;
  vm.loopingVid = false;
  vm.isNewEntry = false;
  vm.isNewFolder = false;
  vm.isNewMeaning = false;
  vm.isHomepage = false;
  vm.addingWordsToFolder = false;
  vm.addedWordToFolder = false;
  vm.areRearranging = false;
  vm.watchers = [];
  vm.vidElem = $("#video-elem")[0];

  /* INITIALIZATIONS */

  vm.init = function(callback) {
    vm.initVidPicSizes();
    vm.initShown();
    vm.dbAPI.readJsonFile(vm.entriesFile, function(entriesResult) {
      vm.data.entries = entriesResult;
      console.log("entries", vm.data.entries);
      vm.loadNodeAndEntry();
      // loadFolder() later in folderController init
      // initialize constants that depend on entries obj
      vm.setUpWatchers();
      if (callback) callback();
    });
  };

  /* REINIT FUNCTIONS */

  vm.updatePageToNode = function(nodeId) {
    console.log("\nasync page update to nodeId:", nodeId);
    // "fake" new url params
    vm.params.nodeId = nodeId;
    // set view-specific data objects to null
    vm.data.entry = null;
    vm.data.entryObj = null;
    vm.data.folderObj = null;
    vm.data.parentFolderObj = null;
    vm.data.treeData = vm.data.folders.children;
    // remove any watchers we have with the destroy functions we got
    for (var i = vm.watchers.length - 1; i >= 0; i--) {
      var destroyWatcherFn = vm.watchers[i];
      destroyWatcherFn();
    };
    vm.watchers = [];
    // other variables to reinit
    setHelpText("Learn sign language!");
    vm.isFolderView = false;
    vm.englishInputText = "";
    vm.folderInputText = "";
    vm.curVidIndex = 0;
    vm.curPicIndex = 0;
    vm.curEngIndex = 0;
    vm.vidElemShow = true;
    vm.playingVid = false;
    vm.loopingVid = false;
    vm.isNewEntry = false;
    vm.isNewFolder = false;
    vm.isNewMeaning = false;
    vm.isHomepage = false;
    vm.addingWordsToFolder = false;
    vm.addedWordToFolder = false;
    vm.areRearranging = false;
    // reload the node and entry
    vm.loadNodeAndEntry();
    // also reload the folders
    vm.loadFolder();
    // set up new watchers
    vm.setUpWatchers();
    // reinit the picture upload
    if (!vm.isFolderView && !vm.isNewEntry) {
      vm.myDropzone = null;
      $("#my-dropzone").remove();
      $(".dz-hidden-input").remove();
      var newDropzone = $('<form action="/picUpload.php"' +
        ' class="dropzone" id="my-dropzone"></form>');
      $("#pic-upload-area").append(newDropzone);
      vm.initDropzone();
    }
    vm.reinitVidPicSizes();
  }

  vm.reinitFolders = function(callback) {
    vm.dbAPI.readJsonFile(vm.foldersFile, function(oldFolders) {
      // destructively adds new data
      vm.data.folders = jQuery.extend(true, {}, oldFolders);
      vm.data.folderObj = findNodeWithId(vm.data.nodeId, vm.data.folders);
      vm.data.parentFolderObj = findParentNodeOfNodeWithId(
        vm.data.nodeId, vm.data.folders);
      vm.data.treeData = vm.data.folders.children;
      console.log("folders reverted", vm.data.folders);
      $scope.$apply();
      if (callback) callback();
    });
  }

  /* INIT HELPERS */

  vm.initVidPicSizes = function() {
    vm.adjustVidPicSizes();
    $(window).resize(function() {
      vm.adjustVidPicSizes();
    });
    $scope.$watchCollection('showHides',
      function() {
        vm.updateShown();
        vm.adjustVidPicSizes();
      });
  }

  vm.reinitVidPicSizes = function() {
    // do some stuff after $scope.$apply
    setTimeout(function() {
      $(".button-bar").each(function(i, elem) {
        if ($(elem).width() === 0) $(elem).hide();
        else $(elem).show();
      });
      vm.adjustVidPicSizes();
    }, 2);
  }

  vm.initShown = function() {
    if (vm.params.shown) vm.shown = vm.params.shown;
    else vm.shown = "vpw";
    vm.showHides = {
      showVid: vm.isShown("v"),
      showPic: vm.isShown("p"),
      showWord: vm.isShown("w"),
      disableVid: false,
      disablePic: false
    }
  }

  vm.changeVid = function(vidName) {
    if (vidName) {
      var isGif = getFileType(vidName) === "gif";
      var vidDir;
      // GIF
      if (isGif) {
        vidDir = vm.gifDir;
        vm.vidElemShow = false;
      }
      // WEBM
      else {
        vidDir = vm.webmDir;
        vm.vidElem = $("#video-elem")[0];
        vm.vidElemShow = true;
        vm.loopVid();
      }
      vm.vidUrl = vidDir + vidName;
      if (isGif) {
        $("#vid-loop").css('background-image','url('+vm.vidUrl+')');
      }
      else {
        $("#video-elem").attr('src', vm.vidUrl);
        // loop by default
        vm.loopVid();
      }
      vm.showHides.disableVid = false;
    }
  }

  vm.setUpWatchers = function() {
    if (!vm.isFolderView && !vm.isNewEntry && !vm.isHomepage) {
      vm.watchers = [];
      // watch isEditMode change
      vm.watchers.push($scope.$watch('isEditMode', function() {
        vm.reinitVidPicSizes();
      }));
      // update current video and picture information
      vm.numVids = vm.data.entryObj.videos.length;
      vm.numPics = vm.data.entryObj.images.length;
      vm.watchers.push($scope.$watch('curVidIndex', function() {
        var vidName = vm.data.entryObj.videos[vm.curVidIndex];
        vm.changeVid(vidName);
      }));
      if (!vm.data.entryObj.videos[vm.curVidIndex]) 
        vm.showHides.disableVid = true;
      vm.watchers.push($scope.$watch('curPicIndex', function() {
        if (vm.data.entryObj.images[vm.curPicIndex]) {
          vm.picUrl = vm.imgDir + vm.data.entryObj.images[vm.curPicIndex];
          $("#pic-img").css('background-image','url('+vm.picUrl+')');
        }
      }));
      if (!vm.data.entryObj.images[vm.curPicIndex]) 
        vm.showHides.disablePic = true;
      vm.watchers.push($scope.$watch('curEngIndex', function() {
        if (vm.data.entryObj.english[vm.curEngIndex])
          vm.englishInputText = vm.data.entryObj.english[vm.curEngIndex];
      }));
      // update numVids and numPics when they change
      vm.watchers.push($scope.$watch('data.entryObj.videos.length', function() {
        vm.numVids = vm.data.entryObj.videos.length;
        var vidName = vm.data.entryObj.videos[vm.curVidIndex];
        vm.changeVid(vidName);
      }));
      vm.watchers.push($scope.$watch('data.entryObj.images.length', function() {
        vm.numPics = vm.data.entryObj.images.length;
        if (vm.data.entryObj.images[vm.curPicIndex]) {
          vm.picUrl = vm.imgDir + vm.data.entryObj.images[vm.curPicIndex];
          $("#pic-img").css('background-image','url('+vm.picUrl+')');
          vm.showHides.disablePic = false;
        }
      }));
      vm.watchers.push($scope.$watch('data.entryObj.english.length', function() {
        vm.numEng = vm.data.entryObj.english.length;
        if (vm.data.entryObj.english[vm.curEngIndex]) {
          vm.englishInputText = vm.data.entryObj.english[vm.curEngIndex];
          // can't disable text, should always be there
        }
      }));
    }
  }

  vm.adjustVidPicSizes = function() {
    var minW = 320;
    var minH = 240;
    // get max width (mh) possible
    var totalW = $("#vid-pic-container").width();
    var widthScale = 0.45;
    var hwRatio = ($("#video-container").height() /
                   $("#video-container").width());
    var maxwW = totalW * widthScale;
    var maxwH = maxwW * hwRatio;
    // get max height (mh) possible
    var totalH = $(window).height();
    var curH = $("#right-display-container").height();
    var curVidPicH = $("#video-container").height();
    var takenH = curH - curVidPicH;
    var maxhH = totalH - takenH;
    var maxhW = maxhH / hwRatio;

    // apply constraints of height and width, but
    // enforce a minimum height and width
    var idealH = Math.min(maxwH, maxhH);
    var idealW = Math.min(maxwW, maxhW);
    var finalW, finalH;
    // height is prioritized to fit
    if (idealH === maxhH) {
      finalH = maxhH; finalW = maxhW;
    }
    // otherwise, stretch width as far as it can go
    else {
      finalW = maxwW; finalH = maxwH;
    }
    // and finally make sure we enforce a minimum size
    if (finalW < minW) {
      finalW = minW; finalH = minW * hwRatio;
    }
    $("#video-container").width(finalW).height(finalH);
    $("#pic-container").width(finalW).height(finalH);
    $("#video-elem").width(finalW).height(finalH);
  }

  vm.loadNodeAndEntry = function() {
    if (!vm.params.nodeId) {
      vm.isHomepage = true;
      return;
    }
    vm.data.nodeId = vm.params.nodeId;
    vm.data.entry = vm.nodeIdToEntry(vm.data.nodeId);
    console.log("node:", vm.data.nodeId, "entry:", vm.data.entry);
    // for new words
    if (vm.data.entry === vm.newWordId) {
      vm.isNewEntry = true;
      console.log("new word!");
      setHelpText("Enter the english word or phrase for "+
                  "your new dictionary entry.");
      setTimeout(function() {
        $("#eng-input").focus();
      }, 5);
      return true;
    }
    // this is not an entry, probably a folder
    else if (!(vm.data.entry in vm.data.entries)) {
      vm.isFolderView = true;
      vm.data.entry = null;
      return false;
    }
    // it is an entry with an entryObj
    else {
      vm.data.entryObj = vm.data.entries[vm.data.entry];
      var entryObj = vm.data.entryObj;
      // Video
      if (entryObj.videos.length > 0) {
      }
      // Image
      if (entryObj.images.length > 0) {
      }
      // English (should always be there)
      if (entryObj.english.length > 0) {
        vm.englishInputText = entryObj.english[vm.curEngIndex];
      }
      return true;
    }
  }

  vm.loadFolder = function() {
    if (!vm.params.nodeId) {
      vm.isHomepage = true;
      return;
    }
    vm.data.nodeId = vm.params.nodeId;
    vm.data.parentFolderObj = findParentNodeOfNodeWithId(
      vm.data.nodeId, vm.data.folders);
    // should have parentFolder open
    vm.data.parentFolderObj.collapsed = false;
    var possibleFolderObj = findNodeWithId(
      vm.data.nodeId, vm.data.folders);
    // if nodeId is an entry
    if (!possibleFolderObj) {
      if (vm.isFolderView) 
        console.error("Folder does not exist:", vm.data.nodeId);
      return;
    }
    if (!vm.isFolderView) return;
    // if nodeId is a folder and we are looking at it, set up folder data
    var folderObj = possibleFolderObj;
    vm.data.folderObj = possibleFolderObj;
    // for new folder
    if (vm.data.folderObj.id === vm.newFolderId) {
      vm.isNewFolder = true;
      setHelpText("Enter the name for "+
                  "your new topic.");
      setTimeout(function() {
        $("#folder-name-input").focus();
      }, 5);
    }
    else {
      if (vm.isEditMode)
        setHelpText("Change your topic folder's name or "+
          "add words to it.");
      else
        setHelpText("Click a word or topic within this "+
          "topic folder to view it.");
      vm.folderInputText = folderObj.name;
    }
    if (vm.data.folderObj.collapsed) {
      vm.data.folderObj.collapsed = false;
      vm.updateFoldersFromModel();
    }
  }

  /* EDIT BUTTONS */

  vm.updateEntries = function(callback) {
    console.log("updated entries", vm.data.entries);
    vm.dbAPI.writeJsonFile(vm.entriesFile, vm.data.entries, function() {
      if (callback) callback();
      $scope.$apply();
    });
  }

  // from word view

  vm.addNewVideo = function() {
    updatePage({vidMode: true,
                nodeId: vm.data.nodeId, 
                eng: vm.data.entryObj.english[0],
                editMode: true,
                shown: vm.shown},
                vm.newVidPage);
  }

  vm.takeNewPic = function() {
    updatePage({vidMode: false,
                nodeId: vm.data.nodeId, 
                eng: vm.data.entryObj.english[0],
                editMode: true,
                shown: vm.shown},
                vm.newVidPage);
  }

  // from homepage (creating video/pic before word is known)

  vm.takeVideoForNewWord = function() {
    updatePage({vidMode: true,
                nodeId: "", 
                eng: "",
                editMode: true,
                shown: vm.shown},
                vm.newVidPage);
  }

  vm.takePictureForNewWord = function() {
    updatePage({vidMode: false,
                nodeId: "", 
                eng: "",
                editMode: true,
                shown: vm.shown},
                vm.newVidPage);
  }

  /* UPDATE TEXT BUTTONS */

  vm.updateEnglishText = function() {
    var entryObj = vm.data.entryObj;
    var newEntryId = makeWordHtmlSafe(vm.englishInputText);
    vm.isNewMeaning = false;
    if (!vm.englishInputText || !newEntryId)
      return;
    // new entry
    if (vm.isNewEntry) {
      // check that it isn't a folder or entry
      if (idExistsInEntriesOrFolders(newEntryId, 
        vm.data.entries, vm.data.folders)) {
        alert("That name is already taken.  Please choose another name.");
        return;
      }
      id = makeWordHtmlSafe(vm.englishInputText);
      var eng = vm.englishInputText;
      var newEntryObj = {
        english: [eng],
        videos: [],
        images: []
      }
      vm.data.entries[newEntryId] = newEntryObj;
      var newWordNode = findNodeWithId(vm.newWordId, 
                                    vm.data.folders);
      if (newWordNode) {
        newWordNode.id = newEntryId;
        newWordNode.name = eng;
      }
    }
    // updated english meaning
    else if (entryObj.english.length > 0) {
      entryObj.english[vm.curEngIndex] = vm.englishInputText;
    }
    // fallback case, but english field should exist
    else {
      console.error("why no english field?");
      entryObj.english = [vm.englishInputText];
    }
    // update entry in database
    vm.updateEntries(function() {
      // only update node display name if index 0 (primary meaning)
      if (vm.curEngIndex === 0) {
        if (vm.isNewEntry) var entry = newEntryId;
        else var entry = vm.nodeIdToEntry(vm.data.nodeId);
        vm.updateEnglishTextInFolders(entry, vm.englishInputText);
      }
    });
  }

  // update all node labels (node.name) in folder tree for this entry
  vm.updateEnglishTextInFolders = function(entry, name) {
    vm.modifyAllEntriesWithFn(entry, function(node) {
      node.name = name;
    });
    // update folders
    vm.updateFoldersFromModel(function() {
      if (vm.isNewEntry) {
        // entry same as nodeId ONLY for a brand new entry
        vm.updatePageToNode(entry);
      }
      else {
        // this should have happened, but didn't ..? so do it here
        $(".entry-id-"+entry+" > .item > .label").html(name);
      }
    });
  }

  vm.updateFolderName = function() {
    var newFolderNodeId = makeWordHtmlSafe(vm.folderInputText);
    if (!vm.folderInputText || 
        !newFolderNodeId)
      return;
    // check that it isn't a folder or entry
    if (idExistsInEntriesOrFolders(newFolderNodeId, 
      vm.data.entries, vm.data.folders)) {
      alert("That name is already taken.  Please choose another name.");
      return;
    }
    // can't change "Unsorted Words"
    if (vm.data.folderObj.id === vm.newWordsFolderId) {
      alert("You cannot change this folder name.");
      vm.folderInputText = vm.newWordsFolderName;
      return;
    }
    var folderObj = vm.data.folderObj;
    folderObj.name = vm.folderInputText;
    if (vm.isNewFolder) {
      folderObj.id = newFolderNodeId;
    }
    vm.updateFoldersFromModel(function() {
      if (vm.isNewFolder) {
        console.log("folders", vm.data.folders);
        vm.updatePageToNode(folderObj.id);
      }
    });
  }

  /* EDIT FUNCTIONS */

  function setHelpText(helpStr) {
    $("#help-text").html(helpStr);
  }

  // finds all entryObjs from everywhere an entry is found
  // (all copies) and modifies them with the modFn function
  // that takes in a node and destructively modifies it
  vm.modifyAllEntriesWithFn = function(entry, modFn) {
    function modEntriesInFolderObj(entry, folderObj, modFn) {
      // recursively remove from all subfolders
      for (var i = 0; i < folderObj.children.length; i++) {
        var curNode = folderObj.children[i];
        if (vm.nodeIdToEntry(curNode.id) === entry) {
          modFn(curNode);
        }
        if (curNode.isFolder && curNode.children && 
                 curNode.children.length > 0) {
          modEntriesInFolderObj(entry, curNode, modFn);
        }
      }
    }
    modEntriesInFolderObj(entry, vm.data.folders, modFn);
  }

  /* SIDEBAR BUTTONS */

  // updateFolders depending on if we update
  // from the "view" or from the "model"
  vm.updateFoldersFromView = function(callback) {
    vm.updateFolderModel();
    vm.dbAPI.writeJsonFile(vm.foldersFile, vm.data.folders, function() {
      if (callback) callback();
      $scope.$apply();
    });
  }
  vm.updateFoldersFromModel = function(callback) {
    vm.cleanExtraNodeParams(vm.data.folders);
    vm.data.treeData = vm.data.folders.children;
    vm.dbAPI.writeJsonFile(vm.foldersFile, vm.data.folders, function() {
      if (callback) callback();
      $scope.$apply();
    });
  }

  vm.toggleMode = function() {
    vm.isEditMode = !vm.isEditMode;
  }

  vm.toggleRearrange = function() {
    vm.areRearranging = !vm.areRearranging;
    // start rearranging, enable movement
    if (vm.areRearranging) {
      $("#tree-root").nestedSortable("enable");
    }
    // done rearranging, save folder layout
    else {
      $("#tree-root").nestedSortable("disable");
      vm.updateFoldersFromView(function() {
        if (vm.isHomepage) {
          vm.goToHomepage();
          return;
        }
        // TODO - really bad bug, if you don't refresh bad things happen
        // like if you move a folder to root layer it will duplicate
        // or if you move unsorted words within a folder it will crash
        updatePage({
          nodeId: vm.data.nodeId,
          editMode: vm.isEditMode,
          shown: vm.shown
        });
      });
    }
  }

  vm.toggleAddWordsToFolder = function() {
    if (vm.addingWordsToFolder) {
      vm.addedWordToFolder = false;
      vm.updateFoldersFromModel();
    }
    vm.addingWordsToFolder = !vm.addingWordsToFolder;
  }

  vm.cancelRearrange = function() {
    updatePage({
      nodeId: vm.data.nodeId,
      editMode: vm.isEditMode,
      shown: vm.shown
    });
  }

  vm.cancelAddingWords = function() {
    vm.reinitFolders();
    vm.addingWordsToFolder = false;
    vm.addedWordToFolder = false;
  }

  // destructively remove attributes we don't want in JSON folders file
  vm.cleanExtraNodeParams = function(node) {
    var childNodes = node.children;
    var legalNodeAttrs = vm.legalNodeAttrs;
    for (var i = 0; i < childNodes.length; i++) {
      var curNode = childNodes[i];
      var badAttrs = [];
      // find bad attributes
      for (attr in curNode) {
        // if attribute isn't legal, queue it for cleaning
        if (legalNodeAttrs.indexOf(attr) === -1) {
          badAttrs.push(attr);
        }
      }
      // remove the bad attributes
      for (var k = 0; k < badAttrs.length; k++) {
        var badAttr = badAttrs[k];
        delete curNode[badAttrs];
      }
      // recursively go through children
      if (curNode.isFolder && curNode.children && 
               curNode.children.length > 0) {
        vm.cleanExtraNodeParams(curNode);
      }
    }
    // returns nothing, destructive
  }

  // updates treeData structure from html representation from sortable
  vm.updateFolderModel = function() {
    // Note: If serialize returns an empty string, make sure the id attributes
    // include an underscore. They must be in the form: "set_number" 
    var curFolderTree = $("#tree-root").nestedSortable("toHierarchy");
    // recursively, destructively modifies a folderTree structure
    function traverseAndUpdate(fTree) {
      for (var i = 0; i < fTree.length; i++) {
        var node = fTree[i];
        node.name = $("#label_"+node.id).html();
        var isFolder = $("#li_"+node.id).hasClass("folder");
        if (isFolder) {
          node.isFolder = true;
          var isCollapsed = $("#li_"+node.id)
              .hasClass(vm.closedFolderClass);
          node.collapsed = isCollapsed;
          if (node.children) {
            node.children = traverseAndUpdate(node.children);
          }
          // for folders with no children
          else if (isFolder) {
            node.children = [];
          }
        }
      }
      return fTree
    }
    var result = traverseAndUpdate(curFolderTree);
    vm.data.folders.children = result;
    vm.data.treeData = vm.data.folders.children;
  }

  vm.createEntry = function() {
    if ($("#li_"+vm.newWordId).length > 0) {
      var newWordParentObj = findParentNodeOfNodeWithId(
        vm.newWordId, vm.data.folders);
      alert("You already have a new word in the '" +
        newWordParentObj.name + "' topic.");
      vm.updatePageToNode(vm.newWordId);
      return;
    }
    var newWordsFolderObj = findNodeWithId(
      vm.newWordsFolderId, vm.data.folders);
    if (!newWordsFolderObj) {
      alert("A folder named 'Unsorted Words' must exist"+
        " to create a new word.");
      return;
    }
    var entryParentFolderObj = newWordsFolderObj;
    // if we are in a folder, add word to that folder!
    if (vm.isFolderView && vm.data.folderObj) {
      entryParentFolderObj = vm.data.folderObj;
    }
    else if (!vm.isFolderView && vm.data.parentFolderObj) {
      entryParentFolderObj = vm.data.parentFolderObj;
    }
    if (!entryParentFolderObj.children) {
      entryParentFolderObj.children = [];
    }
    // make sure 'New Words' folder is open
    var folderId = entryParentFolderObj.id;
    var parentFolderElem = $("#li_"+folderId);
    if (parentFolderElem.hasClass(vm.closedFolderClass)) {
        parentFolderElem.removeClass(vm.closedFolderClass)
                        .addClass(vm.openFolderClass);
    }
    var newWordData = {id: vm.newWordId, name: "New Word"};
    entryParentFolderObj.children.push(newWordData);
    vm.updateFoldersFromModel(function() {
      vm.updatePageToNode(vm.newWordId);
    });
  }

  vm.createEntryFromHomepage = function() {
    vm.isEditMode = true;
    vm.createEntry();
  }

  vm.createFolder = function() {
    if (findNodeWithId(vm.newFolderId, vm.data.folders)) {
      vm.updatePageToNode(vm.newFolderId);
      return;
    }
    var newFolderObj = {
      id: vm.newFolderId,
      children: [],
      name: "New Folder",
      isFolder: true,
      collapsed: true
    }
    vm.data.folders.children.push(newFolderObj);
    vm.updateFoldersFromModel(function() {
      vm.updatePageToNode(vm.newFolderId);
    });
  }

  vm.createFolderFromHomepage = function() {
    vm.isEditMode = true;
    vm.createFolder();
  }

  // assume in folder view, add entry to that folder
  vm.addEntryToFolder = function(entry, entryName, folderId) {
    // if entry is in New Words, remove it from there
    var newWordsFolderObj = findNodeWithId(
          vm.newWordsFolderId, vm.data.folders);
    vm.deleteEntryFromFolder(entry, newWordsFolderObj)
    // add new unique folder entry id for this entry
    var newEntryId = vm.findNewFolderEId(entry);
    var entryObj = {id: newEntryId, name: entryName};
    // add the entry to the desired folder, if not already there
    var folderObj = findNodeWithId(folderId, vm.data.folders);
    var folderHasEntry = folderObj.children.reduce(
      function(prevResult, childObj, i, arr) {
        return (prevResult || 
          (vm.nodeIdToEntry(childObj.id) === entry));
      }, false);
    if (!folderHasEntry) {
      folderObj.children.push(entryObj);
      vm.addedWordToFolder = true;
    }
    else {
      console.log("folder", folderId, "already has entry", entry);
    }
  }

  /* DELETION */

  // delete 
  vm.deleteItem = function() {
    var confirmDelete = false;

    // delete folder
    if (vm.isFolderView) {
      var folderObj = vm.data.folderObj;
      var parentFolderObj = vm.data.parentFolderObj;
      // delete folder and all contents
      if (vm.deleteAll) {
        confirmDelete = confirm("Are you sure you want to delete "+
          vm.data.folderObj.name + 
          " and every word and folder inside, and " +
          "every word's videos and pictures?");
        if (confirmDelete) {
          confirmDelete = confirm("Are you really sure? " +
            "Any folders inside will also have everything deleted! " +
            "Every word inside that is the only copy of that word "+
            "will be permanently deleted!");
          if (confirmDelete) {
            vm.deleteFolderAndContents(folderObj, parentFolderObj, true);
          }
        }
      }
      // delete just folder, move original contents to unsorted words
      else {
        var confirmStr = ("Are you sure you want to delete "+
          "the " + vm.data.folderObj.name + " folder? ");
        if (folderObj.children.length > 0)
          confirmStr = (confirmStr +
          "Contents not found in other folders "+
          "will be moved to the Unsorted Words folder.");
        confirmDelete = confirm(confirmStr);
        if (confirmDelete) {
          vm.deleteFolderAndContents(folderObj, parentFolderObj, false);
        }
      }
      if (confirmDelete) console.log("deleted folder:", vm.data.folderObj.id);      
    }

    // delete entry
    else {
      var entryNodeId = vm.data.nodeId;
      var entry = vm.nodeIdToEntry(entryNodeId);
      // delete new word (no need for confirmation)
      if (entry === vm.newWordId) {
        vm.deleteEntryFromFolder(entry, vm.data.parentFolderObj);
        vm.deleteEntryFromEntries(entry);
        confirmDelete = true;
      }
      // delete all copies / only copy of an entry
      else if (vm.deleteAll || (vm.entryCount(entry) === 1)) {
        if (vm.deleteAll) {
          confirmDelete = confirm("Are you sure you want to delete "+
            vm.data.entryObj.english[0] + " in every folder, and " +
            "its videos and pictures permanently?");
        }
        else {
          confirmDelete = confirm("Are you sure you want to delete "+
            vm.data.entryObj.english[0] + ", and " +
            "its videos and pictures permanently?");
        }
        if (confirmDelete) {
          // delete from entries database
          vm.deleteEntryFromEntries(entry);
          // delete from every folder it is found in
          vm.deleteEntryFromAllFolders(entry);
        }
      }
      // delete only one copy of an entry with multiple copies
      else {
        confirmDelete = confirm("Are you sure you want to delete "+
          vm.data.entryObj.english[0] + " from the " + 
          vm.data.parentFolderObj.name + " folder?");
        if (confirmDelete) {
          // delete this specific entry
          vm.deleteEntryFromFolder(entry, vm.data.parentFolderObj);
        }
      }
      if (confirmDelete) console.log("deleted entry:", vm.data.nodeId);
    }

    // update database if we did the delete
    if (confirmDelete) {
      vm.updateEntries(function() {
        vm.updateFoldersFromModel(function() {
          if (vm.isFolderView) {
            vm.goToHomepage();
          }
          else {
            vm.updatePageToNode(vm.data.parentFolderObj.id);
          }
        });
      });
    }
  }

  // removes entry with id "entry" from entries object
  // but does not write to database
  vm.deleteEntryFromEntries = function(entry) {
    delete vm.data.entries[entry];
  }

  // delete entry from inside a folder object
  // but does not write to database
  vm.deleteEntryFromFolder = function(entry, folderObj) {
    folderObj.children = folderObj.children.filter(
      function(childObj, i, a) {
        return vm.nodeIdToEntry(childObj.id) !== entry;
      });
  }

  // recursively deletes an entry from everywhere it is found
  vm.deleteEntryFromAllFolders = function(entry) {
    function deleteEntryFromFolders(entry, folderObj) {
      // recursively remove from all subfolders
      for (var i = 0; i < folderObj.children.length; i++) {
        var curNode = folderObj.children[i];
        if (curNode.isFolder && curNode.children && 
                 curNode.children.length > 0) {
          deleteEntryFromFolders(entry, curNode);
        }
      }
      // remove from current folder
      vm.deleteEntryFromFolder(entry, folderObj);
    }
    deleteEntryFromFolders(entry, vm.data.folders);
  }

  // used in folder view, adds it to Unsorted Words if only copy
  vm.deleteNodeFromCurFolder = function(node) {
    var entry = vm.nodeIdToEntry(node.id);
    vm.deleteEntryFromFolder(entry, vm.data.folderObj);
    vm.addedWordToFolder = true;
    if (vm.entryCount(entry) <= 1)
      vm.addEntryToFolder(entry, node.name,
                          vm.newWordsFolderId);
  }

  // removes folder with id "folder" from folders object
  // but does not write to database
  // only call on empty folders! (removes contents blindly)
  vm.deleteFolder = function(folderObj, parentFolderObj) {
    parentFolderObj.children = parentFolderObj.children.filter(
      function(childObj, i, a) {
        return (childObj.id !== folderObj.id);
      });
  }

  // deletes folder from folders object
  // if deleteContents is true, moves unique entries to Unsorted Words
  // if deleteContents is false, deletes all entries and removes
  //    unique entries from the entries object
  vm.deleteFolderAndContents = function(folderObj, parentFolderObj, deleteContents) {
    if (typeof(deleteContents) === "undefined")
      deleteContents = false;
    // loop over all contents
    for (var i = 0; i < folderObj.children.length; i++) {
      var childObj = folderObj.children[i];
      // recursively remove all subfolders too
      if (childObj.isFolder) {
        vm.deleteFolderAndContents(childObj, folderObj, deleteContents);
      }
      else { // is entry
        var entry = vm.nodeIdToEntry(childObj.id);
        // completely delete entries
        if (deleteContents) {
          if (vm.entryCount(entry) <= 1) {
            vm.deleteEntryFromEntries(entry);
          }
        }
        // move unique entries to Unsorted Words
        else {
          if (vm.entryCount(entry) <= 1) {
            vm.addEntryToFolder(entry, childObj.name, 
                                vm.newWordsFolderId);
          }
        }
      }
    }
    // remove everything in the folder
    folderObj.children.length = 0;
    // finally, remove the folder
    vm.deleteFolder(folderObj, parentFolderObj);
  }

  /* UTILS */

  vm.goToPageOfNodeObj = function(node) {
    if (!vm.addingWordsToFolder) {
      vm.updatePageToNode(node.id);
    }
  }

  vm.goToHomepage = function() {
    updatePage({}, "/");
  }

  vm.entryCount = function(entry) {
    var entryClassSelector = ".entry-id-"+entry;
    return $(entryClassSelector).length;
  }

  vm.firstFolderWithEntry = function(entry) {
    return $($(".entry-id-"+entry)
              .closest(".folder")[0]).attr("id")
              .substring("li_".length);
  }

  // find first available entry id with entry-number
  vm.findNewFolderEId = function(entry) {
    for (var i = 1; i < 100; i++) {
      if (i === 1) {
        if ($("#li_"+entry).length === 0) {
          return entry;
        }
      }
      else {
        var testId = entry+vm.nodeIdSep+i.toString();
        if ($("#li_"+testId).length === 0) {
          return testId;
        }
      }
    }
    console.error("More than 100 copies of an element!");
    return;
  }

  vm.nodeIdToEntry = function(nodeId) {
    // folder entry id is like "entry_id_with_underscores-12"
    var dashIndex = nodeId.indexOf(vm.nodeIdSep);
    if (dashIndex !== -1) {
      return nodeId.substring(0, dashIndex);
    }
    else return nodeId;
  }

  /* VIDEO CONTROLS */

  vm.nextVid = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.videos ||
        vm.curVidIndex >= vm.data.entryObj.videos.length-1)
      return;
    else
      vm.curVidIndex++;
  }
  vm.prevVid = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.videos ||
        vm.curVidIndex <= 0)
      return;
    else
      vm.curVidIndex--;
  }
  vm.deleteVid = function() {
    if (confirm("Are you sure you want to delete this video?")) {
      vm.data.entryObj.videos.splice(vm.curVidIndex, 1);
      if (vm.curVidIndex > 0) vm.curVidIndex--;
      // don't actually delete video from the computer
      // in case someone gets delete-happy, it's like a backup
      vm.updateEntries();
    }
  }

  // for webm video

  vm.playVid = function() {
    vm.playingVid = true;
    vm.loopingVid = false;
    if (vm.vidElem) {
      vm.vidElem.play();
      vm.setUpVidStop();
    }
  }
  vm.pauseVid = function() {
    vm.playingVid = false;
    vm.loopingVid = false;
    if (vm.vidElem) {
      vm.vidElem.pause();
      vm.setUpVidStop();
    }
  }
  vm.loopVid = function() {
    vm.loopingVid = true;
    vm.playingVid = false;
    if (vm.vidElem) {
      vm.vidElem.play();
      vm.setUpVidLoop();
    }
  }
  vm.stopVid = function() {
    vm.loopingVid = false;
    vm.playingVid = false;
    if (vm.vidElem) {
      vm.vidElem.currentTime = 0;
      vm.vidElem.pause();
      vm.setUpVidStop();
    }
  }
  vm.setUpVidStop = function() {
    if (!vm.vidElem) return;
    $(vm.vidElem).unbind("ended").on("ended", function() {
      vm.playingVid = false;
      vm.loopingVid = false;
      $scope.$apply();
    });
  }
  vm.setUpVidLoop = function() {
    if (!vm.vidElem) return;
    $(vm.vidElem).unbind("ended").on("ended", function() {
      vm.vidElem.currentTime = 0;
      vm.vidElem.play();
      $scope.$apply();
    });
  }

  /* IMAGE CONTROLS */

  vm.nextPic = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.images ||
        vm.curPicIndex >= vm.data.entryObj.images.length-1)
      return;
    else
      vm.curPicIndex++;
  }
  vm.prevPic = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.images ||
        vm.curPicIndex <= 0)
      return;
    else
      vm.curPicIndex--;
  }
  // delete a picture, with an optional index passed in
  vm.deletePic = function(picIndex) {
    // default picIndex to currently shown picture
    if (!picIndex) picIndex = vm.curPicIndex;
    // check valid bounds
    if (picIndex < 0 || picIndex > vm.data.entryObj.images-1)
      return;
    // use the existing click handler
    var picName = vm.data.entryObj.images[picIndex];
    var deleteId = "delete-pic-" + picName.split(".")[0];
    $("#"+deleteId).click();
  }

  /* IMAGE UPLOAD */

  vm.addEntryImagesToDropzone = function() {
    var curEntry = vm.data.entryObj;
    var images = curEntry.images;
    // initialize dropzone with entry's pictures
    for (var i = 0; i < images.length; i++) {
      var imageName = images[i];
      // Create the mock file, size will be hidden
      var mockFile = {name: imageName, size: 12345};
      // Call the default addedfile event handler
      vm.myDropzone.emit("addedfile", mockFile);
      // Make sure that there is no progress bar, etc...
      vm.myDropzone.emit("complete", mockFile);
    }
  }

  // called after entries are loaded
  vm.initDropzone = function() {
    // set options
    Dropzone.options.myDropzone = {
      maxFilesize: 256, // TODO - PHP actually limits it to 2MB...
      acceptedFiles: "image/*"
    }
    // create dropzone
    var myDropzone = new Dropzone("#my-dropzone");
    vm.myDropzone = myDropzone;

    // init dropzone with existing images
    myDropzone.on("addedfile", function(file) {
      // image preview as background
      $(file.previewElement)
        .css("background-image", "url("+vm.imgDir+file.name+")");
      // don't create a thumbnail
      //myDropzone.emit("thumbnail", file, "");
      var trashIcon = $("<i class='fa fa-trash fa-2x'>");
      var btnId = "delete-pic-" + file.name.split(".")[0];
      var removeBtn = $("<button id="+btnId+">")
        .append(trashIcon)
        .addClass("delete-pic");
      $(file.previewElement).append(removeBtn);
      removeBtn.click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (confirm("Are you sure you want to remove "+
                    "this picture?")) {
          // go back a picture
          if (vm.curPicIndex !== 0)
            vm.curPicIndex--;
          // actually remove picture from computer
          // since pictures are likely another place too
          var correctFileName = file.name;
          if (typeof(file.filename) !== "undefined")
            correctFileName = file.filename;
          // update entries
          var images = vm.data.entryObj.images;
          images.splice(images.indexOf(correctFileName), 1);
          vm.updateEntries(function() {
            $.ajax({
              type: "POST",
              url: "picDelete.php",
              data: {
                filename: correctFileName
              }
            }).done(function (result) {
              var resultObj = JSON.parse(result);
              myDropzone.removeFile(file);
              // keep help text in right spot
              $(".dropzone").addClass("dz-started");
            }).fail(function (err) {
              console.error("image delete error:", err);
            });
          });
        }
      });
    });
    
    // when file uploaded, save it in the entries database
    myDropzone.on("success", function(file, response) {
      var responseObj = JSON.parse(response);
      // if there is an upload error
      if (responseObj.error !== 0) {
        if (responseObj.error === 1) {
          alert("Your file is too big. "+
            "Please upload a picture less than 2MB.");
        } else {
          alert("Your file had an error uploading. Sorry!");
        }
        myDropzone.removeFile(file);
        return;
      }
      var filename = responseObj.filename;
      file.filename = filename;
      // if id was wrong before, fix it
      var wrongBtnId = "delete-pic-" + file.name.split(".")[0];
      var rightBtnId = "delete-pic-" + filename.split(".")[0];
      // adjust filename in DOM
      $("#"+wrongBtnId).attr("id", rightBtnId);
      $(file.previewElement).find(".dz-filename > span").html(filename);
      $(file.previewElement)
        .css("background-image", "url("+vm.imgDir+filename+")");
      // add file to 
      vm.data.entryObj.images.push(filename);
      console.log("just added", filename, "to entryObj", vm.data.entryObj);
      vm.updateEntries();
    });

    myDropzone.on("error", function(file, eMsg, req) {
      console.error(eMsg);
    });

    $(".dz-message").html("Click here to upload a picture,<br>" +
      "or drag and drop pictures<br>into this box.");

    // init dropzone with images
    vm.addEntryImagesToDropzone();
  };

  /* ENGLISH WORD CONTROLS */

  vm.nextEng = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.english ||
        vm.curEngIndex >= vm.data.entryObj.english.length-1)
      return;
    else
      vm.curEngIndex++;
  }
  vm.prevEng = function() {
    if (!vm.data.entryObj || !vm.data.entryObj.english ||
        vm.curEngIndex <= 0)
      return;
    else
      vm.curEngIndex--;
  }
  vm.addNewEng = function() {
    vm.data.entryObj.english.push("");
    vm.curEngIndex = vm.data.entryObj.english.length - 1;
    vm.englishInputText = "";
    vm.isNewMeaning = true;
  }
  vm.deleteEng = function() {
    if (vm.numEng === 1) return; // can't delete only meaning
    var deletingPrimaryMeaning = (vm.curEngIndex === 0);
    if (confirm("Are you sure you want to delete "+
      "this meaning of the word?")) {
      vm.data.entryObj.english.splice(vm.curEngIndex, 1);
      if (vm.curEngIndex > 0) vm.curEngIndex--;
      // update the database entry english list
      vm.updateEntries(function() {
        // if a new index 0, change the name of this node in the folders
        if (deletingPrimaryMeaning) {
          var entry = vm.nodeIdToEntry(vm.data.nodeId);
          var primaryMeaning = vm.data.entryObj.english[0];
          vm.updateEnglishTextInFolders(entry, primaryMeaning);
        }
      });
    }
  }
  vm.cancelEngUpdate = function() {
    if (vm.isNewMeaning && !vm.isNewEntry) {
      vm.data.entryObj.english.splice(vm.curEngIndex, 1);
      if (vm.curEngIndex > 0) vm.curEngIndex--;
      vm.updateEntries();
      vm.isNewMeaning = false;
    }
    vm.englishInputText = vm.data.entryObj.english[vm.curEngIndex];
  }

  /* BOTTOM BAR */

  vm.moveFromNodeInFolder = function(step, nodeId, parentFolderObj) {
    var nodeIndex = vm.findIndexOfEntryIdInFolder(
      nodeId, parentFolderObj);
    var newIndex = nodeIndex + step;
    var maxIndex = parentFolderObj.children.length - 1;
    // loop within in the folder
    if (newIndex < 0) {
      newIndex = maxIndex;
    }
    else if (newIndex > maxIndex) {
      newIndex = 0;
    }
    var nextObj = parentFolderObj.children[newIndex];
    // skip folders
    if (nextObj.isFolder) {
      vm.moveFromNodeInFolder(step, nextObj.id, parentFolderObj);
      return;
    }
    // go to that page
    vm.updatePageToNode(nextObj.id);
  }

  vm.goToPrevWord = function() {
    if (vm.isHomepage || vm.isFolderView) return;
    vm.moveFromNodeInFolder(-1, vm.data.nodeId, vm.data.parentFolderObj);
  }

  vm.goToNextWord = function() {
    if (vm.isHomepage || vm.isFolderView) return;
    vm.moveFromNodeInFolder(1, vm.data.nodeId, vm.data.parentFolderObj);
  }

  // traverses a list of node objects with ids and children arrays
  // looking for an id match
  vm.findIndexOfEntryIdInFolder = function(id, folderObj) {
    var folderChildren = folderObj.children;
    for (var i = 0; i < folderChildren.length; i++) {
      var curNode = folderChildren[i];
      if (curNode.id == id) {
        return i;
      }
    }
    return -1;
  }

  vm.toggleVidShow = function() {
    vm.showHides.showVid = !vm.showHides.showVid;
  }
  vm.togglePicShow = function() {
    vm.showHides.showPic = !vm.showHides.showPic;
  }
  vm.toggleWordShow = function() {
    vm.showHides.showWord = !vm.showHides.showWord;
  }
  // if vm.shown is "vw" and check if word is shown,
  // itemLetter is "w" and it returns true
  vm.isShown = function(itemLetter) {
    return (vm.shown.indexOf(itemLetter) !== -1)
  }
  vm.updateShown = function() {
    vm.shown = "";
    if (vm.showHides.showVid) vm.shown += "v";
    if (vm.showHides.showPic) vm.shown += "p";
    if (vm.showHides.showWord) vm.shown += "w";
  }

  /* BIND KEY EVENTS */

  function doKeyboardShortcuts(e) {
    e = e || window.event;
    if (vm.areRearranging || vm.addingWordsToFolder) {
      return;
    }
    // if we are in an input, return can submit it
    else if (e.keyCode === 13 &&
             $(document.activeElement).is("input")) {
      doInputSubmit($(document.activeElement).attr("id"));
      return;
    }
    // only use shortcuts if we are not in an input
    // (buttons are okay)
    else if (!($(document.activeElement).is("body")) &&
        !($(document.activeElement).is("button"))) {
      return;
    }
    // left/up arrow
    if (e.keyCode == '37' || e.keyCode == '38') {
      $("#prev-word-btn").click();
    }
    // right/down arrow
    else if (e.keyCode == '39' || e.keyCode == '40') {
      $("#next-word-btn").click();
    }
    // Q - prev vid
    else if (e.keyCode == '81') $("#left-video-btn").click();
    // W - next vid
    else if (e.keyCode == '87') $("#right-video-btn").click();
    // E - show/hide vid
    else if (e.keyCode == '69') $("#show-vid-btn").click();
    // A - prev pic
    else if (e.keyCode == '65') $("#left-pic-btn").click();
    // S - next pic
    else if (e.keyCode == '83') $("#right-pic-btn").click();
    // D - show/hide pic
    else if (e.keyCode == '68') $("#show-pic-btn").click();
    // Z - prev meaning
    else if (e.keyCode == '90') $("#left-eng-btn").click();
    // X - next meaning
    else if (e.keyCode == '88') $("#right-eng-btn").click();
    // C - show/hide meaning
    else if (e.keyCode == '67') $("#show-word-btn").click();
    else {
      //console.log("pressed: ", e.keyCode);
    }
  }
  document.onkeydown = doKeyboardShortcuts;

  function doInputSubmit(inputElemId) {
    if (inputElemId === "eng-input") {
      $("#update-eng-btn").click();
    }
    else if (inputElemId === "folder-name-input") {
      $("#update-folder-name-btn").click();
    }
  }

  /* INITIALIZATION CALLS */

  // Prevent Dropzone from auto discovering the element
  Dropzone.options.myDropzone = false;

  // NEVER cache the entries or folders objects!!
  // (was a bug sometimes on Windows executable)
  $.ajaxSetup({ cache: false });

  vm.init(function() {
    // initialize the picture upload if we need it
    if (!vm.isFolderView && !vm.isNewEntry && !vm.isHomepage) 
      vm.initDropzone();
    if (vm.isHomepage)
      setHelpText("Explore topics and words in view mode,<br>"+
        "or click 'Edit Mode' for more options.");
    vm.reinitVidPicSizes();
    vm.dbAPI.makeObjBackups(function(backupCount) {
      console.log("backed up", backupCount, "files");
    });
  });
  
  // test stuff here that requires folders and entries to be loaded
  setTimeout(function() {
    
  }, 500);

});
