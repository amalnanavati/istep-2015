// my-gif-api.js
// hacked together by Erik Pintar in June 2015
// uses 4 other js files, and jquery
//  <script src="scripts/gif-recorder.js"></script>
//  <script src="scripts/gif-recorder-jsgif.js"></script>
//  <script src="scripts/recordRTC.js"></script>
//  <script src="scripts/omggif.js"></script>

function MyGifAPI(makingGif) {
  var self = this;

  self.makingGif = makingGif;

  // API requires these javascript elements actually
  var video = $('video#video-record')[0],
    editorCanvas = $('canvas#editor-canvas')[0],
    editorContext = editorCanvas.getContext('2d');

  var webcamStream = null,
    recorder = null,
    curGifReader = null,
    haveEdited = false,
    storedFrameObjList = null,
    editorOut = {},
    firstFrameNotBlack = 0,
    shownStartFrame = 0,
    mostRecentFilepath = "";

  self.initRecorder = function(recWidth, recHeight, callback) {
    recorder = null;
    curGifReader = null;
    haveEdited = false;
    editorOut = {};

    video.width = $('#video-record').width();
    video.height = $('#video-record').height();

    var options = {
      type: ((self.makingGif) ? 'gif' : 'video'),
      video: video,
      canvas: {
        width: recWidth,
        height: recHeight
      },
      disableLogs: false,
      recorderType: null // to let RecordRTC choose relevant types itself
    };

    // sets class recorder variable
    recorder = window.RecordRTC(webcamStream, options);
    if (callback) callback();
  }

  self.startRecording = function(callback) {
    recorder.startRecording();
    if (callback) callback();
  }

  self.stopRecording = function(callback) {
    recorder.stopRecording(function(url, newStoredFrameObjList) {
      if (self.makingGif) {
        storedFrameObjList = newStoredFrameObjList;
        if (callback) callback(url, storedFrameObjList);
      }
      else {
        video.src = url;
        video.play();
        $(video).attr("controls", true);
        $(video).removeClass("flipped");
        if (callback) callback(url);
      }
    });
  }

  // (unused) this method was unreliable, due to the counter
  self.takePicture = function(callback) {
    recorder.startRecording();
    setTimeout(function() {
      recorder.stopRecording(function(url, newStoredFrameObjList) {
        storedFrameObjList = newStoredFrameObjList;
        if (callback) callback(url, newStoredFrameObjList);
      })
    }, 250); // 250 ms should take at least one gif pic at 100 fps
  }

  // asks permission to turn camera on and
  // initializes RecordRTC recorder object
  self.turnCameraOn = function(recWidth, recHeight, callback) {
    var videoConstraints = {
      audio: false,
      video: {
        mandatory: {},
        optional: []
      }
    };

    navigator.getUserMedia(videoConstraints, function(stream) {
      webcamStream = stream;
      self.initRecorder(recWidth, recHeight, function() {
        video.onloadedmetadata = false;
        video.src = URL.createObjectURL(webcamStream);
        callback(recorder, webcamStream);
      });
    }, function() {
      alert("WARNING: You will not be able to record video. "+
          "Click the 'no video' icon in your browser "+
          "to change the setting. "+
          "Then refresh the page.");
      callback(null, null);
    });
  }

  // turns off webcam from recording
  self.turnCameraOff = function(callback) {
    if (video) {
      if (self.makingGif) {
        video.pause();
        video.src = '';
      }
      video.load();
    }

    if (webcamStream && webcamStream.stop) {
      webcamStream.stop();
    }
    webcamStream = null;
    if (callback) callback();
  }

  // use storedFrameObjList to reconstruct a list of bytes
  // that when concatted make a GIF!!!
  // startFrame and endFrame are optional
  self.getGifBytes = function(startFrame, endFrame) {
    if (!startFrame || !endFrame) {
      startFrame = 0;
      if (firstFrameNotBlack) startFrame = firstFrameNotBlack;
      endFrame = storedFrameObjList.length-1;
    }
    console.log("getting gifBytes for range:", startFrame, endFrame);
    console.log("storedFrameObjList", storedFrameObjList);
    var gifBytes = [];
    for (var f = startFrame; f <= endFrame; f++) {
      var storedFrameObj = storedFrameObjList[f];
      var headerBytes = [];
      if (f === startFrame) {
        $.merge(headerBytes, storedFrameObj.firstFrameHeader);
      } else {
        $.merge(headerBytes, storedFrameObj.laterFrameHeader);
      }
      var frameBytes = headerBytes.concat(storedFrameObj.pixelData);
      if (f === endFrame) {
        // write gif trailer if last frame
        frameBytes = frameBytes.concat([0x3b]);
      }
      // destructively extends gifBytes
      $.merge(gifBytes, frameBytes);
    }
    return gifBytes;
  }

  // Use XHR to fetch `file` and interpret its contents as being encoded with `encoding`.
  function fetchAndDecode(file) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', file);
    // Using 'arraybuffer' as the responseType ensures that the raw data is returned,
    // rather than letting XMLHttpRequest decode the data first.
    xhr.responseType = 'arraybuffer';
    xhr.onload = function() {
      if (this.status == 200) {
        // The decode() method takes a DataView as a parameter, which is a wrapper on top of the ArrayBuffer.
        var dataView = new DataView(this.response);
        // The TextDecoder interface is documented at http://encoding.spec.whatwg.org/#interface-textdecoder
        var decoder = new TextDecoder('utf-8');
        var decodedString = decoder.decode(dataView);
        // Add the decoded file's text to the <pre> element on the page.
        callback(decodedString);
      } else {
        console.error('Error while requesting', file, this);
      }
    };
    xhr.send();
  }

  self.updateCurGifReaderFromFile = function(filepath, callback) {
    if (curGifReader && filepath === mostRecentFilepath) return;
    console.log("getting filepath:", filepath);
    $.ajax({
      url: filepath,             
      type: "GET",
      contentType: "image/gif",
      dataType: "text",
      success: function(stringData) {
        mostRecentFilepath = filepath;
        var blob = new Blob([stringData], {type: 'image/gif'});
        var f = new FileReader();
        f.onload = function(e) {
          var buf = e.target.result;
          var gifByteArray = new Uint8Array(buf);
          console.log("gifByteArray", gifByteArray);
          curGifReader = new GifReader(gifByteArray);
          callback();
        }
        f.readAsArrayBuffer(blob);
        var newUrl = window.URL.createObjectURL(blob);
        console.log(newUrl);
        $("#link").href = newUrl;
      }
    });
  }

  // updates the variable curGifReader, then calls the callback
  // forceUpdate is optional and will force an update
  self.updateCurGifReader = function(callback, forceUpdate, startFrame, endFrame) {
    if (!curGifReader || forceUpdate) {
      var gifBytes = self.getGifBytes(startFrame, endFrame);
      var gifByteArray = new Uint8Array(gifBytes);
      // update global curGifReader
      curGifReader = new GifReader(gifByteArray);
      if (callback) callback(gifByteArray);
    }
    else {
      if (callback) callback();
    }
  }

  self.updateEditor = function(callback) {
    var callback = callback;
    self.updateCurGifReader(function() {
      // load first frame of gif in editor
      self.showFrame(0);
      if (callback)
        callback();
    }, true);
  }

  self.showFrame = function(frameNum) {
    var imagedata = editorContext.createImageData(
      $('#video-record').width(), 
      $('#video-record').height());
    curGifReader.decodeAndBlitFrameRGBA(frameNum, imagedata.data);
    editorContext.putImageData(imagedata, 0, 0);
  }

  self.getNumFrames = function() {
    return curGifReader.numFrames();
  }

  // modified from recordRTC function
  self.getLastBlackFrame = function(callback) {
    var localCanvas = document.createElement('canvas');
    localCanvas.width = canvas.width;
    localCanvas.height = canvas.height;
    var context2d = localCanvas.getContext('2d');
    var lastBlackFrame = -1;

    var checkUntilNotBlack = true;
    var endCheckFrame = self.getNumFrames();
    var sampleColor = {
      r: 0,
      g: 0,
      b: 0
    };
    var maxColorDifference = Math.sqrt(
      Math.pow(255, 2) +
      Math.pow(255, 2) +
      Math.pow(255, 2)
    );
    var pixTolerance = 0;
    var frameTolerance = 0;
    var doNotCheckNext = false;

    for (var f = 0; f < endCheckFrame; f++) {
      var matchPixCount, endPixCheck, maxPixCount;

      if (!doNotCheckNext) {
        var imageData = editorContext.createImageData(
          $('#video-record').width(), 
          $('#video-record').height());
        curGifReader.decodeAndBlitFrameRGBA(f, imageData.data);
        
        matchPixCount = 0;
        endPixCheck = imageData.data.length;
        maxPixCount = imageData.data.length / 4;

        for (var pix = 0; pix < endPixCheck; pix += 4) {
          var currentColor = {
            r: imageData.data[pix],
            g: imageData.data[pix + 1],
            b: imageData.data[pix + 2]
          };
          var colorDifference = Math.sqrt(
            Math.pow(currentColor.r - sampleColor.r, 2) +
            Math.pow(currentColor.g - sampleColor.g, 2) +
            Math.pow(currentColor.b - sampleColor.b, 2)
          );
          // difference in color it is difference in color 
          // vectors (r1,g1,b1) <=> (r2,g2,b2)
          if (colorDifference <= maxColorDifference * pixTolerance) {
            matchPixCount++;
          }
        }
      }

      if (!doNotCheckNext && maxPixCount - matchPixCount <= 
                             maxPixCount * frameTolerance) {
        lastBlackFrame = f;
      } else {
        if (checkUntilNotBlack) {
          doNotCheckNext = true;
        }
      }
    }
    if (callback) callback(lastBlackFrame);
    else return lastBlackFrame;
  }

  // removes black frames, sets up
  self.initGif = function(callback) {
    self.updateCurGifReader(function() {
      self.getLastBlackFrame(function(lastBlackFrame) {
        firstFrameNotBlack = lastBlackFrame+1;
        console.log("cut black frames up to", lastBlackFrame);
        self.trimGif(firstFrameNotBlack, self.getNumFrames()-1, 
               function(trimEditorOut) {
          shownStartFrame = firstFrameNotBlack;
          self.showFrame(0);
          callback();
        }, true);
      });
    });
  }

  // trims the gif based on the slider positions
  // permanentTrim as true will update the internal gif representation
  self.trimGif = function(startFrame, endFrame, callback, permanentTrim) {
    console.log("trimGif for range", startFrame, endFrame, "firstFrameNotBlack", firstFrameNotBlack);
    // because slider starts from firstFrameNotBlack
    startFrame = shownStartFrame + startFrame;
    endFrame = shownStartFrame + endFrame;
    console.log("trimGif for actual range", startFrame, endFrame);
    var trimEditorOut = {};
    // permanentTrim makes one call to updateCurGifReader
    if (permanentTrim) {
      editorOut = trimEditorOut;
      self.updateCurGifReader(function(gifBytes, gifByteArray) {
        trimEditorOut.bin = gifBytes;
        trimEditorOut.blob = new Blob([gifByteArray], {
          type: 'image/gif'
        });
        trimEditorOut.url = URL.createObjectURL(trimEditorOut.blob);
        if (callback) callback(trimEditorOut);
      }, true, startFrame, endFrame);
    }
    // otherwise, just grab the data
    else {
      trimEditorOut.bin = self.getGifBytes(startFrame, endFrame);
      trimEditorOut.blob = new Blob([new Uint8Array(trimEditorOut.bin)], {
        type: 'image/gif'
      });
      // never used
      //trimEditorOut.url = URL.createObjectURL(trimEditorOut.blob);
      if (callback) callback(trimEditorOut);
    }
  }

  self.downloadGif = function(blob, filename, callback) {
    saveAs(blob, filename);
    if (callback) callback();
  }

  self.saveGif = function(trimEditorOut, saveDir, filename, callback) {
    console.log("in saveGif", trimEditorOut, saveDir, filename);
    var blobObjInput = {blob: trimEditorOut.blob};
    recorder.getDataURL(function(dataURL) {
      console.log("dataURL", dataURL, "saving gif now...");
      $.ajax({
        type: "POST",
        url: "saveGIF.php",
        data: {
           imgBase64: dataURL,
           filename: filename,
           uploadDir: saveDir
        }
      }).done(function(result) {
        var resultObj = JSON.parse(result);
        console.log("resultObj", resultObj);
        console.log('saved gif:', filename, "result filename:", resultObj.filename);
        possiblyNewFilename = resultObj.filename;
        if (callback) callback(possiblyNewFilename);
      });
    }, blobObjInput);
  }

  self.saveWebm = function(blob, saveDir, filename, callback) {
    var blobObjInput = {blob: blob};
    recorder.getDataURL(function(dataURL) {
      console.log("dataURL", dataURL, "saving webm now...");
      $.ajax({
        type: "POST",
        url: "saveWebm.php",
        data: {
           dataURL: dataURL,
           filename: filename,
           uploadDir: saveDir
        }
      }).done(function(result) {
        var resultObj = JSON.parse(result);
        console.log("resultObj", resultObj);
        console.log('saved gif:', filename, "result filename:", resultObj.filename);
        possiblyNewFilename = resultObj.filename;
        if (callback) callback(possiblyNewFilename);
      });
    }, blobObjInput);
  }

  self.getBlobFromRecorder = function() {
    return recorder.getBlob();
  }

  return self;
}