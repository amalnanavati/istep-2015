// index.js
// all javascript that was in index.html


$(document).ready(function () {
	if ($('html').hasClass('desktop')) {
		new WOW().init();
	}

	if ($('html').hasClass('desktop')) {
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 20,
			resposive: true,
			hideDistantElements: true,
		});
	}

	// show newsletter banner 33% of the time
	var randomChoice = Math.random();
	console.log(randomChoice);
	if (randomChoice > 0.67) {
		$("#blog-banner").hide();
		$("#newsletter-banner").show();
	}

	try {
		$('#touch_gallery a').touchTouch();
	}
	catch(err) {
		console.log("no touch_gallery");
	};

});
