iSTEP 2015 Software Development Repository
Bangalore, India

Members:
Erik Pintar
Amal Nanavati
Minnar Xie
Maya Lassiter

Dates of Internship:
Monday May 25 - Friday July 26, 2015

For more details on each project, view the subdirectories and their respective READMEs and User Guides.

Visit our website for more information about these projects: http://istep2015.techbridgeworld.org
