// code adapted from http://thelivingpearl.com/2013/01/04/drive-a-lamborghini-with-your-keyboard/
// NOTE (amal): the Serial.println was causing the python program on the Lenovo G50-70 laptops to carsh after a while, so I removed them.
int reversePin = 10;
int forwardPin = 8;
int leftPin = 7;
int rightPin = 5;

int order = 55;
int time = 75;

//control flag
int flag = 0;

void forward(int time){
//  Serial.println("This is forward...");
  digitalWrite(forwardPin, LOW);
  delay(time);
  digitalWrite(forwardPin,HIGH);
}

void reverse(int time){
//  Serial.println("This is reverse...");
  digitalWrite(reversePin, LOW);
  delay(time);
  digitalWrite(reversePin,HIGH);
}

void left(int time){
//  Serial.println("This is left...");
  digitalWrite(leftPin, LOW);
  delay(time);
  digitalWrite(leftPin,HIGH);
}

void right(int time){
//  Serial.println("This is right...");
  digitalWrite(rightPin, LOW);
  delay(time);
  digitalWrite(rightPin,HIGH);
}

void leftTurnForward(int time){
  digitalWrite(forwardPin, LOW);
  digitalWrite(leftPin, LOW);
  delay(time);
  off();
}

void rightTurnForward(int time){
  digitalWrite(forwardPin, LOW);
  digitalWrite(rightPin, LOW);
  delay(time);
  off();
}

void leftTurnReverse(int time){
  digitalWrite(reversePin, LOW);
  digitalWrite(leftPin, LOW);
  delay(time);
  off();
}

void rightTurnReverse(int time){
  digitalWrite(reversePin, LOW);
  digitalWrite(rightPin, LOW);
  delay(time);
  off();
}

void off(){
  digitalWrite(reversePin, HIGH);
  digitalWrite(forwardPin, HIGH);
  digitalWrite(leftPin, HIGH);
  digitalWrite(rightPin, HIGH);
}

void orderControl(int order, int time){
  switch (order){
     //off order
     case 0: off(); break;
     
     //movment options
     case 11: forward(time); break;
     case 12: reverse(time); break;
     case 13: right(time); break;
     case 14: left(time); break;

     //complex movment
     case 21: rightTurnForward(time); break;
     case 22: leftTurnForward(time); break;
     case 23: rightTurnReverse(time); break;
     case 24: leftTurnReverse(time); break;

     //no match...
     default: Serial.print("\nINVALID ORDER!... TURNING OFF!\n");
    }
}

void setup() {
  // initialize the digital pins as an output.
  pinMode(rightPin, OUTPUT);
  pinMode(leftPin, OUTPUT);
  pinMode(forwardPin, OUTPUT);
  pinMode(reversePin, OUTPUT);

  Serial.begin(115200);
//  Serial.print("\n\nStart...\n");
}

void loop() {

  //Turn everything off...
  off();

  //get input
  if (Serial.available() > 0){
    order = Serial.read() - 65;
//    Serial.print("I received: ");
//    Serial.println(order);
    flag = 1;
  }

  if(flag){
    //complete orders
    orderControl(order,time);
  }

}


