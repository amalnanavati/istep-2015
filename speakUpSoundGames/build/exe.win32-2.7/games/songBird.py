################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.misc import scaledValueInRange
from tools.button import *

class Bird(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        bird = pygame.image.load('img/bird1.png')
        # Scale the image
        (birdW,birdH) = bird.get_rect().size
        width = 125
        height = width*birdH/birdW
        self.image = pygame.transform.scale(bird, (width, height))
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y-height/2, width, height)
        self.startSpeed = 2
        self.speed = self.startSpeed
        self.dir = 0
        self.speedMultiplier = 1 # acceleration/deceleration

    def update(self, targetY, *args):
        """This method is called every clock tick"""
        if self.rect.y < targetY-self.speed: self.dir = +1 # move down
        elif self.rect.y > targetY+self.speed: self.dir = -1 # move up
        else:
            self.dir = 0 #stay
            self.speed = self.startSpeed
        if (abs(self.rect.y-targetY)/self.speed < 5): self.decelerate()
        print targetY, self.rect.y
        self.move()

    def move(self):
        """Moves the sprite"""
        self.rect.y += int(self.speed*self.dir)
        print "speed", self.speed

    def accelerate(self):
        """Accelerates the rabbit towards the targetY"""
        if self.speed < 2: self.speed = self.startSpeed
        self.speed *= 1.1

    def decelerate(self):
        """Decelerates the rabbit towards targetY"""
        self.speed *= 0.85

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Obstacle(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, speed = 5, width=100, height=200):
        """Initializes an obstacle with the above attributes"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        self.imgPath = random.choice(['img/eagle1.png','img/airplane.png','img/thunderCloud.png', 'img/sun1.png'])
        obstacle = pygame.image.load(self.imgPath)
        # Scale the image
        (obstacleW,obstacleH) = obstacle.get_rect().size
        width = random.randint(150, 200)
        height = width*obstacleH/obstacleW
        self.image = pygame.transform.scale(obstacle, (width, height))
        self.mask = pygame.mask.from_surface(self.image)
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed*-1 # Always move left

    def update(self, targtY, *args):
        """This method is called every clock tick"""
        self.move()
        if self.rect.x < -1*self.rect.width: self.destroy()

    def move(self):
        """Moves the sprite"""
        self.rect.x += self.speed
        if "thunder" in self.imgPath: self.rect.y += random.randint(-5, +5)

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Background(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, height, screenWidth):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        imgPath = 'img/songBirdBackground.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        (spriteW,spriteH) = sprite.get_rect().size
        width = height*spriteW/spriteH
        backgroundImg = pygame.transform.scale(sprite, (width, height))
        # Set the sprit's attributes
        self.numberOfImages = max(int(math.ceil(float(2)*screenWidth/width)), 4)
        print self.numberOfImages
        self.image = pygame.Surface((self.numberOfImages*width, height))
        for i in xrange(self.numberOfImages):
            self.image.blit(backgroundImg, (width*i, 0))
        self.rect = pygame.Rect(x, y, self.numberOfImages*width, height)

    def shiftOverBy(self, dPoint):
        self.rect.x += dPoint[0]
        self.rect.y += dPoint[1]
        if self.rect.x <= -2*self.rect.width/self.numberOfImages: self.rect.x = -self.rect.width/self.numberOfImages

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class SongBird(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH,volumeThreshold=50, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (700, 700)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        self.volumeThreshold = volumeThreshold
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.ticksBeforeNextObstacles = 100 # Changed every time an obstacle is created
        self.ticksBeforeUpdatingScore = self.fps
        self.elapsedTicks = {"obstacle": 90} # Keeps track of elapsed time since events
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Set expected pitch levels
        self.minExpectedPitch = 57
        self.maxExpectedPitch = 75
        self.time = 0
        # Retrive Best Time Info
        try:
            with open('SongBirdBestTime.txt', 'rt') as doc:
                self.bestTime = int(doc.read())
        except IOError:
            self.bestTime = None

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            if "end" in self.elapsedTicks and self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
                self.quit()
        else:
            self.time += 1
            self.background.shiftOverBy((-5, 0))
            if self.elapsedTicks["obstacle"] > self.ticksBeforeNextObstacles:
                self.createObstacle()
            # Calls every sprite's update method
            self.allSpritesList.update(self.targetY)
            # Checks for any collisions
            self.checkCollision()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (pitch != None):
                self.detectedVoice(volume, pitch)
            else: self.bird.decelerate()
        # Update the view
        self.update()

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        scaledY = scaledValueInRange(pitch, self.minExpectedPitch, self.maxExpectedPitch)
        print scaledY
        scaledY = 1-scaledY # Lower pitch moves down (increases y)
        self.targetY = (self.screenHeight-self.bird.rect.height)*scaledY
        self.bird.accelerate()

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                if event.type == QUIT:
                    print "quit"
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.OrderedUpdates()
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        #  Create Bird
        self.createBackground()
        self.createBird()
        # The y position the rabbit is moving towards
        self.targetY = self.screenHeight/2
        # Sets attributes related to obstacles
        self.screenSpeed = 3
        self.distanceBetweenObstacles = self.screenWidth/5

    def createBird(self):
        """Create the flying rabbit"""
        self.bird = Bird(self.screenWidth/20, self.screenHeight/2)
        self.allSpritesList.add(self.bird)

    def createBackground(self):
        self.background = Background(0,0,self.screenHeight, self.screenWidth)
        self.allSpritesList.add(self.background)

    def createObstacle(self):
        """Create the obstacle"""
        # Creates an obstacle with 2/3 chance
        if random.randint(0,2) < 2:
            (width, height) = (random.randint(25, 175), random.randint(1, 200))
            obstacle = Obstacle(self.screenWidth, random.randint(0, self.screenHeight-height),
                                speed=self.screenSpeed, width=width, height=height)
            self.allSpritesList.add(obstacle)
            self.ticksBeforeNextObstacles = (width+self.distanceBetweenObstacles)/self.screenSpeed
            self.elapsedTicks["obstacle"] = 0

    def checkCollision(self):
        """Checks whether an enemy has collided with a bullet and/or with the
        shooter"""
        collided = pygame.sprite.spritecollide(self.bird, self.allSpritesList, False, collided=pygame.sprite.collide_mask)
        if len(collided) > 1:
            self.gameOver()

    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!  Score: %d" %(self.score)
        # self.elapsedTicks["end"] = 0
        self.delayBeforeClosingGame = 3 # seconds
        if self.bestTime is None or self.time > self.bestTime:
            try:
                with open('SongBirdBestTime.txt', 'wt') as doc:
                    doc.write(str(self.time))
            except IOError:
                pass
            isBestTime = True
        else:
            isBestTime = False
        self.createGameOverText(isBestTime=isBestTime)
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Initialize the font
        self.fontHeight = 30
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        # Load the initial view elements
        self.createMenuButton()
        self.updateTimeText()
        self.updateBestTimeText()
        # Display the view
        self.update()

    def updateTimeText(self):
        """Updates the score text with the current score"""
        seconds = int(self.time*self.timeBetweenFrames)
        # print seconds
        minutes = seconds/60
        seconds %= 60
        self.timeText = self.font.render("Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))

    def updateBestTimeText(self):
        if self.bestTime is None:
            self.bestTimeText = self.font.render("Longest Time: "+str(self.bestTime), 1, (0,0,0))
        else:
            seconds = int(self.bestTime*self.timeBetweenFrames)
            # print seconds
            minutes = seconds/60
            seconds %= 60
            self.bestTimeText = self.font.render("Longest Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))

    def createGameOverText(self, isBestTime=False):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        if isBestTime:
            text = "Game Over. Best Time!"
        else:
            text = "Game Over"
        self.gameOverText = font.render(text, 1, (0,0,0))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.allSpritesList.draw(self.surface)
        self.updateTimeText()
        self.surface.blit(self.timeText, (0,0))
        self.surface.blit(self.bestTimeText, (0,self.fontHeight*3/2))
        self.buttonsList.draw(self.surface)
        if self.finished:
            self.surface.blit(self.gameOverText, self.gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/2))
        pygame.display.update()

# game = SongBird(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
