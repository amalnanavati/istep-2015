################################################################################
# A Shooter Game to help hearing-impaired students understand vocalization and
# timed vocalization.
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
from tools.misc import *

class Fruit(pygame.sprite.Sprite):
    """The enemy sprite.  Enemy sprites keep track of their own health and move
    themselves.  Drawing the sprite is done in ShooterGame.update()"""

    def __init__(self, x, y, speed, score, imgPath, width=100, missedFruitCallback=None):
        """Initializes a fruit with the given position, speed, health, width,
        and image.  Speed refers to change in pixels per clock tick, and health
        refers to the number of bullets it takes to destroy the enemy."""

        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        fruit = pygame.image.load(imgPath)
        # Scale the image to the desired width
        (fruitW, fruitH) = fruit.get_rect().size
        height = width*fruitH/fruitW
        image = pygame.transform.scale(fruit, (width, height))
        # Set the sprit's attributes
        self.image = image
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = random.randint(max(speed-1, 1), speed+1) #random speed within a range
        self.score = score
        self.missedFruitCallback = missedFruitCallback

    def update(self, targetX, screenHeight, *args):
        """This method is called every clock tick from
        ShooterGame.onClockTick().  It moves the enemy."""
        self.move()
        if self.rect.y > screenHeight:
            if self.missedFruitCallback is not None: self.missedFruitCallback()
            self.destroy()

    def move(self):
        """Moves the enemy."""
        self.rect.y += self.speed

    def getScore(self):
        """Returns enemy health, called from ShooterGame.checkCollision()"""
        return self.score

    def destroy(self):
        """Destroys the enemy."""
        self.kill()

class Basket(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, speed, width=100, maxSpeed=20):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        basket = pygame.image.load("img/basket.png")
        # Scale the image to the desired width
        (basketW, basketH) = basket.get_rect().size
        height = width*basketH/basketW
        self.image = pygame.transform.scale(basket, (width, height))
        self.rect = pygame.Rect(x-width, y-height, width, height)
        self.startSpeed = speed
        self.speed = self.startSpeed
        self.dir = 0
        self.speedMultiplier = 1 # acceleration/deceleration
        self.maxSpeed = maxSpeed

    def update(self, targetX, *args):
        """This method is called every clock tick"""
        if self.rect.x < targetX-self.speed: self.dir = +1 # move down
        elif self.rect.x > targetX+self.speed: self.dir = -1 # move up
        else:
            self.dir = 0 #stay
            self.speed = self.startSpeed
        if (abs(self.rect.x-targetX)/self.speed < 5): self.decelerate()
        # print targetX, self.rect.x
        self.move()

    def move(self):
        """Moves the sprite"""
        self.rect.x+= int(self.speed*self.dir)
        # print "speed", self.speed

    def accelerate(self, factor=1.1):
        """Accelerates the rabbit towards the targetY"""
        if self.speed < self.startSpeed: self.speed = self.startSpeed
        self.speed *= factor
        if self.speed > self.maxSpeed: self.speed = self.maxSpeed

    def decelerate(self, factor = 0.85):
        """Decelerates the rabbit towards targetY"""
        self.speed *= factor

    def setStartSpeed(self):
        self.speed = self.startSpeed

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class FruitBasketWithArrows(object):
    """The CollectTheFruit.  This class runs the main game loop, manages pygame
    events, controls clock ticks, keeps track of sprites, and draws."""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=60, minExpectedVolume=60, maxExpectedVolume=80, maxMissedFruits=None,  timeLimit=None):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume, maxMissedFruits,  timeLimit)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume, maxMissedFruits,  timeLimit):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1000, 800)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.timeLimit = timeLimit
        self.ticksBetweenFruits = 120 # Clock ticks between enemies (this value changes)
        self.minTicksBetweenFruits = 30 # Min clock ticks between enemies
        self.elapsedTicks = {"fruit":95, "time":0} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Set expected pitch levels
        self.minExpectedVolume = minExpectedVolume
        self.maxExpectedVolume = maxExpectedVolume
        self.prevVoice = False
        self.currVoice = False
        self.currentDirection = 0
        self.maxMissedFruits = maxMissedFruits
        if self.maxMissedFruits is not None or self.timeLimit is not None:
            if self.timeLimit is None:
                self.highscoreFilename = 'FruitBasketWithArrowsHighScore%d.txt' % self.maxMissedFruits
            elif self.maxMissedFruits is None:
                self.highscoreFilename = 'FruitBasketWithArrowsHighScoreAnd%d.txt' % self.timeLimit
            else:
                self.highscoreFilename = 'FruitBasketWithArrowsHighScore%dAnd%d.txt' % (self.maxMissedFruits, self.timeLimit)
            try:
                with open(self.highscoreFilename, 'rt') as doc:
                    self.highScore = int(doc.read())
            except IOError:
                self.highScore = None

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over, wait specified delay and then quit game
        if self.finished:
            if "end" in self.elapsedTicks and self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
                self.quit()
        else:
            if self.timeLimit is not None and int(self.elapsedTicks["time"]*self.timeBetweenFrames) >= self.timeLimit:
                self.gameOver()
                return
            if self.elapsedTicks["fruit"] > self.ticksBetweenFruits:
                # Create an enemy with 2/3 chance
                # if random.randint(0, 2) < 2:
                self.createFruit()
                self.elapsedTicks["fruit"] = 0
            # Calls every sprite's update method
            self.allSpritesList.update(self.targetX, self.screenHeight)
            self.checkCollision()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (volume > self.volumeThreshold or pitch != None):
                self.prevVoice = self.currVoice
                self.currVoice = True
                self.detectedVoice(volume)
            else:
                self.prevVoice = self.currVoice
                self.currVoice = False
                self.noVolume()
            # if self.prevVoice != self.currVoice: # if you change from voice to no voice or voice versa, change start speed
            #     self.basket.setStartSpeed()
        # Update the view
        self.update()

    def detectedVoice(self, volume):
        """Called whenever volume above the volumeThreshold is detected"""
        # Only create a bullet if enough ticks have passed since the last one
        scaledX = scaledValueInRange(volume, self.minExpectedVolume, self.maxExpectedVolume)
        # print scaledX
        minSpeed = 2
        maxSpeed = self.basketMaxSpeed
        targetSpeed = scaledX*(maxSpeed - minSpeed) + minSpeed
        if self.currentDirection == -1:
            self.targetX = 0#(self.screenHeight-self.fish.rect.height)*scaledY
            # self.fish.accelerate(1.0+scaledY**6.0)
        elif self.currentDirection == 1:
            self.targetX = self.screenWidth-self.basket.rect.width
            # self.fish.accelerate(1.0+scaledY**6.0)
        else:
            targetSpeed = 0
        if self.basket.speed < targetSpeed:
            self.basket.accelerate()
        else:
            self.basket.decelerate()

    def noVolume(self):
        print "no voice"
        self.basket.decelerate()
        # self.targetX = 0
        # self.basket.accelerate(1.025) # If you haven't been talking for a while

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            # print "in loop"
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                    elif event.key == K_RIGHT:
                        self.currentDirection = 1
                    elif event.key == K_LEFT:
                        self.currentDirection = -1
                if event.type == KEYUP:
                    if event.key == K_RIGHT and self.currentDirection == 1:
                        self.currentDirection = 0
                    elif event.key == K_LEFT and self.currentDirection == -1:
                        self.currentDirection = 0
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                if event.type == QUIT:
                    print "quit"
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.fruitList = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        self.targetX = self.screenWidth/2
        self.missedFruits = 0
        # Creates the shooter
        self.createBasket()

    def createFruit(self):
        """Creates an enemy, with easier enemies more probable than more
        difficult ones"""
        # seed = random.randint(0, 19)
        images = [(1, 1, 'img/bananas.png'),
                  (1, 1, 'img/apple.png'),
                  (1, 1, 'img/mango.png'),
                  (2, 1, 'img/pineapple.png'),
                  (3, 1, 'img/greenGrapes.png'),
                  (4, 1, 'img/lychees.png'),
                  (2, 1, 'img/guava.png'),
                  (1, 1, 'img/lemon.png'),
                  (5, 1, 'img/papaya.png'),
                  (3, 1, 'img/orange.png'),
                  (5, 1, 'img/peach.png'),
                  (8, 1, 'img/pomengranite.png'),
                  (6, 1, 'img/pear.png'),
                  (4, 1, 'img/plum.png'),
                  (7, 1, 'img/purpleGrapes.png'),
                  (8, 1, 'img/cherries.png'),
                  (10, 1,'img/goldenpineapple.png')]
        (speed, score, imgPath) = random.choice(images)
        speed = random.randint(2, 12)
        score = 1
        # if seed < 8: # Create Banana or Apple with 8/20 chance
        #     if random.randint(0, 1): (speed, score, imgPath) = (1, 1, 'img/bananas.png')
        # else:(speed, score, imgPath) = (1, 1, 'img/apple.png')
        # elif seed < 13: # Create Mango with 1/4 chance
        #     (speed, score, imgPath) = (1, 2, 'img/mango.png')
        # elif seed < 18: # Create Lychee with 1/4 chance
        #     (speed, score, imgPath) = (2, 3, 'img/jackfruit.png')
        # else: # Create Pineapple with 1/10 chance
        #     (speed, score, imgPath) = (2, 4, 'img/pineapple.png')
        width = random.randint(self.screenWidth/18, self.screenWidth/12)
        if self.maxMissedFruits is None:
            callback = None
        else:
            def callback():
                self.missedFruits += 1
                self.updateMissedFruitsText()
                if self.missedFruits >= self.maxMissedFruits:
                    self.gameOver()
        fruit = Fruit(x=random.randint(0, self.screenWidth-width), y=0, missedFruitCallback=callback,
                                     speed=speed, score=score, width=width,
                                     imgPath=imgPath)
        self.fruitList.add(fruit)
        self.allSpritesList.add(fruit)

    def createBasket(self):
        """Loads a shooter sprite and adds it the the spritesList."""
        self.basketMaxSpeed = 40
        self.basket = Basket(self.screenWidth/2, self.screenHeight-20, speed=2, width=self.screenWidth/9, maxSpeed=self.basketMaxSpeed)
        self.allSpritesList.add(self.basket)

    def checkCollision(self):
        """Checks whether an enemy has collided with a bullet and/or with the
        shooter"""
        for fruit in self.fruitList:
            if pygame.sprite.collide_mask(self.basket, fruit):
                self.score += fruit.getScore()
                fruit.destroy()
                # fruits come faster
                self.ticksBetweenFruits = max(0.95*self.ticksBetweenFruits,
                                              self.minTicksBetweenFruits)
                self.updateScoreText()
            if fruit.rect.y > self.screenHeight: # if fruit is off screen
                self.missedFruits += 1
                print "Missed %d Fruits" %(self.missedFruits)

    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!  Score: %d" %(self.score)
        # self.elapsedTicks["end"] = 0
        self.delayBeforeClosingGame = 2 #seconds
        if (self.maxMissedFruits is not None or self.timeLimit is not None) and (self.highScore is None or self.score > self.highScore):
            try:
                with open(self.highscoreFilename, 'wt') as doc:
                    doc.write(str(self.score))
            except IOError:
                pass
            isHighScore = True
        else:
            isHighScore = False
        self.createGameOverText(isHighScore=isHighScore)
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        # self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Initialize the font
        self.fontHeight = 30
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        # Load the view
        self.createMenuButton()
        self.loadBackground()
        self.updateScoreText()
        if self.maxMissedFruits is not None or self.timeLimit is not None:
            if self.maxMissedFruits is not None: self.updateMissedFruitsText()
            self.updateHighScoreText()
        if self.timeLimit is not None: self.updateTimeText()
        # Display the view
        self.update()

    def loadBackground(self):
        """Load the background, an RGB peach color"""
        self.backGroundRGB = (0xCC, 0xFF, 0xFF)
        background = pygame.image.load('img/tree1.png')
        (backgroundW,backgroundH) = background.get_rect().size
        height = self.screenHeight*3/2
        width = height*backgroundW/backgroundH
        self.background = pygame.transform.scale(background, (width, height))

    def updateTimeText(self):
        """Updates the score text with the current score"""
        seconds = self.timeLimit-int(self.elapsedTicks["time"]*self.timeBetweenFrames)
        # print seconds
        minutes = seconds/60
        seconds %= 60
        text = "Remaining Time: %d:%02d" %(minutes, seconds)
        self.timeText = self.font.render(text, 1, (0,0,0))

    def updateScoreText(self):
        """Updates the score text with the current score"""
        self.scoreText = self.font.render("Score: "+str(self.score), 1, (0,0,0))

    def updateMissedFruitsText(self):
        """Updates the score text with the current score"""
        self.missedFruitsText = self.font.render("Missed Fruits: "+str(self.missedFruits)+" / "+str(self.maxMissedFruits), 1, (0,0,0))

    def updateHighScoreText(self):
        self.highScoreText = self.font.render("High Score: "+str(self.highScore), 1, (0,0,0))

    def createGameOverText(self, isHighScore=False):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        if isHighScore:
            text="Game Over. High Score!"
        else:
            text="Game Over"
        self.gameOverText = font.render(text, 1, (0,0,0))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill(self.backGroundRGB)
        self.surface.blit(self.background, self.background.get_rect(centerx=self.screenWidth/2, y=-self.screenHeight/2+20))
        self.allSpritesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        self.surface.blit(self.scoreText, (0, 0))
        if self.maxMissedFruits is not None or self.timeLimit is not None:
            if self.maxMissedFruits is not None:
                self.surface.blit(self.missedFruitsText, (0, self.fontHeight*3/2))
                self.surface.blit(self.highScoreText, (0,self.fontHeight*3))
                if self.timeLimit is not None:
                    if not self.finished: self.updateTimeText()
                    self.surface.blit(self.timeText, (0,self.fontHeight*9/2))
            else:
                self.surface.blit(self.highScoreText, (0,self.fontHeight*3/2))
                if self.timeLimit is not None:
                    if not self.finished: self.updateTimeText()
                    self.surface.blit(self.timeText, (0,self.fontHeight*3))
        if self.finished: self.surface.blit(self.gameOverText, self.gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/2))
        pygame.display.update()

# game = FruitBasketWithArrows(lambda: 1, 1280, 800, maxMissedFruits=3)
# game.run()
# pygame.quit()
# sys.exit()
