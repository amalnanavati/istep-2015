################################################################################
# A meter to help students visualize pitch
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
from tools.misc import scaledValueInRange

class Pitchometer(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, cx, cy, width=None):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Create the surface and the pitchometer
        imgPath = 'img/pitchometer.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        (spriteW,spriteH) = sprite.get_rect().size
        # Scale the image to the desired width
        if width != None:
            height = width*spriteH/spriteW
            pitchometer = pygame.transform.scale(sprite, (width, height))
        else:
            pitchometer = sprite
            (width, height) = (spriteW, spriteH)
        # Set the sprit's attributes
        self.image = pygame.Surface((width,height), flags=pygame.SRCALPHA, depth=32)
        self.image = self.image.convert_alpha()
        self.pitchometer = pitchometer
        self.rect = pygame.Rect(0,0,width, height)
        self.rect.centerx = cx
        self.rect.centery = cy
        if width == None: scaleFactor =  None
        else: scaleFactor = float(width)/spriteW
        # Create the pointer
        self.createPointer(scaleFactor)
        # Draw the pitchometer
        self.drawPitchometer()
        # Draw the pointer
        self.targetCx = height
        self.drawPointer()

    def createPointer(self, scaleFactor):
        self.pointer = pygame.image.load('img/pitchometerPointer.png')
        if scaleFactor != None:
            (pointerW, pointerH) = self.pointer.get_rect().size
            (width, height) = (int(pointerW*scaleFactor), int(pointerH*scaleFactor))
            self.pointer = pygame.transform.scale(self.pointer, (width, height))
        self.pointerRect=self.pointer.get_rect(centerx=self.image.get_rect().centerx, centery=self.image.get_rect().centery)
        print self.pointerRect.x, self.pointerRect.y, self.rect.y, self.rect.height

    def drawPitchometer(self):
        print "drawSpeedometer", self.rect
        self.image.blit(self.pitchometer, (0,0))

    def drawPointer(self):
        print self.pointerRect.x, self.pointerRect.y
        self.image.blit(self.pointer, (self.pointerRect.x, self.pointerRect.y))

    def update(self, scaledPitch, *args):
        """This method is called every clock tick"""
        # 0 is at y=self.rect.y+self.rect.height, 100 is at y=self.image.get_rect().y
        scaledPitch *= 100 # scaledPitch is in (0,1) not in (0, 100)
        (x, w) = (self.rect.x, self.rect.width)
        self.targetCx = int(x+float(w)/(100)*(scaledPitch))
        self.drawPitchometer()
        self.drawPointer()

    def move(self):
        self.pointerRect.centerx += (self.targetCx-self.pointerRect.centerx)/4

    def getColor(self):
        cx = self.pointerRect.centerx
        (x, w) = (self.image.get_rect().x, self.image.get_rect().width)
        scaledPitch = int((cx-x)/float(w)*100)
        # 0 is 0x8bc441, 40 is 0x8bc441, 65 is0xfbed80, 85 is 0xe6721f, 100 is 0xea2127
        if scaledPitch <= 40: (r, g, b) = (0x8b, 0xc4, 0x41)
        elif scaledPitch <= 65:
            r = int(0x8b + float(0xfb-0x8b)/(65-40)*(scaledPitch-40))
            g = int(0xc4 + float(0xed-0xc4)/(65-40)*(scaledPitch-40))
            b = int(0x41 + float(0x80-0x41)/(65-40)*(scaledPitch-40))
        elif scaledPitch <= 85:
            r = int(0xfb + float(0xfb-0xe6)/(85-65)*(scaledPitch-65))
            g = int(0xed + float(0xed-0x72)/(85-65)*(scaledPitch-65))
            b = int(0x80 + float(0x80-0x1f)/(85-65)*(scaledPitch-65))
        elif scaledPitch <= 100:
            r = int(0xe6 + float(0xe6-0xea)/(100-85)*(scaledPitch-85))
            g = int(0x72 + float(0x72-0x21)/(100-85)*(scaledPitch-85))
            b = int(0x1f + float(0x1f-0x27)/(100-85)*(scaledPitch-85))
        r = min(0xff, r)
        g = min(0xff, g)
        b = min(0xff, b)
        return (r, g, b)


    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class PitchometerGame(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=50, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (800, 800)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # print defaultW, defaultH, widthHeightRatio, currentW, currentH, actualRatio
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        print self.screenWidth, self.screenHeight
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.elapsedTicks = {} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN, 32)
        pygame.display.set_caption('Shoot the Enemies')

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            pass
        else:
            self.pitchometer.move()
            # Listen for audio
            (minPitch, maxPitch) = (57, 75)
            (volume, pitch) = self.listener.listen()
            if pitch == None: pitch = (minPitch+maxPitch)/2
            scaledPitch = scaledValueInRange(pitch, minPitch, maxPitch)
            self.allSpritesList.update(scaledPitch)
            self.color = self.pitchometer.getColor()
        # Update the view
        self.update()

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.buttonsList = pygame.sprite.Group()
        self.allSpritesList = pygame.sprite.Group()
        # Create the pitchometer
        self.createPitchometer()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0

    def createPitchometer(self):
        self.pitchometer = Pitchometer(self.screenWidth/2, self.screenHeight/2, width=self.screenWidth)
        self.allSpritesList.add(self.pitchometer)

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Load the view elements
        self.loadView()
        self.color = (0xFF, 0xFF, 0xFF)
        # Create Bird
        self.createBird()
        # Display the view
        self.update()

    def createBird(self):
        pass

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill(self.color)
        self.allSpritesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = PitchometerGame(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
