################################################################################
# Fruit Tree sound game for Sound Games suite
# Using template.py for sound game development
#
# Authors:
#       - Maya Lassiter (mlassite@andrew.cmu.edu)
#
# Copyright 2015 TechBridgeWorld
################################################################################

from template import *


class Tree(Token):
    def __init__(self, width, cx, cy, speed = 6, imgPath = 'img/tree1.png'):
        """Initializes tree Token"""
        Token.__init__(self, width=width, x=cx, y=cy, speed = 3, imgPath = imgPath)
        self.rect.y -= self.rect.height/2
        self.rect.x -= self.rect.width/2
        self.defaultx = self.rect.x
        self.defaulty = self.rect.y

    def move(self, screenWidth):
        """Creates a shake effect with the Tree"""
        dx, dy = random.randint(-3, 3), random.randint(-1, 1)
        self.rect.x = self.defaultx + dx
        self.rect.y = self.defaulty + dy

    def update(self, screenWidth, *args):
        """This method is called every clock tick"""
        self.move(screenWidth)

class Fruit(Enemy):
    def __init__(self, x, y, health, width, imgPath):
        Enemy.__init__(self, x=x, y=y, speed = 5, health=health, width=width, imgPath=imgPath)
        self.health = random.randint(max(health-1, 1), health+1)
        self.firstImage = pygame.image.load(imgPath)

    def gotHit(self):
        if self.health < 0:
            return
        else:
            self.health -= 1
            x,y = self.rect.x, self.rect.y
            self.image = pygame.transform.smoothscale(self.firstImage, (int(round(self.rect.width*1.07)), int(round(self.rect.height*1.07))))
            self.rect = self.image.get_rect(centerx = self.rect.centerx, centery = self.rect.centery)
            self.mask = pygame.mask.from_surface(self.image)


    def move(self):
        """Only fall if the health has been diminished"""
        tick = 0
        if self.health < 0:
            tick += 1
            self.rect.y += self.speed * (tick*tick)

class fallingFruit(Game):
    """ Falling Fruit game """
    def __init__(self, returnToMenu, currentWidth, currentHeight, volumeThreshold=65, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(volumeThreshold, currentWidth, currentHeight)
        self.pitch = None
        self.maxVolume = 80
        self.initModel()
        self.initView()

    def setClockRelated(self):
        self.ticksBetweenEnemies = 50
        self.elapsedTicks = {'enemy':10}

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.enemiesList = pygame.sprite.Group()
        self.listOfEnemies = []
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        #create player game token
        self.createToken()

    def createToken(self):
        self.leaves = Tree(width=800, cx=self.screenWidth/2, cy=self.screenHeight/2-125, speed=6, imgPath = 'img/leaves.png')
        self.trunk = Tree(width=450, cx=self.screenWidth/2, cy=self.screenHeight/2+175, speed=6, imgPath = 'img/treetrunk.png')
        self.allSpritesList.add(self.leaves)
        self.allSpritesList.add(self.trunk)

    def createEnemy(self):
        # images = [(15, 5, 25, 'img/mango.png'),
        #           (15, 5, 33, 'img/pineapple.png'),
        #           (15, 5, 25, 'img/mango.png'),
        #           (15, 5, 25, 'img/apple.png'),
        #           (15, 5, 42, 'img/goldenpineapple.png'),
        #           (15, 5, 20, 'img/cherries.png'),
        #           (15, 5, 28, 'img/jackfruit.png')]
        images = [('img/bananas.png'),
                  ('img/apple.png'),
                  ('img/mango.png'),
                  ('img/pineapple.png'),
                  ('img/greenGrapes.png'),
                  ('img/lychees.png'),
                  ('img/guava.png'),
                  ('img/lemon.png'),
                  ('img/papaya.png'),
                  ('img/orange.png'),
                  ('img/peach.png'),
                  ('img/pomengranite.png'),
                  ('img/pear.png'),
                  ('img/plum.png'),
                  ('img/purpleGrapes.png'),
                  ('img/cherries.png'),
                  ('img/goldenpineapple.png')]
        (imgPath) = random.choice(images)
        width = 15
        speed = random.randint(3, 7)
        health = random.randint(20, 40)
        # seed = random.randint(0, 60)
        # if seed < 10:
        #     (width, speed, health, imgPath) = (15, 5, 25, 'img/mango.png')
        # elif seed < 20:
        #     (width, speed, health, imgPath) = (15, 5, 33, 'img/pineapple.png')
        # elif seed < 30:
        #     (width, speed, health, imgPath) = (15, 5, 25, 'img/mango.png')
        # elif seed < 40:
        #     (width, speed, health, imgPath) = (15, 5, 25, 'img/apple.png')
        # elif seed == 42 or self.pitch > 70:
        #     (width, speed, health, imgPath) = (15, 5, 42, 'img/goldenpineapple.png')
        # elif seed < 50:
        #     (width, speed, health, imgPath) = (15, 5, 20, 'img/cherries.png')
        # else:
        #     (width, speed, health, imgPath) = (15, 5, 28, 'img/jackfruit.png')
        y = random.randint(0, (self.leaves.rect.height/2))
        x = random.randint(0, self.leaves.rect.width) if y > self.leaves.rect.height/4 else random.randint(self.leaves.rect.width/3, (2*self.leaves.rect.width)/3)

        fruit = Fruit(x = x+self.leaves.rect.x, y = y+self.leaves.rect.y, width = width,
                        health = health, imgPath = imgPath)
        self.enemiesList.add(fruit)
        self.listOfEnemies.append(fruit)
        self.allSpritesList.add(fruit)

    def sendUpdates(self):
        self.enemiesList.update()
        (volume, pitch) = self.listener.listen()
        if (volume > self.volumeThreshold or pitch != None):
                self.detectedVoice(volume, pitch)

    def detectedVoice(self, volume, pitch):
        self.pitch = pitch
        scale = volume/self.maxVolume
        if scale > 1:
            scale = 1
        effect = int(scale*len(self.enemiesList))
        self.leaves.update(self.screenWidth)
        for fruit in self.listOfEnemies[:effect]:
            fruit.gotHit()

    def removeEnemy(self):
        """Remove enemies after they fall off of the screen"""
        for fruit in self.enemiesList:
            if fruit.rect.y > self.screenHeight:
                fruit.destroy()
                self.listOfEnemies.remove(fruit)


    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()
        #start with some fruit in tree
        self.createEnemy()
        self.createEnemy()

    def update(self):
        """Updates the view, by drawing the back`ground, sprites, and score"""
        self.surface.fill((0xF1, 0xCC, 0x66))
        self.allSpritesList.draw(self.surface)
        self.enemiesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# fallingFruit = fallingFruit(lambda: 1, 1000, 1000, 55)
# fallingFruit.run()
# pygame.quit()
# sys.exit()
