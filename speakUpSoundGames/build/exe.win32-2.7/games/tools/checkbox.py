import pygame

class Checkbox(pygame.sprite.Sprite):
    """A pygame Sprite that functions like a checkbox"""

    def __init__(self, x, y,
                 callback=lambda x, y: Checkbox.defaultCallback(x), text="Checkbox", updateDrawing=lambda: Checkbox.defaultUpdateDrawing(), isChecked=False,
                 width=200, height=50, lineThickness=5, whiteSpace=10, callbackArgument={}, checkmarkPath='games/tools/checkmark.png', fontSize=30):
        """Initializes the button"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set the font
        self.font = pygame.font.SysFont("Arial", fontSize)
        self.isChecked = isChecked
        # Load the surface
        self.x = x
        self.y = y
        self.text = text
        self.width = width
        self.height = height
        self.lineThickness = lineThickness
        self.whiteSpace = whiteSpace
        self.check = None
        self.checkmarkPath = checkmarkPath
        self.updateDrawing = updateDrawing
        if self.isChecked: self.createCheckedCheckbox()
        else: self.createUncheckedCheckbox()
        # Set the callback
        self.callback = callback
        # Sets the game data
        self.callbackArgument = callbackArgument

    def createUncheckedCheckbox(self):
        # Create the Surface
        self.image = pygame.Surface((self.width, self.height))
        self.image.fill((0xFF, 0xFF, 0xFF))
        # Load the checkbox rectangle
        outerRect = pygame.Surface((min(self.image.get_rect().width, self.image.get_rect().height), min(self.image.get_rect().width, self.image.get_rect().height)))
        outerRect.fill((0x00, 0x00, 0x00))
        self.image.blit(outerRect, outerRect.get_rect(x=self.image.get_rect().x, y=self.image.get_rect().y))
        innerRect = pygame.Surface((outerRect.get_rect().width-2*self.lineThickness, outerRect.get_rect().height-2*self.lineThickness))
        innerRect.fill((0xFF, 0xFF, 0xFF))
        self.image.blit(innerRect, innerRect.get_rect(x=self.image.get_rect().x+self.lineThickness, y=self.image.get_rect().y+self.lineThickness))
        # Load text
        text = self.font.render(self.text, 1, (0x00,0x00,0x00))
        self.image.blit(text, text.get_rect(x=self.image.get_rect().x + outerRect.get_rect().width + self.whiteSpace, centery=self.image.get_rect().centery))
        # Set the spritw's attributes
        self.rect = self.image.get_rect(x=self.x, y=self.y)

    def createCheckedCheckbox(self):
        # Create the Surface
        self.image = pygame.Surface((self.width, self.height))
        self.image.fill((0xFF, 0xFF, 0xFF))
        # Load the checkbox rectangle
        outerRect = pygame.Surface((min(self.image.get_rect().width, self.image.get_rect().height), min(self.image.get_rect().width, self.image.get_rect().height)))
        outerRect.fill((0x00, 0x00, 0x00))
        self.image.blit(outerRect, outerRect.get_rect(x=self.image.get_rect().x, y=self.image.get_rect().y))
        innerRect = pygame.Surface((outerRect.get_rect().width-2*self.lineThickness, outerRect.get_rect().height-2*self.lineThickness))
        innerRect.fill((0xFF, 0xFF, 0xFF))
        self.image.blit(innerRect, innerRect.get_rect(x=self.image.get_rect().x+self.lineThickness, y=self.image.get_rect().y+self.lineThickness))
        # Load text
        text = self.font.render(self.text, 1, (0x00,0x00,0x00))
        self.image.blit(text, text.get_rect(x=self.image.get_rect().x + outerRect.get_rect().width + self.whiteSpace, centery=self.image.get_rect().centery))
        # Load the checkmark if it has not already been loaded
        if self.check == None:
            image = pygame.image.load(self.checkmarkPath)
            # Scale the image
            (imageW,imageH) = image.get_rect().size
            imageWidth = outerRect.get_rect().width
            imageHeight = imageWidth*imageH/imageW
            # print margin, imageW, imageH, width, height, imageWidth, imageHeight
            image = pygame.transform.scale(image, (imageWidth, imageHeight))
            self.check = image
        self.image.blit(self.check, self.check.get_rect(x=innerRect.get_rect().x, y=innerRect.get_rect().y))
        # Set the sprite's attributes
        self.rect = self.image.get_rect(x=self.x, y=self.y)

    def update(self, x0, y0, *args):
        """Called every mouse click"""
        (x, y, w, h) = (self.rect.x, self.rect.y, self.rect.width, self.rect.height)
        print x0,y0,x,y,w,h
        if x0 > x and x0 < x + w and y0 > y and y0 < y + h:
            print "Checkbox Clicked!"
            if self.isChecked:
                self.isChecked = False
                print "Uncheck"
                self.createUncheckedCheckbox()
            else:
                self.isChecked = True
                print "Check"
                self.createCheckedCheckbox()
            self.updateDrawing()
            self.callback(self.isChecked, self.callbackArgument)

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    @staticmethod
    def defaultCallback(x, y):
        print "Checkbox Clicked!"

    @staticmethod
    def defaultUpdateDrawing():
        print "Update Drawing!"
