################################################################################
# A basic game where student voice controls a robotic car
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2017 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
import serial.tools.list_ports

class StateChangingSprite(pygame.sprite.Sprite):
    """RoboticCarBasicGame StateChangingSprite class description"""

    def __init__(self, centerx, centery, maxWidth, maxHeight, imagePaths={"UP": "img/UP.png", "DOWN": "img/DOWN.png", "UPRIGHT": "img/UPRIGHT.png", "UPLEFT": "img/UPLEFT.png", "DOWNRIGHT": "img/DOWNRIGHT.png", "DOWNLEFT": "img/DOWNLEFT.png", "STOP": "img/STOP.png"}, startingState="STOP"):
        """Initializes StateChangingSprite"""
        # Initialize the sprite
        #print x, y, speed, imgPath
        pygame.sprite.Sprite.__init__(self)
        #imgPath = 'img/'
        # Load all the images
        self.images = {}
        maxImgWidth, maxImgHeight = 0, 0
        for key, path in imagePaths.iteritems():
            image = pygame.image.load(path)
            # Scale the image to the desired width
            (imageW,imageH) = image.get_rect().size
            scaleFactor = min(float(maxWidth)/imageW, float(maxHeight)/imageH)
            imgWidth, imgHeight = int(imageW*scaleFactor), int(imageH*scaleFactor)
            image = pygame.transform.scale(image, (imgWidth, imgHeight))
            if maxImgWidth < imgWidth: maxImgWidth = imgWidth
            if maxImgHeight < imgHeight: maxImgHeight = imgHeight
            self.images[key] = image
        # Create the image
        self.currentState = startingState
        self.centerx, self.centery = centerx, centery
        self.image = pygame.Surface((maxImgWidth, maxImgHeight))
        self.image.fill((0xFF, 0xFF, 0xFF))
        self.image.blit(self.images[self.currentState], self.images[self.currentState].get_rect(centerx=self.image.get_rect().width/2, centery=self.image.get_rect().height/2))
        # Set the sprit's attributes
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect(centerx=centerx, centery=centery)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def changeState(self, state):
        if state != self.currentState:
            self.currentState = state
            self.image.fill((0xFF, 0xFF, 0xFF))
            self.image.blit(self.images[self.currentState], self.images[self.currentState].get_rect(centerx=self.image.get_rect().width/2, centery=self.image.get_rect().height/2))

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class RoboticCarBasicGame(object):
    """The RoboticCarBasicGame"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=70, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(volumeThreshold, currentW, currentH)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, volumeThreshold, currentW, currentH):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1000, 1000)
        # defaultRatio = float(defaultW)/defaultH
        # actualRatio = float(currentWidth)/currentHeight
        # if actualRatio > defaultRatio:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        # else:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.setClockRelated()
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyRoboticCarBasicGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Create serial connection
        ser = self.connectToArduinoSerial("Arduino Uno")
        if ser is None:
            self.isArduinoPluggedIn = False
            self.createNotConnectedToArduinoButtonAndText()
        else:
            self.isArduinoPluggedIn = True
            self.serial = ser

    def setClockRelated(self):
        self.elapsedTicks = {} # Keeps track of elapsed time since events

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            pass
        elif self.isArduinoPluggedIn:
            self.sendUpdates()
        # Update the view
        self.update()

    def sendUpdates(self):
        if self.currentDirection != self.previousDirection and self.currentDirection == self.directionsMap["STOP"]:
            print "ASDF", self.currentDirection, self.previousDirection
            self.serial.write(chr(self.currentDirection + 65))
            self.previousDirection = self.currentDirection
        (volume, pitch) = self.listener.listen()
        if (volume > self.volumeThreshold or pitch != None):
            self.detectedVoice(volume, pitch)
        else:
            self.currentOnOFF = False
            print "QWER", self.currentDirection, self.previousDirection
            self.serial.write(chr(self.directionsMap["STOP"] + 65))
            self.previousDirection = self.directionsMap["STOP"]

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        self.currentOnOFF = True
        if self.previousDirection is not None and self.currentDirection != self.previousDirection:
            print "ZXCV", self.currentDirection, self.previousDirection
            self.serial.write(chr(self.currentDirection + 65))
            self.previousDirection = self.currentDirection

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def connectToArduinoSerial(self, keyword):
        ports = list(serial.tools.list_ports.comports())
        print ports
        for p in ports:
            print p
            print(repr(p))
        try:
            port = next(serial.tools.list_ports.grep(keyword))
            print port
            print repr(port)
            ser = serial.Serial(port=port.device, baudrate=115200, timeout=1)
            return ser
        except StopIteration:
            return None

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            # TODO (amal): add all the robotic car events here
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                    if self.isArduinoPluggedIn:
                        keyinput = pygame.key.get_pressed();
                        # complex orders
                        if keyinput[K_UP] and keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["UPRIGHT"]
                        elif keyinput[K_UP] and keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["UPLEFT"]
                        elif keyinput[K_DOWN] and keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["DOWNRIGHT"]
                        elif keyinput[K_DOWN] and keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["DOWNLEFT"]

                        #simple orders
                        elif keyinput[K_UP]:

                            self.currentDirection = self.directionsMap["UP"]
                        elif keyinput[K_DOWN]:

                            self.currentDirection = self.directionsMap["DOWN"]
                        elif keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["RIGHT"]
                        elif keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["LEFT"]

                elif event.type == pygame.KEYUP:
                    if self.isArduinoPluggedIn:
                        #single key
                        if ((self.currentDirection == self.directionsMap["UP"] and event.key == K_UP) or
                            (self.currentDirection == self.directionsMap["DOWN"] and event.key == K_DOWN) or
                            (self.currentDirection == self.directionsMap["RIGHT"] and event.key == K_RIGHT) or
                            (self.currentDirection == self.directionsMap["LEFT"] and event.key == K_LEFT)):

                            self.currentDirection = self.directionsMap["STOP"]

                        #up-right
                        elif (self.currentDirection == self.directionsMap["UPRIGHT"]):
                            if event.key == K_RIGHT:

                                self.currentDirection = self.directionsMap["UP"]
                            elif event.key == K_UP:

                                self.currentDirection = self.directionsMap["RIGHT"]

                        #up-left
                        elif (self.currentDirection == self.directionsMap["UPLEFT"]):
                            if event.key == K_LEFT:

                                self.currentDirection = self.directionsMap["UP"]
                            elif event.key == K_UP:

                                self.currentDirection = self.directionsMap["LEFT"]

                        #back-right
                        elif (self.currentDirection == self.directionsMap["DOWNRIGHT"]):
                            if event.key == K_RIGHT:

                                self.currentDirection = self.directionsMap["DOWN"]
                            elif event.key == K_DOWN:

                                self.currentDirection = self.directionsMap["RIGHT"]

                        #back-left
                        elif (self.currentDirection == self.directionsMap["DOWNLEFT"]):
                            if event.key == K_LEFT:

                                self.currentDirection = self.directionsMap["DOWN"]
                            elif event.key == K_DOWN:

                                self.currentDirection = self.directionsMap["LEFT"]

                elif event.type == QUIT:
                    if self.isArduinoPluggedIn: self.serial.close()
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                elif event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                    if not self.isArduinoPluggedIn: self.connectButtonList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.arrowList = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        # TODO (amal): score I don't need, time I should have, and a distance measure
        self.score = 0
        #create player game arrow
        self.createStateChangingSprites()
        # store the current direction the car will go if provided voice, based on the arrow keys
        # NOTE (amal): If you change the directions mapping below, you must correspondingly change the arduino code for the robotic car
        self.directionsMap = {"STOP": 0, "UP": 11, "DOWN": 12, "RIGHT": 13, "LEFT": 14, "UPRIGHT": 21, "UPLEFT": 22, "DOWNRIGHT": 23, "DOWNLEFT": 24}
        self.currentDirection = self.directionsMap["STOP"]
        self.previousDirection = None
        # Set current on off stage
        self.currentOnOFF = False # Off


    def createStateChangingSprites(self):
        """Creates a player arrow and adds it to the Sprite List"""
        self.arrow = StateChangingSprite(centerx=self.screenHeight/2, centery=self.screenHeight/2, maxWidth=self.screenHeight, maxHeight=self.screenHeight)
        self.onoff = StateChangingSprite(centerx=(self.screenWidth-self.screenHeight)/2+self.screenHeight, centery=self.screenHeight/2, maxWidth=(self.screenWidth-self.screenHeight/2), maxHeight=self.screenHeight/4, imagePaths={"ON": "img/ON.png", "OFF": "img/OFF.png"}, startingState="OFF")
        self.arrowList.add(self.arrow)
        self.arrowList.add(self.onoff)

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Sound RoboticCarBasicGame Template')
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()
        # self.drawStateChangingSprite()

    def setCurrentDirectionArrowAndOnOff(self):
        if self.currentDirection == self.directionsMap["STOP"]:
            self.arrow.changeState("STOP")
        elif self.currentDirection == self.directionsMap["UP"]:
            self.arrow.changeState("UP")
        elif self.currentDirection == self.directionsMap["DOWN"]:
            self.arrow.changeState("DOWN")
        elif self.currentDirection == self.directionsMap["RIGHT"]:
            self.arrow.changeState("STOP")
        elif self.currentDirection == self.directionsMap["LEFT"]:
            self.arrow.changeState("STOP")
        elif self.currentDirection == self.directionsMap["UPRIGHT"]:
            self.arrow.changeState("UPRIGHT")
        elif self.currentDirection == self.directionsMap["UPLEFT"]:
            self.arrow.changeState("UPLEFT")
        elif self.currentDirection == self.directionsMap["DOWNRIGHT"]:
            self.arrow.changeState("DOWNRIGHT")
        elif self.currentDirection == self.directionsMap["DOWNLEFT"]:
            self.arrow.changeState("DOWNLEFT")
        if self.currentOnOFF:
            self.onoff.changeState("ON")
        else:
            self.onoff.changeState("OFF")

    def createNotConnectedToArduinoButtonAndText(self):
        # Create Text
        fontHeight = self.screenWidth/30
        font = pygame.font.SysFont("Arial", fontHeight)
        self.notConnectedTextLine1 = font.render("Could not detect remote control.", 1, (0,0,0))
        self.notConnectedTextLine2 = font.render("Plug it in, power it on, and press Connect.", 1, (0,0,0))
        # Create Button
        self.connectButtonList = pygame.sprite.Group()
        margin = 20
        (width, height) = (150, 50)
        (x,y) = (self.screenWidth/2-width/2, self.screenHeight*4/5-height/2)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        def callback(*data):
            ser = self.connectToArduinoSerial("Arduino Uno")
            if ser is not None:
                self.isArduinoPluggedIn = True
                self.serial = ser
                self.connectButtonList = pygame.sprite.Group()
        connect = Button(x, y, color=color, width=width, height=height,
                 callback=callback, text="Connect")
        self.connectButtonList.add(connect)

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.allSpritesList.draw(self.surface)
        if self.isArduinoPluggedIn:
            print self.currentDirection, self.previousDirection
            self.setCurrentDirectionArrowAndOnOff()
            self.arrowList.draw(self.surface)
        else:
            margin = 20
            self.surface.blit(self.notConnectedTextLine1, self.notConnectedTextLine1.get_rect(centerx=self.screenWidth/2, y=self.screenHeight/2-self.notConnectedTextLine1.get_rect().height-margin/2))
            self.surface.blit(self.notConnectedTextLine2, self.notConnectedTextLine2.get_rect(centerx=self.screenWidth/2, y=self.screenHeight/2+margin/2))
            self.connectButtonList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = RoboticCarBasicGame(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
