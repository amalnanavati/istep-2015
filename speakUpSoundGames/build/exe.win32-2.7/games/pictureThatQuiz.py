################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
#       - Maya Lassiter (mlassite@andrew.cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

# TODO (amal): add options for which to display first: picture, word, together
# then add option for spelling or not
# for not spelling, teacher verify correct.  For spelling, the game checks, and gives x chances.
# correct should display the other thing and a checkmark, but wrong should just display an x and only after the number of tries, it should display the answer
# display score, lives, and num tries!
# stop the game after it has cycled through all words!!
# add an ordering (teacher-defined) of the words!!!!
# add a play again button at the end, so the teacher doesn't have to reconfigure the whole game (do this for all games?)
# Todo: add stars/rewards

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
from tools.textInput import TextInput
from tools.GIFImage import *

class Image(pygame.sprite.Sprite):
    """Game Token class description"""

    # displayMode: 0 = word first, 1 = picture first, 2 = word and picture together
    def __init__(self, x, y, width, height, imgPath, caption, displayMode):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set the Sprite Attributes
        self.displayMode = displayMode
        self.imgPath = imgPath
        self.width, self.height = width, height
        # Determine text placement
        self.margin = height/20
        self.fontHeight = 50
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        self.pictureTitle = self.font.render(caption, 1, (0,0,0))
        self.pictureTitleRect = self.pictureTitle.get_rect(centerx=width/2, centery=self.margin+self.fontHeight/2)
        # Determine picture Placement
        topMargin = self.margin + self.fontHeight + 2*self.margin # Jank fix so the image is below the instructions
        bottomMargin, leftMargin, rightMargin = 2*self.margin, self.margin, self.margin
        self.pictureWidth, self.pictureHeight = (width-leftMargin-rightMargin), (height-bottomMargin-topMargin)
        self.pictureCenterx, self.pictureCentery = self.pictureWidth/2+leftMargin, self.pictureHeight/2+topMargin
        if self.imgPath is not None:
            if ".gif" in imgPath:
                image = pygame.Surface((width, height))
                image.fill((0xFF, 0xFF, 0xFF))
                print "imgPath: ", imgPath
                img = GIFImage(imgPath, self.pictureWidth, self.pictureHeight)
                img.render(image, (width/2, height/2))
                self.picture = image
            else:
                image = pygame.Surface((width, height))
                image.fill((0xFF, 0xFF, 0xFF))
                img = pygame.image.load(imgPath)
                # Scale the image to the desired width
                (imageW,imageH) = img.get_rect().size
                scaleFactor = min(float(self.pictureWidth)/imageW, float(self.pictureHeight)/imageH)
                imgWidth, imgHeight = int(imageW*scaleFactor), int(imageH*scaleFactor)
                img = pygame.transform.scale(img, (imgWidth, imgHeight))
                image.blit(img, img.get_rect(centerx=width/2, centery=height/2))
                self.picture = image
            # # Load the image
            # self.picture = pygame.image.load(self.imgPath)
            # # Scale the image to the desired width
            # (imageW,imageH) = self.picture.get_rect().size
            # imageScaleFactor = min(float(self.pictureWidth)/imageW, float(self.pictureHeight)/imageH)
            # self.picture = pygame.transform.scale(self.picture, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        # Create the image and rect
        self.image = pygame.Surface((width, height))
        self.image.fill((0xFF, 0xFF, 0xFF))
        self.rect = pygame.Rect(0, 0, width, height)
        if self.displayMode == 1 or self.displayMode == 2:
            if self.imgPath is not None:
                # Display the image
                self.image.blit(self.picture, self.picture.get_rect(centerx=self.pictureCenterx, centery=self.pictureCentery))
        if self.displayMode == 0 or self.displayMode == 2:
            # Display the text
            self.image.blit(self.pictureTitle, self.pictureTitleRect)
        # Create CorrectWrong
        self.correctWrongGroup = pygame.sprite.Group()
        width, height = self.fontHeight, self.fontHeight
        x, y = self.width*45/64, self.height*9/64
        self.correctWrong = CorrectWrong(x, y, width, height, baseColor=(0xFF, 0xFF, 0xFF), correctImgPath="img/checkmark.png", wrongImgPath="img/xmark.png")
        self.correctWrongGroup.add(self.correctWrong)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    # If we are displaying the word only or the image only, additionally
    # display the other one
    def completeDisplay(self):
        if self.displayMode == 0:
            if self.imgPath is not None:
                # Display the image
                self.image.blit(self.picture, self.picture.get_rect(centerx=self.pictureCenterx, centery=self.pictureCentery))
                self.image.blit(self.pictureTitle, self.pictureTitleRect)
        elif self.displayMode == 1:
            # Display the text
            self.image.blit(self.pictureTitle, self.pictureTitleRect)

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    def setCorrect(self):
        self.correctWrong.setCorrect()
        self.correctWrongGroup.draw(self.image)

    def setWrong(self):
        self.correctWrong.setWrong()
        self.correctWrongGroup.draw(self.image)

    def setNeutral(self):
        self.correctWrong.setNeutral()
        self.correctWrongGroup.draw(self.image)

class CorrectWrong(pygame.sprite.Sprite):
    """Game Token class description"""

    def __init__(self, x, y, width, height, baseColor=(0xFF, 0xFF, 0xFF), correctImgPath="img/checkmark.png", wrongImgPath="img/xmark.png"):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        self.baseColor = baseColor
        # Load the image
        image = pygame.Surface((width, height))
        image.fill(self.baseColor)
        # Load the correct and wrong images
        self.correctImage = pygame.image.load(correctImgPath)
        # Scale the image to the desired width
        (imageW,imageH) = image.get_rect().size
        imageScaleFactor = min(float(width)/imageW, float(height)/imageH)
        self.correctImage = pygame.transform.scale(self.correctImage, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        self.wrongImage = pygame.image.load(wrongImgPath)
        # Scale the image to the desired width
        (imageW,imageH) = image.get_rect().size
        imageScaleFactor = min(float(width)/imageW, float(height)/imageH)
        self.wrongImage = pygame.transform.scale(self.wrongImage, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    def setCorrect(self):
        self.image.fill(self.baseColor)
        self.image.blit(self.correctImage, self.correctImage.get_rect(x=0, y=0))

    def setWrong(self):
        self.image.fill(self.baseColor)
        self.image.blit(self.wrongImage, self.correctImage.get_rect(x=0, y=0))

    def setNeutral(self):
        self.image.fill(self.baseColor)

class PictureThatQuiz(object):
    """The Game"""

    # displayMode: 0 = word first, 1 = picture first, 2 = word and picture together
    # quizMode: 0 = give the word sign, 1 = give the sign spelling, 2 = say the word, 3 = type the word
    def __init__(self, returnToMenu, currentWidth, currentHeight, volumeThreshold=60, pictureList=[], displayMode=0, quizMode=0, numTries=3, lives=5):
        self.returnToMenu = returnToMenu
        self.initController(currentWidth, currentHeight, volumeThreshold, quizMode)
        self.initModel(pictureList, displayMode, numTries)#, lives)
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, quizMode):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1200, 900)
        # defaultRatio = float(defaultW)/defaultH
        # actualRatio = float(currentWidth)/currentHeight
        # if actualRatio > defaultRatio:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = int(defaultRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentWidth, defaultW)
        #     self.screenHeight = int(self.screenWidth/defaultRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH

        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.setClockRelated()
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        # self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        # self.listener.startStream()
        # Set the correct and wrong keys
        self.correctKey = ("r", K_r)
        self.wrongKey = ("w", K_w)
        # Set the Quiz type
        self.quizMode = quizMode
        if self.quizMode == 3: self.textInput = TextInput() # TODO (amal): configure this!!!

    def setClockRelated(self):
        self.elapsedTicks = {"time": 0} # Keeps track of elapsed time since events

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            # TODO (amal): probably dont automatically quit, instead display a play again button
            # if self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
            #     self.quit()
            pass
        else:
            self.sendUpdates()
        # Update the view
        self.update()

    def sendUpdates(self):
        #To make sprite updates depend on volume, move this line to detectedVoice
        #self.allSpritesList.update(self.screenWidth, 50)
        # (volume, pitch) = self.listener.listen()
        # if (volume > self.volumeThreshold or pitch != None):
        #         self.detectedVoice()
        pass

    def detectedVoice(self):
        """Called whenever volume above the volumeThreshold is detected"""
        # if len(self.imageCaptions) > 0: self.mask.update()
        pass


    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            print "Top of run loop"
            self.onClockTick()
            events = pygame.event.get()
            for event in events:
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                    elif event.key == self.correctKey[1] and self.quizMode != 3 and not self.finished:
                        self.correctAnswer()
                    elif event.key == self.wrongKey[1] and self.quizMode != 3 and not self.finished:
                        self.wrongAnswer()
                    elif event.key == K_RETURN and self.quizMode == 3 and not self.finished:
                        inputtedText = self.textInput.get_text()
                        # excuse mistakes by a trailing s
                        if ((inputtedText.lower() == self.imageList[self.imageIndex][0][0].lower()) or
                           (inputtedText.lower()[-1] == "s" and inputtedText.lower()[0:len(inputtedText)-1] == self.imageList[self.imageIndex][0][0].lower()) or
                           (self.imageList[self.imageIndex][0][0].lower()[-1] == "s" and inputtedText.lower() == self.imageList[self.imageIndex][0][0][0:len(self.imageList[self.imageIndex][0][0])-1].lower())):
                            self.correctAnswer()
                        else:
                            self.wrongAnswer()
                    elif event.key == K_RIGHT and self.imageIsDone and not self.finished:
                        self.nextCallback()
                    # elif event.key == K_LEFT and self.imageIsDone and not self.finished:
                    #     self.prevCallback()
                    # elif event.key == K_SPACE and self.finished:
                    #     self.playAgainCallback()
                if event.type == QUIT:
                    print "Quit"
                    # self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                    if not self.finished: self.prevNextRepeatButtons.update(*event.pos)
                    else: self.playAgainButton.update(*event.pos)
            if self.quizMode == 3 and not self.finished: self.textInput.update(events)
            self.clock.tick(self.fps)
            #self.removeEnemy()
            if self.endLoop == True: break

    def correctAnswer(self):
        if not self.imageIsDone:
            self.image.completeDisplay()
            self.score += 1 if self.triesLeft == self.numTries else 0.5
            self.imageIsDone = True
            self.toggleNextButton(True)
            # NOTE (amal): if we change this so the imageList doesn't start form 0, this will have to change
            if self.imageIndex == len(self.imageList)-1:
                self.gameOver()
        self.image.setCorrect()

    def wrongAnswer(self):
        if not self.imageIsDone:
            self.triesLeft -= 1
            if self.triesLeft == 0:
                # self.lives -= 1
                self.imageIsDone = True
                self.toggleNextButton(True)
                self.image.completeDisplay()
                # NOTE (amal): if we change this so the imageList doesn't start form 0, this will have to change
                if self.imageIndex == len(self.imageList)-1:
                    self.gameOver()
                # if self.lives == 0:
                #     self.gameOver()
            # TODO (amal): maybe have the wrong x disappear for a second before coming back to indicate that the student got it wrong again?
        self.image.setWrong()

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self, pictureList, displayMode, numTries):#, lives, ):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.OrderedUpdates()
        self.buttonsList = pygame.sprite.Group()
        self.prevNextRepeatButtons = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0.0
        # self.lives = lives
        self.numTries = numTries
        self.triesLeft = self.numTries
        # Create Image Dictionary and ImageCaptions list
        self.imageList = pictureList
        # self.imageCaptions = [caption for (caption, path) in self.imageList]
        # random.shuffle(self.imageCaptions)
        print self.imageList
        self.imageIndex = 0
        self.imageIsDone = False
        # Create image and/or word
        self.displayMode = displayMode
        self.createImageAndWord()

    def moveImage(self, dIndex):
        if len(self.imageList) > 0:
            self.imageIndex += dIndex
            # NOTE (amal): I changed it so it no longer mods by len, so it doesn't cycle through words but rather stops at the end.
            # If for whatever reason I change the start index from 0, I will have to change this
            # self.imageIndex %= len(self.imageList)
            print self.imageIndex
            if self.imageIndex >= len(self.imageList):
                self.imageIsDone = True
                self.image.completeDisplay()
                self.gameOver()
                return
            self.createImageAndWord()
            if self.quizMode == 3: self.textInput.delete_text()
            self.imageIsDone = False
            self.toggleNextButton(False)
            # self.image.setNeutral()

    def createImageAndWord(self):
        """Creates a player token and adds it to the Sprite List"""
        if len(self.imageList) > 0:
            self.allSpritesList = pygame.sprite.OrderedUpdates()
            caption, img = self.imageList[self.imageIndex][0][0], self.imageList[self.imageIndex][1][0]
            self.image = Image(x=self.screenWidth/2, y=self.screenHeight/2, width=self.screenWidth, height=self.screenHeight, imgPath=img, caption=caption, displayMode=self.displayMode)
            self.allSpritesList.add(self.image)
        else:
            self.image = Image(x=self.screenWidth/2, y=self.screenHeight/2, width=self.screenWidth, height=self.screenHeight, imgPath=None, caption="No pictures found...", displayMode=self.displayMode)
            self.allSpritesList.add(self.image)

    # TODO (amal): display Play Again button here, and don't end game after 2 seconds
    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!"
        self.playAgainButton = pygame.sprite.Group()
        self.createPlayAgainButton()
        # if "end" not in self.elapsedTicks: self.elapsedTicks["end"] = 0
        # self.delayBeforeClosingGame = 5 #seconds
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption("Image Now!")
        # Load the Font
        self.font = pygame.font.SysFont("Arial", self.screenWidth/30)
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.drawBackground()
        self.createMenuButton()
        self.createNextButton()
        # self.createRepeatButton()
        # self.createPreviousButton()
        # self.createCorrectWrongSprite()
        self.detectedVoice()

    def drawBackground(self):
        self.surface.fill((0xFF, 0xFF, 0xFF))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        def callback(data):
            print "Clicked Menu Botton"
            pygame.event.post(pygame.event.Event(QUIT, {}))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=callback, text="Menu")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    # TODO: add function to toggle between skip and next instead of copying and pasting
    def createNextButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = (self.screenWidth/2-width/2, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        self.nextCallback = self.prevNextRepeatCallback(1)
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=self.nextCallback, callbackArgument={}, text="Skip")
        self.nextButton = menu
        self.prevNextRepeatButtons.add(menu)
        # self.allSpritesList.add(menu)

    def toggleNextButton(self, gotoNext=True):
        margin = 20
        (width, height) = (120, 50)
        if gotoNext:
            self.nextButton.setText("Next")
            self.nextButton.setPos(self.screenWidth-margin-width, self.screenHeight-margin-height)
        else:
            self.nextButton.setText("Skip")
            self.nextButton.setPos(self.screenWidth/2-width/2, self.screenHeight-margin-height)

    def createPlayAgainButton(self):
        margin = 20
        (width, height) = (225, 50)
        # (x,y) = ((self.screenWidth-width)/2, self.screenHeight-margin-height)
        (x,y) = (self.screenWidth-margin-width, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        def callback(*data):
            print "Play Again!"
            self.playAgainButton = pygame.sprite.Group()
            self.elapsedTicks = {"time": 0}
            # Sets game attributes
            self.finished = False # Whether the game has finished
            self.score = 0.0
            # self.lives = lives
            self.triesLeft = self.numTries
            self.imageIndex = 0
            self.imageIsDone = False
            if self.quizMode == 3: self.textInput.delete_text()
            self.toggleNextButton(False)
            # Create image and/or word
            self.createImageAndWord()

        self.playAgainCallback = callback
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=self.playAgainCallback, callbackArgument={}, text="Repeat Words")
        self.playAgainButton.add(menu)
        # self.allSpritesList.add(menu)

    def prevNextRepeatCallback(self, dIndex):
        def callback(*data):
            self.triesLeft = self.numTries
            self.moveImage(dIndex)
        return callback

    def createPreviousButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = (margin, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        self.prevCallback = self.prevNextRepeatCallback(-1)
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=self.prevCallback, callbackArgument={}, text="Previous")
        self.prevNextRepeatButtons.add(menu)
        # self.allSpritesList.add(menu)

    # def createCorrectWrongSprite(self):
    #     width, height = min(self.screenWidth/4, self.screenHeight/4), min(self.screenWidth/4, self.screenHeight/4)
    #     centerx, centery = self.screenWidth/2, self.screenHeight/2
    #     self.correctWrongSprite = CorrectWrong(centerx-width/2, centery-height/2, width, height, baseColor=(0xFF, 0xFF, 0xFF), correctImgPath="img/checkmark.png", wrongImgPath="img/xmark.png")
    #     self.allSpritesList.add(self.correctWrongSprite)

    # quizMode: 0 = give the word sign, 1 = give the sign spelling, 2 = say the word, 3 = type the word
    def drawInstructionsAndTextInput(self):
        fontHeight = self.screenWidth/50
        font = pygame.font.SysFont("Arial", fontHeight)
        if self.quizMode == 0:
            message = "Give the sign for this "
            if self.displayMode == 1:
                message += "picture."
            else:
                message += "word."
        elif self.quizMode == 1:
            message = "Spell this "
            if self.displayMode == 1:
                message += "picture "
            else:
                message += "word "
            message += "using signs."
        elif self.quizMode == 2:
            message = "Use your voice to say "
            if self.displayMode == 1:
                message += "what is in this picture."
            else:
                message += "this word."
        elif self.quizMode == 3:
            message = "Use the keyboard to type "
            if self.displayMode == 1:
                message += "what is in this picture."
            else:
                message += "this word."
            message += "  Press enter when you are done."
        instructions = font.render(message, 1, (0,0,0))
        self.surface.blit(instructions, (instructions.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight*5/40)))
        if self.quizMode != 3:
            message = "Press "+self.correctKey[0]+" for right, and "+self.wrongKey[0]+" for wrong."
            instructions = font.render(message, 1, (0,0,0))
            self.surface.blit(instructions, (instructions.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight*5/40+fontHeight*4/3)))
        else:
            self.surface.blit(self.textInput.get_surface(), self.textInput.get_surface().get_rect(centerx=self.screenWidth/2, centery=self.screenHeight*5/40+fontHeight*4/3))

    def drawScoreTimeNumTries(self):
        margin = 10
        fontHeight = self.screenWidth/40
        font = pygame.font.SysFont("Arial", fontHeight)
        # Score
        scoreText = "Score: "+str(self.score)
        score = font.render(scoreText, 1, (0,0,0))
        self.surface.blit(score, (score.get_rect(x=margin, y=margin)))

        # Maximum Score
        maxScoreText = "Maximum Score: "+str(float(len(self.imageList)))
        maxScore = font.render(maxScoreText, 1, (0,0,0))
        self.surface.blit(maxScore, (score.get_rect(x=margin, y=margin*2+fontHeight)))

        # Tries Left
        triesText = "Tries Left: "+str(self.triesLeft)
        tries = font.render(triesText, 1, (0,0,0))
        self.surface.blit(tries, (tries.get_rect(x=margin, y=margin*3+fontHeight*2)))

        # Time Text
        seconds = int(self.elapsedTicks["time"]/self.fps)
        # print seconds
        minutes = seconds/60
        seconds %= 60
        timeText = font.render("Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))
        self.surface.blit(timeText, (timeText.get_rect(x=margin, y=margin*4+fontHeight*3)))

        # Lives
        # livesText = "Lives: "+str(self.lives)
        # lives = font.render(livesText, 1, (0,0,0))
        # self.surface.blit(lives, (lives.get_rect(x=margin, y=margin*3+fontHeight*2)))

    def drawGameOverText(self):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        gameOverText = font.render("Game Over.  Score: "+str(self.score)+" / "+str(float(len(self.imageList))), 1, (0,0,0))
        self.surface.blit(gameOverText, gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight*19/20))

    def drawTryAgainText(self):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        tryAgainText = font.render(str(self.triesLeft)+" More Tries", 1, (0,0,0))
        self.surface.blit(tryAgainText, tryAgainText.get_rect(centerx=self.screenWidth/4, centery=self.screenHeight*19/20))

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.drawBackground()
        self.allSpritesList.draw(self.surface)
        self.drawInstructionsAndTextInput()
        self.drawScoreTimeNumTries()
        if not self.finished:
            print "Drawing Prev buttons"
            self.prevNextRepeatButtons.draw(self.surface)
        if self.triesLeft < self.numTries and not self.imageIsDone and not self.finished: self.drawTryAgainText()
        if self.finished:
            print "Drawing Play Again Button"
            self.playAgainButton.draw(self.surface)
            self.drawGameOverText()
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = PictureThatQuiz(lambda: 1, 1280, 800, pictureList=[(["Mango"], ["img/PictureThatImages/Mango.jpg"]), (["Pineapple"], ["img/PictureThatImages/goldenpineapple.png"]), (["Fig"], ["img/PictureThatImages/Fig.jpg"])], displayMode=1, quizMode=2)
# game.run()
# pygame.quit()
# sys.exit()
