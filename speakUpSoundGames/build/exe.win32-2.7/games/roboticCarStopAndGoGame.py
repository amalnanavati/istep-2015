################################################################################
# A basic game where student voice controls a robotic car
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2017 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
import serial.tools.list_ports
from tools.GIFImage import *

class Image(pygame.sprite.Sprite):
    """Game Token class description"""

    # displayMode: 0 = word first, 1 = picture first, 2 = word and picture together
    def __init__(self, x, y, width, height, imgPath, caption, displayMode=1):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set the Sprite Attributes
        self.displayMode = displayMode
        self.imgPath = imgPath
        self.width, self.height = width, height
        # Determine text placement
        self.margin = height/20
        self.fontHeight = 50
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        self.pictureTitle = self.font.render(caption, 1, (0,0,0))
        self.pictureTitleRect = self.pictureTitle.get_rect(centerx=width/2, centery=self.margin+self.fontHeight/2)
        # Determine picture Placement
        topMargin = self.margin + self.fontHeight + 2*self.margin # Jank fix so the image is below the instructions
        bottomMargin, leftMargin, rightMargin = 2*self.margin, self.margin, self.margin
        self.pictureWidth, self.pictureHeight = (width-leftMargin-rightMargin), (height-bottomMargin-topMargin)
        self.pictureCenterx, self.pictureCentery = self.pictureWidth/2+leftMargin, self.pictureHeight/2+topMargin
        if self.imgPath is not None:
            if ".gif" in imgPath:
                image = pygame.Surface((width, height))
                image.fill((0xFF, 0xFF, 0xFF))
                print "imgPath: ", imgPath
                img = GIFImage(imgPath, self.pictureWidth, self.pictureHeight)
                img.render(image, (width/2, height/2))
                self.picture = image
            else:
                image = pygame.Surface((width, height))
                image.fill((0xFF, 0xFF, 0xFF))
                img = pygame.image.load(imgPath)
                # Scale the image to the desired width
                (imageW,imageH) = img.get_rect().size
                scaleFactor = min(float(self.pictureWidth)/imageW, float(self.pictureHeight)/imageH)
                imgWidth, imgHeight = int(imageW*scaleFactor), int(imageH*scaleFactor)
                img = pygame.transform.scale(img, (imgWidth, imgHeight))
                image.blit(img, img.get_rect(centerx=width/2, centery=height/2))
                self.picture = image
            # # Load the image
            # self.picture = pygame.image.load(self.imgPath)
            # # Scale the image to the desired width
            # (imageW,imageH) = self.picture.get_rect().size
            # imageScaleFactor = min(float(self.pictureWidth)/imageW, float(self.pictureHeight)/imageH)
            # self.picture = pygame.transform.scale(self.picture, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        # Create the image and rect
        self.image = pygame.Surface((width, height))
        self.image.fill((0xFF, 0xFF, 0xFF))
        self.rect = pygame.Rect(x, y, width, height)
        if self.displayMode == 1 or self.displayMode == 2:
            if self.imgPath is not None:
                # Display the image
                self.image.blit(self.picture, self.picture.get_rect(centerx=self.pictureCenterx, centery=self.pictureCentery))
        if self.displayMode == 0 or self.displayMode == 2:
            # Display the text
            self.image.blit(self.pictureTitle, self.pictureTitleRect)
        # Create CorrectWrong
        self.correctWrongGroup = pygame.sprite.Group()
        width, height = self.fontHeight, self.fontHeight
        x, y = self.width*51/64, self.height*7/64
        self.correctWrong = CorrectWrong(x, y, width, height, baseColor=(0xFF, 0xFF, 0xFF), correctImgPath="img/checkmark.png", wrongImgPath="img/xmark.png")
        self.correctWrongGroup.add(self.correctWrong)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    # If we are displaying the word only or the image only, additionally
    # display the other one
    def completeDisplay(self):
        if self.displayMode == 0:
            if self.imgPath is not None:
                # Display the image
                self.image.blit(self.picture, self.picture.get_rect(centerx=self.pictureCenterx, centery=self.pictureCentery))
                self.image.blit(self.pictureTitle, self.pictureTitleRect)
        elif self.displayMode == 1:
            # Display the text
            self.image.blit(self.pictureTitle, self.pictureTitleRect)

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    def setCorrect(self):
        self.correctWrong.setCorrect()
        self.correctWrongGroup.draw(self.image)

    def setWrong(self):
        self.correctWrong.setWrong()
        self.correctWrongGroup.draw(self.image)

    def setNeutral(self):
        self.correctWrong.setNeutral()
        self.correctWrongGroup.draw(self.image)

class CorrectWrong(pygame.sprite.Sprite):
    """Game Token class description"""

    def __init__(self, x, y, width, height, baseColor=(0xFF, 0xFF, 0xFF), correctImgPath="img/checkmark.png", wrongImgPath="img/xmark.png"):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        self.baseColor = baseColor
        # Load the image
        image = pygame.Surface((width, height))
        image.fill(self.baseColor)
        # Load the correct and wrong images
        self.correctImage = pygame.image.load(correctImgPath)
        # Scale the image to the desired width
        (imageW,imageH) = image.get_rect().size
        imageScaleFactor = min(float(width)/imageW, float(height)/imageH)
        self.correctImage = pygame.transform.scale(self.correctImage, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        self.wrongImage = pygame.image.load(wrongImgPath)
        # Scale the image to the desired width
        (imageW,imageH) = image.get_rect().size
        imageScaleFactor = min(float(width)/imageW, float(height)/imageH)
        self.wrongImage = pygame.transform.scale(self.wrongImage, (int(imageW*imageScaleFactor), int(imageH*imageScaleFactor)))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    def setCorrect(self):
        self.image.fill(self.baseColor)
        self.image.blit(self.correctImage, self.correctImage.get_rect(x=0, y=0))

    def setWrong(self):
        self.image.fill(self.baseColor)
        self.image.blit(self.wrongImage, self.correctImage.get_rect(x=0, y=0))

    def setNeutral(self):
        self.image.fill(self.baseColor)

class StateChangingSprite(pygame.sprite.Sprite):
    """RoboticCarStopAndGoGame StateChangingSprite class description"""

    def __init__(self, centerx, centery, maxWidth, maxHeight, imagePaths={"Green": "img/trafficlightGreen.png", "Red": "img/trafficlightRed.png", "Yellow": "img/trafficlightYellow.png", "UP": "img/trafficlightGreenUP.png", "DOWN": "img/trafficlightGreenDOWN.png", "UPRIGHT": "img/trafficlightGreenUPRIGHT.png", "UPLEFT": "img/trafficlightGreenUPLEFT.png", "DOWNRIGHT": "img/trafficlightGreenDOWNRIGHT.png", "DOWNLEFT": "img/trafficlightGreenDOWNLEFT.png"}, startingState="Red"):
        """Initializes StateChangingSprite"""
        # Initialize the sprite
        #print x, y, speed, imgPath
        pygame.sprite.Sprite.__init__(self)
        #imgPath = 'img/'
        # Load all the images
        self.images = {}
        maxImgWidth, maxImgHeight = 0, 0
        for key, path in imagePaths.iteritems():
            image = pygame.image.load(path)
            # Scale the image to the desired width
            (imageW,imageH) = image.get_rect().size
            scaleFactor = min(float(maxWidth)/imageW, float(maxHeight)/imageH)
            imgWidth, imgHeight = int(imageW*scaleFactor), int(imageH*scaleFactor)
            image = pygame.transform.scale(image, (imgWidth, imgHeight))
            if maxImgWidth < imgWidth: maxImgWidth = imgWidth
            if maxImgHeight < imgHeight: maxImgHeight = imgHeight
            self.images[key] = image
        # Create the image
        self.currentState = startingState
        self.image = pygame.Surface((maxImgWidth, maxImgHeight))
        self.image.fill((0xFF, 0xFF, 0xFF))
        self.image.blit(self.images[self.currentState], self.images[self.currentState].get_rect(centerx=self.image.get_rect().width/2, centery=self.image.get_rect().height/2))
        # Set the sprit's attributes
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect(centerx=centerx, centery=centery)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def changeState(self, state):
        if state != self.currentState:
            self.currentState = state
            self.image = pygame.Surface((self.rect.width, self.rect.height))
            self.image.fill((0xFF, 0xFF, 0xFF))
            self.image.blit(self.images[self.currentState], self.images[self.currentState].get_rect(centerx=self.image.get_rect().width/2, centery=self.image.get_rect().height/2))

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class RoboticCarStopAndGoGame(object):
    """The RoboticCarStopAndGoGame"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=70, minExpectedVolume=60, maxExpectedVolume=80, pictureList=[]):
        self.returnToMenu = returnToMenu
        self.initController(volumeThreshold, currentW, currentH)
        self.initModel(pictureList)
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, volumeThreshold, currentW, currentH):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1000, 1000)
        # defaultRatio = float(defaultW)/defaultH
        # actualRatio = float(currentWidth)/currentHeight
        # if actualRatio > defaultRatio:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        # else:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.setClockRelated()
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyRoboticCarStopAndGoGame timing is not exact,
        # we increase the size of the listening buffer.
        # scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        # self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        # self.listener.startStream()
        # Create serial connection
        ser = self.connectToArduinoSerial("Arduino Uno")
        if ser is None:
            self.isArduinoPluggedIn = False
            self.createNotConnectedToArduinoButtonAndText()
        else:
            self.isArduinoPluggedIn = True
            self.serial = ser
        self.lightIsRed = True
        # Set the correct and wrong keys
        self.correctKey = ("r", K_r)
        self.wrongKey = ("w", K_w)
        self.gotWrong = False

    def setClockRelated(self):
        self.yellowMinTime = 2.0
        self.yellowMaxTime = 4.0
        yellowTime = random.uniform(self.yellowMinTime, self.yellowMaxTime)
        self.yellowTicks = int(yellowTime*self.fps)
        self.greenMinTime = 7.0
        self.greenMaxTime = 20.0
        greenTime = random.uniform(self.greenMinTime, self.greenMaxTime)
        self.greenTicks = int(greenTime*self.fps)

        self.elapsedTicks = {} # Keeps track of elapsed time since events

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        print self.elapsedTicks
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            pass
        elif self.isArduinoPluggedIn:
            if "yellow" in self.elapsedTicks and self.elapsedTicks["yellow"] >= self.yellowTicks:
                self.lightIsRed = True
                print "switching to red"
                self.trafficlight.changeState("Red")
                del self.elapsedTicks["yellow"]
                self.moveImage(1)
                self.createImageAndWord()
            if "green" in self.elapsedTicks and self.elapsedTicks["green"] >= self.greenTicks:
                print "switching to yellow"
                yellowTime = random.uniform(self.yellowMinTime, self.yellowMaxTime)
                self.yellowTicks = int(yellowTime*self.fps)
                self.trafficlight.changeState("Yellow")
                self.elapsedTicks["yellow"] = 0
                self.writeToSerial(self.directionsMap["STOP"])
                del self.elapsedTicks["green"]
            if "green" in self.elapsedTicks and self.elapsedTicks["green"] < self.greenTicks:
                if self.previousDirection is None:
                    self.writeToSerial(self.currentDirection)
                    self.previousDirection = self.currentDirection
                elif self.previousDirection != self.currentDirection:
                    self.writeToSerial(self.currentDirection)
                    self.previousDirection = self.currentDirection
            if "move" in self.elapsedTicks and self.elapsedTicks["move"] == 1:
                pass
                # self.serial.write(chr(self.currentDirection + 65))
                # print "WROTE TO SERIAL MOVE!!!", self.currentDirection
            elif "move" in self.elapsedTicks and self.elapsedTicks["move"] >= self.ticksToMoveFor:
                pass
                # self.serial.write(chr(self.directionsMap["STOP"] + 65))
                # print "WROTE TO SERIAL STOP"
                # del self.elapsedTicks["move"]
        # Update the view
        self.update()

    def writeToSerial(self, command):
        self.serial.write(chr(command + 65))

    def sendUpdates(self):
        pass
        # if self.currentDirection != self.previousDirection and self.currentDirection == self.directionsMap["STOP"]:
        #     print "ASDF", self.currentDirection, self.previousDirection
        #     self.serial.write(chr(self.currentDirection + 65))
        #     self.previousDirection = self.currentDirection
        # (volume, pitch) = self.listener.listen()
        # if (volume > self.volumeThreshold or pitch != None):
        #     self.detectedVoice(volume, pitch)
        # else:
        #     self.currentOnOFF = False
        #     print "QWER", self.currentDirection, self.previousDirection
        #     self.serial.write(chr(self.directionsMap["STOP"] + 65))
        #     self.previousDirection = self.directionsMap["STOP"]

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        pass
        # self.currentOnOFF = True
        # if self.previousDirection is not None and self.currentDirection != self.previousDirection:
        #     print "ZXCV", self.currentDirection, self.previousDirection
        #     self.serial.write(chr(self.currentDirection + 65))
        #     self.previousDirection = self.currentDirection

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def connectToArduinoSerial(self, keyword):
        ports = list(serial.tools.list_ports.comports())
        print ports
        for p in ports:
            print p
            print(repr(p))
        try:
            port = next(serial.tools.list_ports.grep(keyword))
            print port
            print repr(port)
            ser = serial.Serial(port=port.device, baudrate=115200, timeout=1)
            return ser
        except StopIteration:
            return None

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            # TODO (amal): add all the robotic car events here
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                    if self.isArduinoPluggedIn:
                        keyinput = pygame.key.get_pressed();
                        # complex orders
                        if keyinput[K_UP] and keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["UPRIGHT"]
                        elif keyinput[K_UP] and keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["UPLEFT"]
                        elif keyinput[K_DOWN] and keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["DOWNRIGHT"]
                        elif keyinput[K_DOWN] and keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["DOWNLEFT"]

                        #simple orders
                        elif keyinput[K_UP]:

                            self.currentDirection = self.directionsMap["UP"]
                        elif keyinput[K_DOWN]:

                            self.currentDirection = self.directionsMap["DOWN"]
                        elif keyinput[K_RIGHT]:

                            self.currentDirection = self.directionsMap["RIGHT"]
                        elif keyinput[K_LEFT]:

                            self.currentDirection = self.directionsMap["LEFT"]

                        # if self.lightIsRed and keyinput[K_SPACE]:
                        #     print "Space Pressed!"
                        #     greenTime = random.uniform(self.greenMinTime, self.greenMaxTime)
                        #     self.greenTicks = int(greenTime*self.fps)
                        #     self.elapsedTicks["green"] = 0
                        #     self.lightIsRed = False

                        if self.lightIsRed and keyinput[self.correctKey[1]]:# and not self.finished:
                            self.correctAnswer()
                        elif self.lightIsRed and keyinput[self.wrongKey[1]]:# and not self.finished:
                            self.wrongAnswer()

                elif event.type == pygame.KEYUP:
                    if self.isArduinoPluggedIn:
                        #single key
                        if ((self.currentDirection == self.directionsMap["UP"] and event.key == K_UP) or
                            (self.currentDirection == self.directionsMap["DOWN"] and event.key == K_DOWN) or
                            (self.currentDirection == self.directionsMap["RIGHT"] and event.key == K_RIGHT) or
                            (self.currentDirection == self.directionsMap["LEFT"] and event.key == K_LEFT)):

                            self.currentDirection = self.directionsMap["STOP"]

                        #up-right
                        elif (self.currentDirection == self.directionsMap["UPRIGHT"]):
                            if event.key == K_RIGHT:

                                self.currentDirection = self.directionsMap["UP"]
                            elif event.key == K_UP:

                                self.currentDirection = self.directionsMap["RIGHT"]

                        #up-left
                        elif (self.currentDirection == self.directionsMap["UPLEFT"]):
                            if event.key == K_LEFT:

                                self.currentDirection = self.directionsMap["UP"]
                            elif event.key == K_UP:

                                self.currentDirection = self.directionsMap["LEFT"]

                        #back-right
                        elif (self.currentDirection == self.directionsMap["DOWNRIGHT"]):
                            if event.key == K_RIGHT:

                                self.currentDirection = self.directionsMap["DOWN"]
                            elif event.key == K_DOWN:

                                self.currentDirection = self.directionsMap["RIGHT"]

                        #back-left
                        elif (self.currentDirection == self.directionsMap["DOWNLEFT"]):
                            if event.key == K_LEFT:

                                self.currentDirection = self.directionsMap["DOWN"]
                            elif event.key == K_DOWN:

                                self.currentDirection = self.directionsMap["LEFT"]

                elif event.type == QUIT:
                    if self.isArduinoPluggedIn: self.serial.close()
                    # self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                elif event.type == MOUSEBUTTONDOWN:
                    if not self.finished: self.prevNextRepeatButtons.update(*event.pos)
                    self.buttonsList.update(*event.pos)
                    if not self.isArduinoPluggedIn: self.connectButtonList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    def correctAnswer(self):
        if not self.imageIsDone and not self.gotWrong:
            self.image.completeDisplay()
            # self.score += 1 if self.triesLeft == self.numTries else 0.5
            self.imageIsDone = True
            greenTime = random.uniform(self.greenMinTime, self.greenMaxTime)
            self.greenTicks = int(greenTime*self.fps)
            self.elapsedTicks["green"] = 0
            self.lightIsRed = False
            # self.moveImage(1)
            # self.toggleNextButton(False)
            # NOTE (amal): if we change this so the imageList doesn't start form 0, this will have to change
            # if self.imageIndex == len(self.imageList)-1:
            #     self.gameOver()
        self.image.setCorrect()

    def wrongAnswer(self):
        if not self.imageIsDone:
            # self.triesLeft -= 1
            # if self.triesLeft == 0:
                # self.lives -= 1
            self.gotWrong = True
            # self.imageIsDone = True
            self.toggleNextButton(True)
            self.image.completeDisplay()
            # self.moveImage(1)
                # NOTE (amal): if we change this so the imageList doesn't start form 0, this will have to change
                # if self.imageIndex == len(self.imageList)-1:
                #     self.gameOver()
                # if self.lives == 0:
                #     self.gameOver()
            # TODO (amal): maybe have the wrong x disappear for a second before coming back to indicate that the student got it wrong again?
        self.image.setWrong()

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self, pictureList):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.trafficlightList = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        self.prevNextRepeatButtons = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        # TODO (amal): score I don't need, time I should have, and a distance measure
        self.score = 0
        #create player game arrow
        self.createStateChangingSprites()
        # store the current direction the car will go if provided voice, based on the arrow keys
        # NOTE (amal): If you change the directions mapping below, you must correspondingly change the arduino code for the robotic car
        self.directionsMap = {"STOP": 0, "UP": 11, "DOWN": 12, "RIGHT": 13, "LEFT": 14, "UPRIGHT": 21, "UPLEFT": 22, "DOWNRIGHT": 23, "DOWNLEFT": 24}
        self.currentDirection = self.directionsMap["STOP"]
        self.previousDirection = None
        self.imageList = pictureList
        # self.imageCaptions = [caption for (caption, path) in self.imageList]
        # random.shuffle(self.imageCaptions)
        print self.imageList
        self.imageIndex = 0
        self.imageIsDone = False
        if self.isArduinoPluggedIn: self.createImageAndWord()

    def moveImage(self, dIndex):
        if len(self.imageList) > 0:
            self.imageIndex += dIndex
            self.imageIndex %= len(self.imageList)
            self.toggleNextButton(False)
            self.gotWrong = False
            # NOTE (amal): I changed it so it no longer mods by len, so it doesn't cycle through words but rather stops at the end.
            # If for whatever reason I change the start index from 0, I will have to change this
            # self.imageIndex %= len(self.imageList)
            # print self.imageIndex
            # if self.imageIndex >= len(self.imageList):
            #     self.imageIsDone = True
            #     self.image.completeDisplay()
            #     self.gameOver()
            #     return
            # self.createImageAndWord()
            self.imageIsDone = False
            # self.image.setNeutral()

    def createImageAndWord(self):
        """Creates a player token and adds it to the Sprite List"""
        if len(self.imageList) > 0:
            self.allSpritesList = pygame.sprite.OrderedUpdates()
            caption, img = self.imageList[self.imageIndex][0][0], self.imageList[self.imageIndex][1][0]
            self.image = Image(x=self.screenWidth/2, y=0, width=self.screenWidth/2, height=self.screenHeight, imgPath=img, caption=caption)
            self.allSpritesList.add(self.image)
        else:
            self.image = Image(x=self.screenWidth/2, y=0, width=self.screenWidth/2, height=self.screenHeight, imgPath=None, caption="No pictures found...")
            self.allSpritesList.add(self.image)

    def createStateChangingSprites(self):
        """Creates a player arrow and adds it to the Sprite List"""
        self.trafficlight = StateChangingSprite(centerx=self.screenWidth/4, centery=self.screenHeight/2, maxWidth=self.screenWidth/2, maxHeight=self.screenHeight)
        # self.onoff = StateChangingSprite(centerx=(self.screenWidth-self.screenHeight)/2+self.screenHeight, centery=self.screenHeight/2, maxWidth=(self.screenWidth-self.screenHeight/2), maxHeight=self.screenHeight/4, imagePaths={"ON": "img/ON.png", "OFF": "img/OFF.png"}, startingState="OFF")
        self.trafficlightList.add(self.trafficlight)
        # self.trafficlightList.add(self.onoff)

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Sound RoboticCarStopAndGoGame Template')
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.drawBackground()
        self.createMenuButton()
        self.createSkipButton()
        # self.drawStateChangingSprite()

    def drawBackground(self):
        self.surface.fill((0xFF, 0xFF, 0xFF))

    def setCurrentDirectionArrowAndOnOff(self):
        if "green" in self.elapsedTicks and self.elapsedTicks["green"] < self.greenTicks:
            if self.currentDirection == self.directionsMap["STOP"]:
                self.trafficlight.changeState("Green")
            elif self.currentDirection == self.directionsMap["UP"]:
                self.trafficlight.changeState("UP")
            elif self.currentDirection == self.directionsMap["DOWN"]:
                self.trafficlight.changeState("DOWN")
            elif self.currentDirection == self.directionsMap["RIGHT"]:
                self.trafficlight.changeState("Green")
            elif self.currentDirection == self.directionsMap["LEFT"]:
                self.trafficlight.changeState("Green")
            elif self.currentDirection == self.directionsMap["UPRIGHT"]:
                self.trafficlight.changeState("UPRIGHT")
            elif self.currentDirection == self.directionsMap["UPLEFT"]:
                self.trafficlight.changeState("UPLEFT")
            elif self.currentDirection == self.directionsMap["DOWNRIGHT"]:
                self.trafficlight.changeState("DOWNRIGHT")
            elif self.currentDirection == self.directionsMap["DOWNLEFT"]:
                self.trafficlight.changeState("DOWNLEFT")

    def createSkipButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = (self.screenWidth*3/4-width/2, self.screenHeight*19/20-height/2)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        def callback(*data):
            # self.triesLeft = self.numTries
            self.moveImage(1)
            self.createImageAndWord()
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=callback, callbackArgument={}, text="Skip")
        self.skipButton = menu
        self.prevNextRepeatButtons.add(menu)
        # self.allSpritesList.add(menu)

    def toggleNextButton(self, gotoNext=True):
        margin = 20
        (width, height) = (120, 50)
        if gotoNext:
            self.skipButton.setText("Next")
            # self.skipButton.setPos(self.screenWidth-margin-width, self.screenHeight-margin-height)
        else:
            self.skipButton.setText("Skip")
            # self.skipButton.setPos(self.screenWidth/2-width/2, self.screenHeight-margin-height)

    def createNotConnectedToArduinoButtonAndText(self):
        # Create Text
        fontHeight = self.screenWidth/30
        font = pygame.font.SysFont("Arial", fontHeight)
        self.notConnectedTextLine1 = font.render("Could not detect remote control.", 1, (0,0,0))
        self.notConnectedTextLine2 = font.render("Plug it in, power it on, and press Connect.", 1, (0,0,0))
        # Create Button
        self.connectButtonList = pygame.sprite.Group()
        margin = 20
        (width, height) = (150, 50)
        (x,y) = (self.screenWidth/2-width/2, self.screenHeight*4/5-height/2)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        def callback(*data):
            ser = self.connectToArduinoSerial("Arduino Uno")
            if ser is not None:
                self.isArduinoPluggedIn = True
                self.serial = ser
                self.connectButtonList = pygame.sprite.Group()
                self.createImageAndWord()
        connect = Button(x, y, color=color, width=width, height=height,
                 callback=callback, text="Connect")
        self.connectButtonList.add(connect)

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def drawInstructionsAndTextInput(self):
        fontHeight = self.screenWidth/50
        font = pygame.font.SysFont("Arial", fontHeight)
        message = "What is in this picture?"
        instructions = font.render(message, 1, (0,0,0))
        self.surface.blit(instructions, (instructions.get_rect(centerx=self.screenWidth*3/4, centery=self.screenHeight*5/40)))
        message = "Press "+self.correctKey[0]+" for right, and "+self.wrongKey[0]+" for wrong."
        instructions = font.render(message, 1, (0,0,0))
        self.surface.blit(instructions, (instructions.get_rect(centerx=self.screenWidth*3/4, centery=self.screenHeight*5/40+fontHeight*4/3)))

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.allSpritesList.draw(self.surface)
        if self.isArduinoPluggedIn:
            print self.currentDirection, self.previousDirection
            self.setCurrentDirectionArrowAndOnOff()
            self.trafficlightList.draw(self.surface)
            self.drawInstructionsAndTextInput()
            if not self.imageIsDone:
                self.prevNextRepeatButtons.draw(self.surface)
        else:
            margin = 20
            self.surface.blit(self.notConnectedTextLine1, self.notConnectedTextLine1.get_rect(centerx=self.screenWidth/2, y=self.screenHeight/2-self.notConnectedTextLine1.get_rect().height-margin/2))
            self.surface.blit(self.notConnectedTextLine2, self.notConnectedTextLine2.get_rect(centerx=self.screenWidth/2, y=self.screenHeight/2+margin/2))
            self.connectButtonList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = RoboticCarStopAndGoGame(lambda: 1, 1280, 800, pictureList=[(["Mango"], ["img/PictureThatImages/Mango.jpg"]), (["Fig"], ["img/PictureThatImages/Fig.jpg"]), (["Flower"], ["img/PictureThatImages/Flower.jpg"]), (["Clock"], ["img/PictureThatImages/Clock.jpg"])])
# game.run()
# pygame.quit()
# sys.exit()
