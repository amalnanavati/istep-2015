################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from games.tools.listener import *
from games.tools.button import *

class CalibrateMenu(object):
    """The menu to calibrate the background noise of the game"""

    def __init__(self, returnToMenu, currentW, currentH):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1200, 900)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.elapsedTicks = {"InitialMessage":0} # Keeps track of elapsed time since events
        timeToShowInitialMessage = 3 # seconds
        timeToStaySilentFor = 5 # seconds
        timeToSpeakInASoftVoiceFor = 6 # seconds, will ignore the first second because of transition
        timeToSpeakInALoudVoiceFor = 6 # seconds, will ignore the first second because of transition
        self.ticksToShowInitialMessage = int(timeToShowInitialMessage/self.timeBetweenFrames)
        self.ticksToStaySilentFor = int(timeToStaySilentFor/self.timeBetweenFrames)
        self.ticksToSpeakInASoftVoiceFor = int(timeToSpeakInASoftVoiceFor/self.timeBetweenFrames)
        self.ticksToSpeakInALoudVoiceFor = int(timeToSpeakInALoudVoiceFor/self.timeBetweenFrames)
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(2.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        self.noiseList = []

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.elapsedTicks["InitialMessage"] < self.ticksToShowInitialMessage:
            self.showInitialMessage()
            print self.listener.listen()
        elif self.elapsedTicks["InitialMessage"] == self.ticksToShowInitialMessage:
            self.elapsedTicks["Silent"] = 0
            self.noiseList = []
        elif self.elapsedTicks["Silent"] < self.ticksToStaySilentFor:
            # print self.elapsedTicks["Silent"], self.ticksToStaySilentFor
            self.showSilentText()
            (volume, pitch) = self.listener.listen()
            if volume == 0: # IO Error
                self.elapsedTicks["Silent"] -= 1
            else:
                self.noiseList.append(volume)
        elif self.elapsedTicks["Silent"] == self.ticksToStaySilentFor:
            self.backgroundNoise = self.calculateNoise(0.5) # 3rd quartile
            self.elapsedTicks["Soft"] = 0
            self.noiseList = []
            self.showSoftText()
        elif self.elapsedTicks["Soft"] < self.ticksToSpeakInASoftVoiceFor:
            # print self.elapsedTicks["Silent"], self.ticksToStaySilentFor
            self.showSoftText()
            if self.elapsedTicks["Soft"] > 1:
                (volume, pitch) = self.listener.listen()
                if volume == 0: # IO Error
                    self.elapsedTicks["Soft"] -= 1
                else:
                    self.noiseList.append(volume)
        elif self.elapsedTicks["Soft"] == self.ticksToSpeakInASoftVoiceFor:
            self.softNoise = self.calculateNoise(0.75) # 3rd quartile
            # Save Background noise
            with open('BackgroundNoise.txt', 'wt') as doc:
                weightToSoftNoise = 0.75
                volumeThreshold = self.backgroundNoise*(1.0-weightToSoftNoise) + self.softNoise*weightToSoftNoise
                print "Volume Threshold: ", volumeThreshold
                doc.write(str(volumeThreshold))
            # Save minExpectedVolume
            with open('MinExpectedNoise.txt', 'wt') as doc:
                print "MinExpectedNoise: ", self.softNoise
                doc.write(str(self.softNoise))
            self.elapsedTicks["Loud"] = 0
            self.noiseList = []
            self.showLoudText()
        elif self.elapsedTicks["Loud"] < self.ticksToSpeakInALoudVoiceFor:
            # print self.elapsedTicks["Silent"], self.ticksToStaySilentFor
            self.showLoudText()
            if self.elapsedTicks["Loud"] > 1:
                (volume, pitch) = self.listener.listen()
                if volume == 0: # IO Error
                    self.elapsedTicks["Loud"] -= 1
                else:
                    self.noiseList.append(volume)
        elif self.elapsedTicks["Loud"] == self.ticksToSpeakInALoudVoiceFor:
            self.loudNoise = self.calculateNoise(0.8725) # 7/8 median
            # Save maxExpectedVolume
            with open('MaxExpectedNoise.txt', 'wt') as doc:
                print "MaxExpectedNoise: ", self.loudNoise
                doc.write(str(self.loudNoise))
            self.elapsedTicks["DoneMessage"] = 0
            self.showDoneMessage()
        # Update the view
        self.update()

    def calculateNoise(self, quartile):
        self.noiseList.sort()
        noise = self.noiseList[int((len(self.noiseList)-1)*quartile)] # Get third quartile
        # roundToNearest = 5
        # backgroundNoiseRounded = [int(round(backgroundNoise/roundToNearest))*roundToNearest for backgroundNoise in self.noiseList]
        # backgroundNoiseFrequency = {}
        # for roundedNoise in backgroundNoiseRounded:
        #     if roundedNoise not in backgroundNoiseFrequency:
        #         backgroundNoiseFrequency[roundedNoise] = 0
        #     else:
        #         backgroundNoiseFrequency[roundedNoise] += 1
        # maxNoise, maxCount = 0, 0
        # for roundedNoise, count in backgroundNoiseFrequency.iteritems():
        #     if maxCount < count:
        #         maxNoise, maxCount = roundedNoise, count
        print "Noise Level: %f" %(noise)
        # print "Background Noise Level RoundToNearest Mode: %f, Count: %f" %(maxNoise, maxCount)
        return noise
        # with open('BackgroundNoise.txt', 'wt') as doc:
        #     doc.write(str(noise))

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.buttonsList = pygame.sprite.Group()

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Calibrate')
        # Intialize the font
        self.font = pygame.font.SysFont("Arial", self.screenWidth/30)
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()
        self.createTitle()
        self.text = self.font.render("", 1, (0,0,0))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def createTitle(self):
        title = pygame.image.load('games/img/CalibrateText.png')
        (titleW, titleH) = title.get_rect().size
        width = self.screenWidth*8/10
        height = width*titleH/titleW
        self.title = pygame.transform.scale(title, (width, height))

    def showInitialMessage(self):
        message = "Please be silent.  Listening will start soon."
        self.text = self.font.render(message, 1, (0,0,0))

    def showSilentText(self):
        message = "Please stay silent for %d seconds" %(int((self.ticksToStaySilentFor-self.elapsedTicks["Silent"])*self.timeBetweenFrames+1))
        # dots = "."*(self.elapsedTicks["Silent"]%30/5+1)
        # message += dots
        self.text = self.font.render(message, 1, (0,0,0))

    def showSoftText(self):
        message = "Please make soft sounds for %d seconds" %(int((self.ticksToSpeakInASoftVoiceFor-self.elapsedTicks["Soft"])*self.timeBetweenFrames+1))
        # dots = "."*(self.elapsedTicks["Silent"]%30/5+1)
        # message += dots
        self.text = self.font.render(message, 1, (0,0,0))

    def showLoudText(self):
        message = "Please make loud sounds for %d seconds" %(int((self.ticksToSpeakInALoudVoiceFor-self.elapsedTicks["Loud"])*self.timeBetweenFrames+1))
        # dots = "."*(self.elapsedTicks["Silent"]%30/5+1)
        # message += dots
        self.text = self.font.render(message, 1, (0,0,0))

    def showDoneMessage(self):
        message = "Background noise has been removed.  Please return to menu."
        self.text = self.font.render(message, 1, (0,0,0))

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        (width, height) = self.title.get_rect().size
        self.surface.blit(self.title, (self.screenWidth/2-width/2, self.screenHeight/8-height/2))
        self.surface.blit(self.text, (self.text.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/2)))
        ellie = pygame.image.load('games/img/calibrationEllies.png')
        (ellieW, ellieH) = ellie.get_rect().size
        width = self.screenWidth
        height = width*ellieH/ellieW
        ellie = pygame.transform.scale(ellie, (width, height))
        self.surface.blit(ellie, (0, self.screenHeight-height))
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = CalibrateMenu(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
