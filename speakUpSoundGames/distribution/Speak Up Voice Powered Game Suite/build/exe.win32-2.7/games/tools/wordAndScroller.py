################################################################################
# The class for a scroller, and for the words to inhabit that scroller
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2017 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from games.tools.button import *

class Word(pygame.sprite.Sprite):
    """A pygame Sprite that functions like a word"""

    def __init__(self, x, y, color=(0xFF, 0xCC, 0x00), deleteColor=(0xF8, 0xC8, 0x08),
                 deleteCallback=lambda x: Word.defaultCallback(), deleteCallbackArgument={},
                 text="Word", deleteButtonText="Delete", width=100, height=20, fontSize=30, deleteFontSize = 20, minFontSize=10):
        """Initializes the button"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set the font
        self.fontSize = fontSize
        fontSize = max(minFontSize, min(fontSize, width/len(text)))
        self.font = pygame.font.SysFont("Arial", fontSize)
        # Load the surface
        self.color = color
        self.image = pygame.Surface((width, height))
        self.image.fill(color)
        # Load text
        self.textWord = text
        self.text = self.font.render(text, 1, (0x00,0x00,0x00))
        textpos = self.text.get_rect()
        textpos.x = self.image.get_rect().x
        textpos.centery = self.image.get_rect().centery
        self.image.blit(self.text, textpos)
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        # Set the callback
        self.deleteCallback = deleteCallback
        # Sets the game data
        self.deleteCallbackArgument = deleteCallbackArgument
        self.deleteFontSize = deleteFontSize
        self.deleteColor = deleteColor
        self.deleteButtonText = deleteButtonText
        # Initialize Buttons
        self.buttonsList = pygame.sprite.Group()
        # self.wasButtonClicked = False
        self.initButtons()
        self.buttonsList.draw(self.image)

    # NOTE (amal): I may be able to change this back to what Button had
    def update(self, type, x, y):
        """Called every 'interesting' pygame event"""
        # print "Called words update", (x, y)
        if type == MOUSEBUTTONUP:
            # Check for button clicks
            self.buttonsList.update(x - self.rect.x, y - self.rect.y)
            # if not self.wasButtonClicked:
            #     # pick up word
            #     (x, y, w, h) = (self.rect.x, self.rect.y, self.rect.width, self.rect.height)
            #     # print x0,y0,x,y,w,h
            #     if event.pos.x > x and event.pos.x < x + w and event.pos.y > y and event.pos.y < y + h:
            #         print "Button Clicked!"
            #         self.callback(self.callbackArgument)
            # pass
        # if event.type == MOUSEBUTTONUP:
        #     # drop word
        #     pass
        # if event.type == MOUSEMOTION:
        #     # move word
        #     pass

    def initButtons(self):
        margin = 10
        h = self.rect.height-2*margin
        w = 2*h
        x = self.rect.width-margin-w
        y = margin
        delete = Button(x, y, color=self.deleteColor, width=w, height=h,
                     callback=lambda data: self.deleteCallback(self.deleteCallbackArgument), text=self.deleteButtonText, fontSize=self.deleteFontSize)
        self.buttonsList.add(delete)

    def move(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    def isInButton(self, x, y):
        """Returns a boolean indicating whether the x,y point is within a button or not"""
        for button in self.buttonsList:
            if (x > button.rect.x and x < button.rect.x + button.rect.width and y > button.rect.y and y < button.rect.y + button.rect.height):
                return True
        return False

    def isIn(self, x, y):
        """Returns a boolean indicating whether the x,y point is within a button or not"""
        print x,y,self.rect.x, self.rect.y, self.rect.w, self.rect.h
        if (x > self.rect.x and x < self.rect.x + self.rect.width and y > self.rect.y and y < self.rect.y + self.rect.height):
            return True
        return False

    def createDuplicate(self, x, y):
        return Word(x, y, color=self.color, deleteColor=self.deleteColor,
                     deleteCallback=self.deleteCallback, deleteCallbackArgument=self.deleteCallbackArgument,
                     text=self.textWord, width=self.rect.width, height=self.rect.height, fontSize=self.fontSize, deleteFontSize=self.deleteFontSize)

    @staticmethod
    def defaultCallback(*args, **kwargs):
        print "Word Button Clicked!"

class VerticalScroller(pygame.sprite.Sprite):
    """A pygame Sprite that functions like a scroller"""

    def __init__(self, x, y, spriteGroup, color=(0xE2, 0xE2, 0xE2), width=300, height=900,
                backgroundHeight = 2000,
                scrollBarWidth=25, scrollBarColor=(0xC1, 0xC1, 0xC1),
                scrollerHeight=50, scrollerColor=(0x7C, 0x7C, 0x7C)):
        """Initializes the button"""
        width = width + scrollBarWidth
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the surface
        self.color = color
        self.image = pygame.Surface((width, height))
        self.image.fill(color)
        # Load the Background
        self.background = pygame.Surface((width, backgroundHeight))
        self.backgroundHeight = backgroundHeight
        self.background.fill(color)
        # draw the spriteGroup
        self.spriteGroup = spriteGroup
        self.spriteGroup.draw(self.background)
        self.image.blit(self.background, (0, 0))
        self.backgroundY = 0
        # Set the scale factor, the amount to scroll the background when the scroller is moved down 1 pixel
        self.scaleFactor = float(backgroundHeight-height)/height
        # Load the scrollBar
        self.scrollBar = pygame.Surface((scrollBarWidth, height))
        self.scrollBar.fill(scrollBarColor)
        self.image.blit(self.scrollBar, (width-scrollBarWidth, 0))
        # Load the Scroller
        self.scrollBarWidth = scrollBarWidth
        self.scrollerHeight = scrollerHeight
        self.scroller = pygame.Surface((scrollBarWidth, scrollerHeight))
        self.scroller.fill(scrollerColor)
        self.image.blit(self.scroller, (width-scrollBarWidth, 0))
        self.scrollerY = 0
        self.scrollerClicked = (False, None, None) # was it clicked, and the offset from the click position to the top-left corner
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        # current Offset based on how much we have scrolled
        self.currentOffset = (0, 0)

    # NOTE (amal): I may be able to change this back to what Button had
    def update(self, event):
        """Called every 'interesting' pygame event"""
        if event.type == MOUSEBUTTONDOWN:
            # Translate event position to our frame of reference
            x0, y0 = event.pos[0]-self.rect.x, event.pos[1]-self.rect.y
            # Check if buttonClick was on the scroller
            (x, y, w, h) = (self.rect.width-self.scrollBarWidth, self.scrollerY, self.scrollBarWidth, self.scrollerHeight)
            print "Vertical Scroller", x0, y0, x, y, w, h
            if x0 > x and x0 < x + w and y0 > y and y0 < y + h:
                print "Scroller Clicked!"
                self.scrollerClicked = (True, x0-x, y0-y)
                print "ScrollerClicked", self.scrollerClicked
        if event.type == MOUSEBUTTONUP:
            # Translate event position to our frame of reference
            x0, y0 = event.pos[0]-self.rect.x, event.pos[1]-self.rect.y
            print "Scroller Mouse Up"
            # update the spritegroup, but only if they are still within the screen (i.e. if the mousclick was within the VerticalScroller's image)
            if x0 > 0 and x0 < self.rect.width and y0 > 0 and y0 < self.rect.height:
                self.spriteGroup.update(event.type, x0, y0-self.backgroundY)
            # drop scroller
            self.scrollerClicked = (False, None, None)
        if event.type == MOUSEMOTION:
            # Translate event position to our frame of reference
            x0, y0 = event.pos[0]-self.rect.x, event.pos[1]-self.rect.y
            # move scroller
            if self.scrollerClicked[0]:
                newScrollerY = y0-self.scrollerClicked[2]
                print "Scroller Mouse Motion", newScrollerY
                if newScrollerY > 0 and newScrollerY < self.rect.height - self.scrollerHeight:
                    print "Scroller Regular Motion"
                    dyScroller = newScrollerY-self.scrollerY
                    self.scrollerY = newScrollerY
                    dyBackground = int(dyScroller*self.scaleFactor)
                    print "dYBackground", dyBackground, "oldBackgroundY", self.backgroundY
                    self.backgroundY = self.backgroundY-dyBackground
                    print "New BackgroundY", self.backgroundY
                elif newScrollerY <= 0:
                    print "Scroller at Top"
                    self.scrollerY = 0
                    self.backgroundY = 0
                elif newScrollerY >= self.rect.height - self.scrollerHeight:
                    print "Scroller at Bottom"
                    self.scrollerY = self.rect.height - self.scrollerHeight
                    self.backgroundY = self.rect.height - self.backgroundHeight
                self.redrawAll()

    def redrawAll(self):
        # Redraw the words
        self.background.fill(self.color)
        self.spriteGroup.draw(self.background)
        # redraw the scrolled the background
        self.image.blit(self.background, (0, self.backgroundY))
        # redraw the scrollBar
        self.image.blit(self.scrollBar, (self.rect.width-self.scrollBarWidth, 0))
        # redraw the scroller
        print "new scroller y", self.scrollerY
        self.image.blit(self.scroller, (self.rect.width-self.scrollBarWidth, self.scrollerY))

    def changeSpriteGroup(self, newSpriteGroup):
        self.spriteGroup = newSpriteGroup
        self.redrawAll()

    def destroy(self):
        """Destroys the sprite"""
        self.kill()
