################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *
from tools.misc import scaledValueInRange

class Speedometer(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, cx, cy, width=None):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Create the surface and the speedometer
        imgPath = 'img/speedometer.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        (spriteW,spriteH) = sprite.get_rect().size
        # Scale the image to the desired width
        if width != None:
            height = width*spriteH/spriteW
            speedometer = pygame.transform.scale(sprite, (width, height))
        else:
            speedometer = sprite
            (width, height) = (spriteW, spriteH)
        # Set the sprit's attributes
        self.image = pygame.Surface((width,height), flags=pygame.SRCALPHA, depth=32)
        self.image = self.image.convert_alpha()
        self.speedometer = speedometer
        self.rect = pygame.Rect(0,0,width, height)
        self.rect.centerx = cx
        self.rect.centery = cy
        if width == None: scaleFactor =  None
        else: scaleFactor = float(width)/spriteW
        # Create the pointer
        self.createPointer(scaleFactor)
        # Create the speedometer center
        self.createSpeedometerCenter(scaleFactor)
        # Draw the speedometer
        self.drawSpeedometer()
        # Draw the pointer & center
        self.targetDeg = 30
        self.deg = 0
        self.drawPointer()

    def createPointer(self, scaleFactor):
        self.pointer = pygame.image.load('img/speedometerPointer.png')
        if scaleFactor != None:
            (pointerW, pointerH) = self.pointer.get_rect().size
            (width, height) = (int(pointerW*scaleFactor), int(pointerH*scaleFactor))
            self.pointer = pygame.transform.scale(self.pointer, (width, height))
        self.pointerRect=self.pointer.get_rect(centerx=self.image.get_rect().centerx, centery=self.image.get_rect().centery)

    def createSpeedometerCenter(self, scaleFactor):
        self.center = pygame.image.load('img/speedometerCenter.png')
        if scaleFactor != None:
            (centerW, centerH) = self.center.get_rect().size
            (width, height) = (int(centerW*scaleFactor), int(centerH*scaleFactor))
            self.center = pygame.transform.scale(self.center, (width, height))
        self.centerRect=self.center.get_rect(centerx=self.image.get_rect().centerx, centery=self.image.get_rect().centery)

    def drawSpeedometer(self):
        print "drawSpeedometer", self.rect
        self.image.blit(self.speedometer, (0,0))

    def drawPointer(self):
        deg = -1*self.deg # Rotate Clockwise
        rotatedPointer = pygame.transform.rotate(self.pointer, deg)
        rotatedPointerRect=rotatedPointer.get_rect(centerx=self.image.get_rect().centerx, centery=self.image.get_rect().centery)
        self.image.blit(rotatedPointer, (rotatedPointerRect.x, rotatedPointerRect.y))
        self.image.blit(self.center, (self.centerRect.x, self.centerRect.y))

    def update(self, scaledVolume, *args):
        """This method is called every clock tick"""
        scaledVolume *= 100 # scaledVolume is is (0,1), not (0, 100)
        # 0 is at 30 degrees, 100 is at 330 degrees
        self.targetDeg = int(30 + 3*scaledVolume)
        self.drawSpeedometer()
        self.drawPointer()

    def move(self):
        print self.deg, self.targetDeg
        self.deg += (self.targetDeg-self.deg)/2

    def getColor(self):
        scaledVolume = (self.deg-30)/3
        # 0 is 0x8bc441, 40 is 0x8bc441, 65 is0xfbed80, 85 is 0xe6721f, 100 is 0xea2127
        if scaledVolume <= 40: (r, g, b) = (0x8b, 0xc4, 0x41)
        elif scaledVolume <= 65:
            r = int(0x8b + float(0xfb-0x8b)/(65-40)*(scaledVolume-40))
            g = int(0xc4 + float(0xed-0xc4)/(65-40)*(scaledVolume-40))
            b = int(0x41 + float(0x80-0x41)/(65-40)*(scaledVolume-40))
        elif scaledVolume <= 85:
            r = int(0xfb + float(0xfb-0xe6)/(85-65)*(scaledVolume-65))
            g = int(0xed + float(0xed-0x72)/(85-65)*(scaledVolume-65))
            b = int(0x80 + float(0x80-0x1f)/(85-65)*(scaledVolume-65))
        elif scaledVolume <= 100:
            r = int(0xe6 + float(0xe6-0xea)/(100-85)*(scaledVolume-85))
            g = int(0x72 + float(0x72-0x21)/(100-85)*(scaledVolume-85))
            b = int(0x1f + float(0x1f-0x27)/(100-85)*(scaledVolume-85))
        r = min(0xff, r)
        g = min(0xff, g)
        b = min(0xff, b)
        return (r, g, b)


    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class VolumeSpeedometer(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=50, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (800, 800)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.elapsedTicks = {} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        self.minExpectedVolume, self.maxExpectedVolume = minExpectedVolume, maxExpectedVolume
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            pass
        else:
            self.speedometer.move()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if volume > 0: # only update if not IOError
                scaledVolume = scaledValueInRange(volume, self.minExpectedVolume, self.maxExpectedVolume)
                print volume, scaledVolume
                self.color = self.speedometer.getColor()
                print self.color
                self.allSpritesList.update(scaledVolume)
        # Update the view
        self.update()

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.buttonsList = pygame.sprite.Group()
        self.allSpritesList = pygame.sprite.Group()
        # Create the speedometer
        self.createSpeedometer()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0

    def createSpeedometer(self):
        self.speedometer = Speedometer(self.screenWidth/2, self.screenHeight/2, width=self.screenWidth/2)
        self.allSpritesList.add(self.speedometer)

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Load the view elements
        self.loadView()
        self.color = (0xFF, 0xFF, 0xFF)
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill(self.color)
        self.allSpritesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = VolumeSpeedometer(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
