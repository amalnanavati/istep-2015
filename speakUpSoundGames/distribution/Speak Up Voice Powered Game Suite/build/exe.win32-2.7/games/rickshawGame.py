import pygame, sys, Tkinter
from pygame.locals import *
from tools.listener import *

class RickshawGame(object):
    def __init__(self, returnToMenu, width = 1000, height = 500, volumeThreshold=500, , minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        pygame.init()
        self.fps = 30
        self.clock = pygame.time.Clock()
        self.font = pygame.font.SysFont("Arial", 200)
        self.width = width
        self.height = height
        self.volumeThreshold = volumeThreshold
        self.listenInterval = 0.010
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(5)
        self.listener = Listener(self.listenInterval*scaleFactor)
        self.listener.startStream()
        self.finished = False

    def initAnimation(self):
        self.loadBackground()
        self.loadRick()
        self.updateRick()

    def loadBackground(self):
        back = pygame.image.load('img/rickshawBackground.jpeg')
        (backW,backH) = back.get_rect().size
        self.backWidth = self.width
        self.backHeight = self.backWidth*backH/backW
        self.backImg = pygame.transform.scale(back,
                                             (self.backWidth,self.backHeight))
        (self.backX, self.backY) = (0,0)

    def loadRick(self):
        rick = pygame.image.load('img/rickshaw.png')
        (rickW,rickH) = rick.get_rect().size
        self.rickWidth = 200
        self.rickHeight = self.rickWidth*rickH/rickW
        self.rickImg = pygame.transform.scale(rick,
                                             (self.rickWidth,self.rickHeight))
        (self.rickX, self.rickY) = (0, self.height-self.rickHeight-50)

    def onClockTick(self):
        if self.finished: return
        (volume, freq) = self.listener.listen()
        if (volume > self.volumeThreshold): self.detectedVoice()

    def detectedVoice(self):
        self.rickX += 5
        self.updateRick()
        if self.isFinished():
            self.finished = True
            self.done()

    def isFinished(self):
        return self.rickX >= self.width-self.rickWidth

    def done(self):
        doneText = self.font.render("Finished!", 1, (255,0,0))
        self.surface.blit(doneText, (self.width/4,50))
        pygame.display.update()
        self.clock.tick(2*self.fps)

    def updateRick(self):
        self.surface.fill((255,255,255))
        self.surface.blit(self.backImg, (self.backX, self.backY))
        self.surface.blit(self.rickImg, (self.rickX, self.rickY))
        pygame.display.update()

    def quit(self):
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        self.surface = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('Move the Rickshaw')
        self.initAnimation()
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == QUIT:
                    print "quit"
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(int(self.fps*self.listenInterval))
            if self.endLoop == True: break

game = RickshawGame(lambda: 1)
game.run()
