################################################################################
# A Shooter Game to help hearing-impaired students understand vocalization and
# timed vocalization.
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *

class Enemy(pygame.sprite.Sprite):
    """The enemy sprite.  Enemy sprites keep track of their own health and move
    themselves.  Drawing the sprite is done in ShooterGame.update()"""

    def __init__(self, screenWidth, y, health, avgWidth):
        """Initializes an enemy with the given position, speed, health, width,
        and image.  Speed refers to change in pixels per clock tick, and health
        refers to the number of bullets it takes to destroy the enemy."""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set Enemy Presets
        self.avgWidth = avgWidth
        self.health = health
        self.setEnemyPresets()
        # Set the sprit's attributes
        x = random.randint(0, screenWidth-self.image.get_rect().width) # Random Position along top of screen
        self.rect = pygame.Rect(x, y, self.image.get_rect().width, self.image.get_rect().height)

    def setEnemyPresets(self):
        # Current enemy presets:
        # Enemy1: speed=1, health=1, width=60, imgPath='img/enemy1.png'
        # Enemy2: speed=1, health=2, width=80, imgPath='img/enemy2.png'
        # Enemy3: speed=2, health=3, width=100, imgPath='img/enemy3.png'
        if self.health == 1:
            width = random.randint(self.avgWidth-20, self.avgWidth-10)
            (speed, imgPath) = (1, 'img/enemy1.png')
        elif self.health == 2:
            width = random.randint(self.avgWidth-10, self.avgWidth+10)
            (speed, imgPath) = (1, 'img/enemy2.png')
        elif self.health == 3:
            width = random.randint(self.avgWidth+10, self.avgWidth+20)
            (speed, imgPath) = (2, 'img/enemy3.png')
        else: return
        self.speed = random.randint(max(speed-1, 1), speed+1) #random speed within a range
        # Load the image
        enemy = pygame.image.load(imgPath)
        # Scale the image to the desired width
        (enemyW,enemyH) = enemy.get_rect().size
        height = width*enemyH/enemyW
        self.image = pygame.transform.scale(enemy, (width, height))
        self.mask = pygame.mask.from_surface(self.image)

    def update(self, screenHeight, *args):
        """This method is called every clock tick from
        ShooterGame.onClockTick().  It moves the enemy."""
        self.move()
        if self.rect.y > screenHeight: self.destroy()

    def move(self):
        """Moves the enemy."""
        self.rect.y += self.speed

    def gotHit(self):
        """Decrements the health by 1.  If enemy has no health left, destroy it.
        This method is called from ShooterGame.checkCollision() whenever a
        bullet hits the enemy."""
        self.health -= 1
        oldRect = self.rect
        self.setEnemyPresets()
        self.rect = self.image.get_rect(centerx=oldRect.centerx, centery=oldRect.centery)


    def getHealth(self):
        """Returns enemy health, called from ShooterGame.checkCollision()"""
        return self.health

    def destroy(self):
        """Destroys the enemy."""
        self.kill()

class Bullet(pygame.sprite.Sprite):
    """The bullet sprite.  Bullet sprites move themselves.  Drawing the sprite
    is done in ShooterGame.update()"""

    def __init__(self, x, y, speed, width=30):
        """Initializes a bullet with the given position and speed.  Position
        refers to the tip of the shooter (bottom-center of the bullet) and speed
        refers to change in pixels per clock tick."""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        bullet = pygame.image.load('img/bullet.png')
        # Scale the image
        (bulletW,bulletH) = bullet.get_rect().size
        height = width*bulletH/bulletW
        image = pygame.transform.scale(bullet, (width, height))
        # Adjust the position (so the bullet is centers and above the shooter)
        x -= width/2
        y -= height
        # Set the sprite's attributes
        # (width, height) = (30, 30)
        # self.image = pygame.Surface([640,480], flags=pygame.SRCALPHA, depth=32)
        # self.image = self.image.convert_alpha()
        # # self.image.fill((0xFF,0xFF,0xFF))
        # pygame.draw.polygon(self.image, (0xFF, 0x00, 0x00), [(10, 10),(20,10),(20,20),(10,20)])
        # self.mask = pygame.mask.from_surface(self.image)
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed*-1 # Bullet always moves up

    def update(self, *args):
        """This method is called every clock tick from
        ShooterGame.onClockTick().  It moves the bullet."""
        self.move()

    def move(self):
        """Moves the bullet.  If the bullet is off the screen, destroys it."""
        self.rect.y += self.speed
        if self.rect.y <= -self.rect.height: #If bullet is off of the screen
            self.destroy()

    def destroy(self):
        """Destroys the bullet.  Called from ShooterGame.checkCollision()"""
        self.kill()

class Shooter(pygame.sprite.Sprite):
    """The Shooter sprite.  The Shooter sprite moves itself, and can shoot
    bullets.  Drawing the sprite is done in ShooterGame.update()"""

    def __init__(self, x, y, speed = 6, width=75):
        """Initializes a bullet witht he given position and speed.  Position
        refers to the tip of the shooter (bottom-center of the bullet) and speed
        refers to change in pixels per clock tick."""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        shooter = pygame.image.load('img/shooter.png')
        # Scale the image
        (shooterW, shooterH) = shooter.get_rect().size
        height = width*shooterH/shooterW
        image = pygame.transform.scale(shooter, (width, height))
        # Set the sprite's attributes
        self.image = image
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y-height, width, height) # NOTE: The given y was the bottom-left corner, since the top-left depends on shooter height
        self.speed = speed

    def update(self, *args):
        """This method is called every clock tick from
        ShooterGame.onClockTick().  It moves the shooter."""
        self.move(args[0])

    def move(self, screenWidth):
        """Moves the shooter."""
        if (self.rect.x < 0 or self.rect.x > screenWidth-self.rect.width):
            self.speed *= -1 # Flip shooter direction
        self.rect.x += self.speed

    def createBullet(self, speed, width=30):
        """Shoots a bullet with the given speed.  Called from
        ShooterGame.detectedVoice()"""
        (x,y) = (self.rect.x+self.rect.width/2, self.rect.y) # tip of shooter
        bullet = Bullet(x, y, speed=speed, width=width)
        return bullet

    def destroy(self):
        """Destroys the shooter."""
        self.kill()

class ShooterGame(object):
    """The ShooterGame.  This class runs the main game loop, manages pygame
    events, controls clock ticks, keeps track of sprites, and draws."""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=70, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1000, 800)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # print defaultW, defaultH, widthHeightRatio, currentW, currentH, actualRatio
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.ticksBetweenBullets = 5 # Min clock ticks between bullets
        self.ticksBetweenEnemies = 80 # Clock ticks between enemies (this value changes)
        self.minTicksBetweenEnemies = 12 # Min clock ticks between enemies
        self.elapsedTicks = {"bullet":6, "enemy":95} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        self.previousVolume = None
        self.volumeJumpThreshold = float(maxExpectedVolume-minExpectedVolume)/4
        # Retrive High Score Info
        try:
            with open('ShooterGameHighScore.txt', 'rt') as doc:
                self.highScore = int(doc.read())
        except IOError:
            self.highScore = None

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over, wait specified delay and then quit game
        if self.finished:
            if "end" in self.elapsedTicks and self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
                self.quit()
        else:
            if self.elapsedTicks["enemy"] > self.ticksBetweenEnemies:
                # Create an enemy with 2/3 chance
                if random.randint(0, 2) < 2:
                    self.createEnemy()
                    self.elapsedTicks["enemy"] = 0
            # Calls every sprite's update method
            self.allSpritesList.update(self.screenWidth)
            self.checkCollision()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (volume > self.volumeThreshold):
                self.detectedVoice(volume)
            self.previousVolume = volume
            # else:
            #     self.previousVolume = None
        # Update the view
        self.update()

    def detectedVoice(self, volume):
        """Called whenever volume above the volumeThreshold is detected"""
        # Only create a bullet if enough ticks have passed since the last one
        if self.previousVolume is not None:
            if (volume-self.previousVolume >= self.volumeJumpThreshold and
                self.elapsedTicks["bullet"] >= self.ticksBetweenBullets):
                self.shoot(volume)
                self.elapsedTicks["bullet"] = 0

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            print "in loop"
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                if event.type == QUIT:
                    print "quit"
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.enemiesList = pygame.sprite.Group()
        self.bulletsList = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        # Creates the shooter
        self.createShooter()

    def createEnemy(self):
        """Creates an enemy, with easier enemies more probable than more
        difficult ones"""
        seed = random.randint(0, 29)
        if seed < 20: # Create Enemy1 with 20/30 chance
            health = 1
            # (width, speed, health, imgPath) = (60, 1, 1, 'img/enemy1.png')
        elif seed < 29: # Create Enemy2 with 9/30 chance
            health = 2
            # (width, speed, health, imgPath) = (80, 1, 2, 'img/enemy2.png')
        else: # Create Enemy3 with 1/30 chance
            health = 3
            # (width, speed, health, imgPath) = (100, 2, 3, 'img/enemy3.png')
        enemy = Enemy(screenWidth=self.screenWidth, y=0, health=health, avgWidth=self.screenWidth/13)
        self.enemiesList.add(enemy)
        self.allSpritesList.add(enemy)

    def createShooter(self):
        """Loads a shooter sprite and adds it the the spritesList."""
        self.shooter = Shooter(0, self.screenHeight, speed=self.screenWidth/120, width=self.screenWidth/12)
        self.allSpritesList.add(self.shooter)

    def checkCollision(self):
        """Checks whether an enemy has collided with a bullet and/or with the
        shooter"""
        for enemy in self.enemiesList:
            for bullet in self.bulletsList:
                if pygame.sprite.collide_mask(bullet, enemy):
                    bullet.destroy()
                    enemy.gotHit()
                    self.score += 2
                    # enemies come faster
                    self.ticksBetweenEnemies = max(0.97*self.ticksBetweenEnemies,
                                                  self.minTicksBetweenEnemies)
                    if enemy.getHealth() <= 0:
                        enemy.destroy()
                        self.score += 4
                    self.updateScoreText()
            if pygame.sprite.collide_mask(enemy, self.shooter):
                self.gameOver()

    def shoot(self, volume):
        """Has the shooter shoot a bullet, with a speed proportional to the
        volume"""
        minBulletSpeed = 10
        bullet = self.shooter.createBullet(volume-self.volumeThreshold+minBulletSpeed, width=self.screenWidth/35)
        self.bulletsList.add(bullet)
        self.allSpritesList.add(bullet)

    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!  Score: %d" %(self.score)
        # self.elapsedTicks["end"] = 0
        self.delayBeforeClosingGame = 2 #seconds
        if self.highScore is None or self.score > self.highScore:
            try:
                with open('ShooterGameHighScore.txt', 'wt') as doc:
                    doc.write(str(self.score))
            except IOError:
                pass
            isHighScore = True
        else:
            isHighScore = False
        self.createGameOverText(isHighScore=isHighScore)
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        # self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Initialize the font
        self.fontHeight = 30
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        # Load the view
        self.createMenuButton()
        self.loadBackground()
        self.updateScoreText()
        self.updateHighScoreText()
        # Display the view
        self.update()

    def loadBackground(self):
        """Load the background, an RGB peach color"""
        background = pygame.image.load('img/spaceshipsBackground.png')
        (backgroundW,backgroundH) = background.get_rect().size
        backgroundRatio=float(backgroundW)/backgroundH
        screenRatio = float(self.screenWidth)/self.screenHeight
        if backgroundRatio > screenRatio:
            height = self.screenHeight
            width = height*backgroundW/backgroundH
        else:
            width = self.screenWidth
            height = width*backgroundH/backgroundW
        self.background = pygame.transform.scale(background, (width, height))

    def updateScoreText(self):
        """Updates the score text with the current score"""
        self.scoreText = self.font.render("Score:"+str(self.score), 1, (0,0,0))

    def updateHighScoreText(self):
        self.highScoreText = self.font.render("High Score: "+str(self.highScore), 1, (0,0,0))

    def createGameOverText(self, isHighScore=False):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        if isHighScore:
            text = "Game Over. High Score!"
        else:
            text = "Game Over"
        self.gameOverText = font.render(text, 1, (0,0,0))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.blit(self.background, (0,0))
        self.allSpritesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        self.surface.blit(self.scoreText, (0, 0))
        self.surface.blit(self.highScoreText, (0,self.fontHeight*3/2))
        if self.finished:
            self.surface.blit(self.gameOverText, self.gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/2))
        pygame.display.update()

# game = ShooterGame(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
