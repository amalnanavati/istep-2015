################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.misc import scaledValueInRange
from tools.button import *

class Fish(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, maxSpeed=20):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        fish = pygame.image.load('img/fish1.png')
        # Scale the image
        (fishW,fishH) = fish.get_rect().size
        width = 125
        height = width*fishH/fishW
        self.image = pygame.transform.scale(fish, (width, height))
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y-height/2, width, height)
        self.startSpeed = 2
        self.speed = self.startSpeed
        self.dir = 0
        self.speedMultiplier = 1 # acceleration/deceleration
        self.maxSpeed = maxSpeed

    def update(self, targetY, *args):
        """This method is called every clock tick"""
        if self.rect.y < targetY-self.speed: self.dir = +1 # move down
        elif self.rect.y > targetY+self.speed: self.dir = -1 # move up
        else:
            self.dir = 0 #stay
            self.speed = self.startSpeed
        if (abs(self.rect.y-targetY)/self.speed < 5): self.decelerate()
        print targetY, self.rect.y
        self.move()

    def move(self):
        """Moves the sprite"""
        self.rect.y += int(self.speed*self.dir)
        print "speed", self.speed

    def accelerate(self, factor=1.1):
        """Accelerates the rabbit towards the targetY"""
        if self.speed < 2: self.speed = self.startSpeed
        self.speed *= factor
        if self.speed > self.maxSpeed: self.speed = self.maxSpeed

    def decelerate(self, factor = 0.85):
        """Decelerates the rabbit towards targetY"""
        self.speed *= factor

    def setStartSpeed(self):
        self.speed = self.startSpeed

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Obstacle(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, speed = 5, width=100, height=200):
        """Initializes an obstacle with the above attributes"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        self.imgPath = random.choice(['img/pirhanna1.png','img/pirhanna2.png','img/turtle.png', 'img/shark.png', 'img/walrus.png'])
        obstacle = pygame.image.load(self.imgPath)
        # Scale the image
        (obstacleW,obstacleH) = obstacle.get_rect().size
        width = random.randint(125, 175)
        height = width*obstacleH/obstacleW
        self.image = pygame.transform.scale(obstacle, (width, height))
        self.mask = pygame.mask.from_surface(self.image)
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed*-1 # Always move left

    def update(self, *args):
        """This method is called every clock tick"""
        self.move()
        if self.rect.x < -1*self.rect.width: self.destroy()

    def move(self):
        """Moves the sprite"""
        self.rect.x += self.speed
        if 'walrus' in self.imgPath:
            verticalMovement = 5
            self.rect.y += random.randint(-verticalMovement, verticalMovement)

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Star(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, speed = 5, width=100, height=100):
        """Initializes an obstacle with the above attributes"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        self.pathsAndScores = {'img/star1.png':1,'img/star2.png':2,'img/star5.png':5}
        self.imgPath = random.choice(self.pathsAndScores.keys())
        obstacle = pygame.image.load(self.imgPath)
        # Scale the image
        (obstacleW,obstacleH) = obstacle.get_rect().size
        width = random.randint(125, 175) if "5" in self.imgPath else (random.randint(100, 125) if "2" in self.imgPath else random.randint(75, 100))
        height = width*obstacleH/obstacleW
        self.image = pygame.transform.scale(obstacle, (width, height))
        self.mask = pygame.mask.from_surface(self.image)
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed*-1 # Always move left

    def update(self, *args):
        """This method is called every clock tick"""
        self.move()
        if self.rect.x < -1*self.rect.width: self.destroy()

    def move(self):
        """Moves the sprite"""
        self.rect.x += self.speed
        print "STAR", self.rect.x, self.rect.y
        verticalMovement = 4*self.pathsAndScores[self.imgPath]
        self.rect.y += random.randint(-verticalMovement, verticalMovement)

    def score(self):
        return self.pathsAndScores[self.imgPath]

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Background(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, height, screenWidth):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        imgPath = 'img/waterBackground.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        (spriteW,spriteH) = sprite.get_rect().size
        width = height*spriteW/spriteH
        backgroundImg = pygame.transform.scale(sprite, (width, height))
        # Set the sprit's attributes
        self.numberOfImages = max(int(math.ceil(float(2)*screenWidth/width)), 4)
        print self.numberOfImages
        self.image = pygame.Surface((self.numberOfImages*width, height))
        for i in xrange(self.numberOfImages):
            self.image.blit(backgroundImg, (width*i, 0))
        self.rect = pygame.Rect(x, y, self.numberOfImages*width, height)

    def shiftOverBy(self, dPoint):
        self.rect.x += dPoint[0]
        self.rect.y += dPoint[1]
        if self.rect.x <= -2*self.rect.width/self.numberOfImages: self.rect.x = -self.rect.width/self.numberOfImages

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class FishGameWithArrows(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH,volumeThreshold=50, minExpectedVolume=60, maxExpectedVolume=80, timeLimit=None):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume, timeLimit)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume, timeLimit):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (700, 700)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        self.volumeThreshold = volumeThreshold
        print "FishGameWithArrows VolumeThreshold: ", volumeThreshold
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.timeLimit = timeLimit
        self.ticksBeforeNextObstacles = 100 # Changed every time an obstacle is created
        self.ticksBeforeNextStar = 150 # Changed every time an star is created
        self.ticksBeforeUpdatingScore = self.fps
        self.elapsedTicks = {"obstacle": 90, "star": 70} # Keeps track of elapsed time since events
        self.time = 0
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        # Set expected pitch levels
        self.minExpectedVolume = minExpectedVolume
        self.maxExpectedVolume = maxExpectedVolume
        self.prevVoice = False
        self.currVoice = False
        self.currentDirection = 0
        # Retrive High Score Info
        if self.timeLimit is None:
            self.highscoreFilename = 'FishGameWithArrowsHighScore.txt'
        else:
            self.highscoreFilename = 'FishGameWithArrowsHighScore%d.txt' % self.timeLimit
        try:
            with open(self.highscoreFilename, 'rt') as doc:
                self.highScore = int(doc.read())
        except IOError:
            self.highScore = None
        # Retrive Best Time Info
        self.bestTimeFilename = 'FishGameWithArrowsBestTime.txt'
        try:
            with open(self.bestTimeFilename, 'rt') as doc:
                self.bestTime = int(doc.read())
        except IOError:
            self.bestTime = None

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            if "end" in self.elapsedTicks and self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
                self.quit()
        else:
            self.time += 1
            if self.timeLimit is not None and int(self.time*self.timeBetweenFrames) >= self.timeLimit:
                self.gameOver()
                return
            self.background.shiftOverBy((-1*self.screenSpeed, 0))
            if self.elapsedTicks["obstacle"] > self.ticksBeforeNextObstacles:
                self.createObstacle()
            if self.elapsedTicks["star"] > self.ticksBeforeNextStar:
                self.createStar()
            # Calls every sprite's update method
            self.allSpritesList.update(self.targetY)
            self.starsList.update()
            # Checks for any collisions
            self.checkCollision()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (volume > self.volumeThreshold or pitch != None):
                self.prevVoice = self.currVoice
                self.currVoice = True
                self.detectedVoice(volume, pitch)
            else:
                self.prevVoice = self.currVoice
                self.currVoice = False
                self.noVolume()
            # if self.prevVoice != self.currVoice: # if you change from voice to no voice or voice versa, change start speed
            #     self.fish.setStartSpeed()
                # self.fish.decelerate()
        # Update the view
        self.update()

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        scaledY = scaledValueInRange(volume, self.minExpectedVolume, self.maxExpectedVolume)
        # print "detectedVoice", scaledY
        # scaledY = 1-scaledY # Lower volume moves down (increases y)
        minSpeed = 2
        maxSpeed = self.fishMaxSpeed
        targetSpeed = scaledY*(maxSpeed - minSpeed) + minSpeed
        if self.currentDirection == -1:
            self.targetY = 0#(self.screenHeight-self.fish.rect.height)*scaledY
            # self.fish.accelerate(1.0+scaledY**6.0)
        elif self.currentDirection == 1:
            self.targetY = self.screenHeight-self.fish.rect.height
            # self.fish.accelerate(1.0+scaledY**6.0)
        else:
            targetSpeed = 0
        if self.fish.speed < targetSpeed:
            self.fish.accelerate()
        else:
            self.fish.decelerate()

    def noVolume(self):
        print "no voice"
        self.fish.decelerate()
        # self.targetY = self.screenHeight-self.fish.rect.height
        # self.fish.accelerate(1.025) # If you haven't been talking for a while

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                    elif event.key == K_DOWN:
                        self.currentDirection = 1
                    elif event.key == K_UP:
                        self.currentDirection = -1
                if event.type == KEYUP:
                    if event.key == K_DOWN and self.currentDirection == 1:
                        self.currentDirection = 0
                    if event.key == K_UP and self.currentDirection == -1:
                        self.currentDirection = 0
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                if event.type == QUIT:
                    print "quit"
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.OrderedUpdates()
        self.buttonsList = pygame.sprite.Group()
        self.starsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        #  Create Fish
        self.createBackground()
        self.createFish()
        # The y position the rabbit is moving towards
        self.targetY = self.screenHeight/2
        rateOfMovingDownOnNoVoice = (self.screenHeight-self.fish.rect.height)/10.0 # pixels/sec
        self.rateOfMovingDownOnNoVoice = int(rateOfMovingDownOnNoVoice/self.fps)
        # Sets attributes related to obstacles
        self.screenSpeed = 8
        self.minDistanceBetweenObstacles = self.screenWidth/12
        self.maxDistanceBetweenObstacles = self.screenWidth/3
        self.distanceBetweenObstacle = random.randint(self.minDistanceBetweenObstacles, self.maxDistanceBetweenObstacles)
        self.minDistanceBetweenStars = self.screenWidth/3
        self.maxDistanceBetweenStars = 2*self.screenWidth
        self.distanceBetweenStars = random.randint(self.minDistanceBetweenStars, self.maxDistanceBetweenStars)

    def createFish(self):
        """Create the flying rabbit"""
        self.fishMaxSpeed = 20
        self.fish = Fish(self.screenWidth/20, self.screenHeight/2, maxSpeed=self.fishMaxSpeed)
        self.allSpritesList.add(self.fish)

    def createBackground(self):
        self.background = Background(0,0,self.screenHeight, self.screenWidth)
        self.allSpritesList.add(self.background)

    def createObstacle(self):
        """Create the obstacle"""
        # Creates an obstacle with 2/3 chance
        if random.randint(0,2) < 2:
            (width, height) = (random.randint(25, 175), random.randint(1, 200))
            obstacle = Obstacle(self.screenWidth, random.randint(0, self.screenHeight-height),
                                speed=random.randint(max(2, self.screenSpeed/2), self.screenSpeed*3/2), width=width, height=height)
            self.allSpritesList.add(obstacle)
            self.distanceBetweenObstacle = random.randint(self.minDistanceBetweenObstacles, self.maxDistanceBetweenObstacles)
            self.ticksBeforeNextObstacles = (width+self.distanceBetweenObstacle)/self.screenSpeed
            self.elapsedTicks["obstacle"] = 0

    def createStar(self):
        """Create the star"""
        # # Creates a star with 2/3 chance
        # if random.randint(0,2) < 2:
        # (width, height) = (random.randint(25, 100), random.randint(25, 100))
        obstacle = Star(self.screenWidth, random.randint(0, self.screenHeight-200),
                            speed=random.randint(max(2, self.screenSpeed/2), self.screenSpeed*3/2))
        self.starsList.add(obstacle)
        self.distanceBetweenStars = random.randint(self.minDistanceBetweenStars, self.maxDistanceBetweenStars)
        self.ticksBeforeNextStar = (self.distanceBetweenStars)/self.screenSpeed
        self.elapsedTicks["star"] = 0
        print "created Star"

    def checkCollision(self):
        """Checks whether an enemy has collided with a bullet and/or with the
        shooter"""
        collided = pygame.sprite.spritecollide(self.fish, self.allSpritesList, False, collided=pygame.sprite.collide_mask)
        if len(collided) > 1:
            self.gameOver()
        collided = pygame.sprite.spritecollide(self.fish, self.starsList, False, collided=pygame.sprite.collide_mask)
        for star in collided:
            self.maxDistanceBetweenObstacles = max(self.minDistanceBetweenObstacles, int(0.97*self.maxDistanceBetweenObstacles)) # Lower the max distance between stars
            self.score += star.score()
            star.destroy()
            self.updateScoreText()

    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!  Score: %d" %(self.score)
        # self.elapsedTicks["end"] = 0
        self.delayBeforeClosingGame = 5 # seconds
        if self.highScore is None or self.score > self.highScore:
            try:
                with open(self.highscoreFilename, 'wt') as doc:
                    doc.write(str(self.score))
            except IOError:
                pass
            isHighScore = True

        else:
            isHighScore = False
        print "At Game Over", self.timeLimit
        if self.timeLimit is None and (self.bestTime is None or self.time > self.bestTime):
            try:
                with open(self.bestTimeFilename, 'wt') as doc:
                    doc.write(str(self.time))
            except IOError:
                pass
            isBestTime = True
        else:
            isBestTime = False
        self.createGameOverText(isHighScore=isHighScore, isBestTime=isBestTime)
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Initialize the font
        self.fontHeight = 30
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        # Load the initial view elements
        self.createMenuButton()
        self.updateTimeText()
        self.updateScoreText()
        self.updateHighScoreText()
        if self.timeLimit is None: self.updateBestTimeText()
        # Display the view
        self.update()

    def updateTimeText(self):
        """Updates the score text with the current score"""
        if self.timeLimit is None:
            seconds = int(self.time*self.timeBetweenFrames)
            # print seconds
            minutes = seconds/60
            seconds %= 60
            text = "Time: %d:%02d" %(minutes, seconds)
        else:
            seconds = self.timeLimit-int(self.time*self.timeBetweenFrames)
            # print seconds
            minutes = seconds/60
            seconds %= 60
            text = "Remaining Time: %d:%02d" %(minutes, seconds)
        self.timeText = self.font.render(text, 1, (0,0,0))

    def updateScoreText(self):
        """Updates the score text with the current score"""
        self.scoreText = self.font.render("Score: %d" %(self.score), 1, (0,0,0))

    def updateHighScoreText(self):
        self.highScoreText = self.font.render("High Score: "+str(self.highScore), 1, (0,0,0))

    def updateBestTimeText(self):
        if self.bestTime is None:
            self.bestTimeText = self.font.render("Longest Time: "+str(self.bestTime), 1, (0,0,0))
        else:
            seconds = int(self.bestTime*self.timeBetweenFrames)
            # print seconds
            minutes = seconds/60
            seconds %= 60
            self.bestTimeText = self.font.render("Longest Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))

    def createGameOverText(self, isHighScore=False, isBestTime=False):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        if self.timeLimit is None and isHighScore and isBestTime:
            text = "Game Over. High Score and Best Time!"
        elif isHighScore:
            text = "Game Over. High Score!"
        elif isBestTime and self.timeLimit is None:
            text = "Game Over. Best Time!"
        else:
            text = "Game Over"
        self.gameOverText = font.render(text, 1, (0x00,0x00,0x00))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.allSpritesList.draw(self.surface)
        self.starsList.draw(self.surface)
        # print "drew starsList", self.starsList
        self.updateTimeText()
        self.surface.blit(self.scoreText, (0,0))
        self.surface.blit(self.timeText, (0,self.fontHeight*3/2))
        self.surface.blit(self.highScoreText, (0,self.fontHeight*3))
        if self.timeLimit is None: self.surface.blit(self.bestTimeText, (0,self.fontHeight*9/2))
        self.buttonsList.draw(self.surface)
        if self.finished:
            self.surface.blit(self.gameOverText, self.gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/2))
        pygame.display.update()

# game = FishGameWithArrows(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
