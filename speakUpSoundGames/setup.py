import cx_Freeze

executables = [cx_Freeze.Executable("menu.py")]

cx_Freeze.setup(
    name="Sound Games",
    options={"build_exe": {"packages":["pyaudio", "numpy", "pygame", "analyse", "PIL", "serial.tools.list_ports"],
                           "include_files":["games", "menuItems.txt", "calibrate.py", "calibrate.pyc", "BackgroundNoise.txt", "licenseInfo.txt", "expectedSignBookLocation.txt", "MaxExpectedNoise.txt", "MinExpectedNoise.txt"],
                           "copy_dependent_files":True,
                           "append_script_to_exe":False}},
    executables = executables

    )
