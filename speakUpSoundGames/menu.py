################################################################################
# Menu of the Sound Games suite.
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
#       - Maya Lassiter (mlassite@andrew.cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random, os, importlib, wave, struct, math, time, json, PIL, serial.tools.list_ports
import shutil
from pygame.locals import *
from games.tools.button import *
from games.tools.wordAndScroller import *

class Menu(object):
    """The class that draws the menu, organizes buttons, and tracks button clicks."""

    ############################################################################
    # Initialization Functions
    ############################################################################

    def __init__(self):
        """Initializes the menu"""
        # Initialize PyGame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set the font
        self.font = pygame.font.SysFont("Arial", 200)
        # Set the screen size attributes
        # (defaultW, defaultH) = (1000, 1000)
        # widthHeightRatio=float(defaultW)/defaultH
        displayInfo = pygame.display.Info()
        # actualRatio = float(displayInfo.current_w)/displayInfo.current_h
        # if actualRatio > widthHeightRatio:
        #     self.height = min(displayInfo.current_h, defaultH)
        #     self.width = int(widthHeightRatio*self.height)
        # else:
        #     self.width = min(displayInfo.current_w, defaultW)
        #     self.height = int(self.width/widthHeightRatio)
        (self.width, self.height) = (displayInfo.current_w, displayInfo.current_h)
        (self.currentW, self.currentH) = (displayInfo.current_w, displayInfo.current_h)
        print self.currentW, self.currentH
        # Read data from menuItems.txt
        self.menuItems = self.readMenuItems('menuItems.txt')
        # Creates a list of colors for the different categories
        self.colorsList = [[(0xcc, 0xff, 0xff),(0xbd, 0xd5, 0xe1)],[(0xcc, 0xff, 0xcc),(0x8b, 0xd1, 0x8f)],[(0xff, 0xff, 0xcc),(0xea, 0xde, 0x7d)]]
        self.quitButtonColor = (0x25, 0x9b, 0x78)
        self.calibrateButtonColor = (0xed, 0xb6, 0x70)
        self.aboutButtonColor = (0xed, 0xb6, 0x70)
        # Set submenu ellie names
        self.submenuEllies = ['games/img/categoryOneEllies.png', 'games/img/categoryTwoEllies.png', 'games/img/categoryThreeEllies.png']
        # Logs the time the game was opened in the speakUpUsageLog.txt file
        self.textLog = "\n\n"+time.strftime("%c")
        self.log()
        # Read the SignBook words and pictures and check for changes
        self.speakUpRelativePathToImgs = "games/img/PictureThatImages/SignBookCopy/www/db/imgs"
        self.speakUpRelativePathToObjs = "games/img/PictureThatImages/SignBookCopy/www/db/objs"
        self.syncToSignBook('expectedSignBookLocation.txt', )
        # Create button sprites
        self.drawMenu()
        # Creates a dict of imported games (to avoid repeated imports)
        self.importedGames = {}

    def readMenuItems(self, path):
        """Reads in data about how to organize menu buttons from the
        menuItems.txt file"""
        with open(path, 'rt') as menuItemsFile:
            menuItemsText = menuItemsFile.read()
            menuItemsData = menuItemsText.split("\n\n")
            # menuItemNames = [] # Prevent same game with different difficulties from being entered in menu
            menuItems = []
            for itemAttributes in menuItemsData:
                itemAttributes = itemAttributes.splitlines()
                item = {}
                # name = itemAttributes[0].split(': ')[1]
                # if name in menuItemNames: continue
                for attribute in itemAttributes:
                    data = attribute.split(': ')
                    (key, value) = data
                    if value.isdigit(): value = int(value)
                    item[key] = value
                menuItems.append(item)
                # menuItemNames.append(name)
            return menuItems

    def readQuizWords(self, path):
        """Reads in data about how to organize menu buttons from the
        quizWords.txt file"""
        with open(path, 'rt') as quizWordsFile:
            quizWordsText = quizWordsFile.read()
            quizWordsData = quizWordsText.split("\n")
            # menuItemNames = [] # Prevent same game with different difficulties from being entered in menu
            quizWords = []
            for word in quizWordsData:
                strippedWord = word.strip()
                if len(strippedWord) > 0:
                    quizWords.append(strippedWord)
                # menuItemNames.append(name)
            return quizWords

    def writeQuizWords(self, path, quizWords):
        """Reads in data about how to organize menu buttons from the
        quizWords.txt file"""
        quizWordsText = ""
        for word in quizWords:
            quizWordsText += word + "\n"
        quizWordsText = quizWordsText.strip()
        with open(path, 'wt') as quizWordsFile:
            quizWordsFile.write(quizWordsText)

    def syncToSignBook(self, path):
        speakUpCorrupted = False
        try:
            with open(path, 'rt') as doc:
                # Load possible paths for Sign Book and ensure it is a list
                signBookPaths = eval(doc.read()) # Generally, evalling is bad.  However, we are making our source code available to the users, they already have the capability to maliciously corrupt it.  So evaling from the text file doesn't open and new vulnerabilities
                print signBookPaths
                assert(type(signBookPaths)==list)
                # The subpaths within SignBook's directory where we expect to find the imgs and objs
                if sys.platform == "darwin": # mac osx
                    imgsSubpath = "www/db/imgs"
                    objsSubpath = "www/db/objs"
                elif sys.platform == "win32":
                    print "On Windows"
                    imgsSubpath = "www\\db\\imgs"
                    objsSubpath = "www\\db\\objs"
                else:
                    imgsSubpath = "www/db/imgs"
                    objsSubpath = "www/db/objs"
                    print "untested platform"
                # The local directories within Speak Up where we expect to find the imgs and objs
                try:
                    speakUpImgs = set(os.listdir(self.speakUpRelativePathToImgs))
                except:
                    speakUpCorrupted = True
                    raise Exception("Speak Up Imgs Corrupted")
                foundImgs = False
                foundObjs = False
                for i in xrange(len(signBookPaths)-1, -1, -1):
                    print "at index %d of signbook list" % i
                    if foundImgs and foundObjs: break
                    timestamp, path = signBookPaths[i]
                    path = os.path.expanduser(path)
                    # quit = [False] # list so it is mutable
                    visited = set() # to prevent infinite looping as a result of followlinks
                    for dirpath, dirnames, filenames in os.walk(path, topdown=True, onerror=None, followlinks=True):
                        print dirpath, dirnames, filenames
                        if foundImgs and foundObjs: break
                        # NOTE (amal): If SignBook's internal directory structure changes, this must also be changed
                        if "www" in dirnames:
                            print "FOUND WWW", dirpath, dirnames, filenames
                            dirnames[0] = "www"
                            for i in xrange(1, len(dirnames)):
                                del dirnames[1]
                        elif "db" in dirnames:
                            print "FOUND db", dirpath, dirnames, filenames
                            dirnames[0] = "db"
                            for i in xrange(1, len(dirnames)):
                                del dirnames[1]
                        elif "imgs" in dirnames:
                            if "objs" in dirnames:
                                print "FOUND imgs and obj", dirpath, dirnames, filenames
                                dirnames[0] = "imgs"
                                dirnames[1] = "objs"
                                for i in xrange(2, len(dirnames)):
                                    del dirnames[2]
                            else:
                                print "FOUND just imgs :(", dirpath, dirnames, filenames
                                dirnames[0] = "imgs"
                                for i in xrange(1, len(dirnames)):
                                    del dirnames[1]
                        # if quit[0]: break
                        if dirpath in visited: continue
                        visited.add(dirpath)
                        if imgsSubpath in dirpath: # found imgs folder
                            print "in imgs Subpath"
                            # Compare the list of files in the imgs folder to the list of files in Speak Up's local copy
                            for imgName in filenames:
                                if imgName not in speakUpImgs:
                                    # Copy the file over
                                    shutil.copy2(dirpath+"/"+imgName, self.speakUpRelativePathToImgs+"/"+imgName)
                            foundImgs = True
                        elif objsSubpath in dirpath: # found objs folder
                            print "in objs subpath"
                            # Always copy the files over, as they are just two small text files
                            for filename in filenames:
                                shutil.copy2(dirpath+"/"+filename, self.speakUpRelativePathToObjs+"/"+filename)
                            foundObjs = True
                if not (foundObjs and foundImgs): raise Exception("fake exception to jump to except if none of the paths can find imgs and objs")
        except:
            print "Got Exception!"
            # TODO (amal): give the user the option to locate sign book themselves
            if speakUpCorrupted:
                pass
            else:
                pass
            return


    ############################################################################
    # Draw Menu Functions
    ############################################################################

    def drawMenu(self, fullscreen=True):
        # Jank way to solve the problem where the PictureThat submenu needs to redraw on a clock tick, but the other menus don't
        self.isDisplayingPictureThatSubmenu = False
        self.scrollerList = pygame.sprite.Group()
        self.shouldProcessEvents = False
        self.framesSinceSetshouldProcessEventsToFalse = 0
        self.startProcessingEventsAfter = int(0.5*self.fps) # 0.5 seconds
        # Initialize the screen
        self.fullscreen = fullscreen
        if fullscreen: self.surface = pygame.display.set_mode((self.width, self.height), pygame.FULLSCREEN, 32)
        else: self.surface = pygame.display.set_mode((self.width, self.height))
        # self.surface = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('Sound Games')
        self.buttonsList = pygame.sprite.Group()
        # Draw the menu
        self.drawMenuBackground()
        self.createQuitButton()
        self.createCalibrateButton()
        self.createAboutButton()
        self.createMenuButtons()
        self.drawMenuTitle()
        self.drawButtons()
        # Display the menu
        pygame.display.update()
        self.shouldProcessEvents = True

    def drawMenuBackground(self):
        backGroundRGB = (0xFF, 0xFF, 0xFF)
        self.surface.fill(backGroundRGB)
        categories = self.filterMenuItemsByKey("Menu Category")
        if len(self.colorsList) != len(categories): raise Exception("Need more colors!")
        for index, category in enumerate(categories):
            height = self.height*3/4/len(categories)
            y0 = self.height/4+height*categories.index(category)
            x0 = 0
            width = self.width
            color = self.colorsList[index][0]
            print color
            pygame.draw.rect(self.surface, color, pygame.Rect(x0,y0,width,height))
        # ellie = pygame.image.load('games/img/ellie-two.png')
        # (ellieW, ellieH) = ellie.get_rect().size
        # width = 175
        # height = width*ellieH/ellieW
        # ellie = pygame.transform.scale(ellie, (width, height))
        # self.surface.blit(ellie, (self.width/2-width/2, self.height/4-height))

    def drawMenuTitle(self):
        title = pygame.image.load('games/img/title.png')
        (titleW, titleH) = title.get_rect().size
        height = self.height/4
        width = height*titleW/titleH
        title = pygame.transform.scale(title, (width, height))
        self.surface.blit(title, (self.width/2-width/2, self.height/8-height/2))

    def maxGamesInACategory(self):
        gamesPerCategory = dict()
        for gameData in self.menuItems:
            category = gameData["Menu Category"]
            name = gameData["Menu Button Text"]
            if category in gamesPerCategory: gamesPerCategory[category].add(name)
            else: gamesPerCategory[category] = set([name])
        for category in gamesPerCategory:
            gamesPerCategory[category] = len(gamesPerCategory[category])
        return max(gamesPerCategory.values())

    def createMenuButtons(self):
        createdButtons = set()
        categories = self.filterMenuItemsByKey("Menu Category")
        maxGamesInACategory = self.maxGamesInACategory()
        print maxGamesInACategory
        for gameData in self.menuItems:
            (name, category) = (gameData["Menu Button Text"], gameData["Menu Category"])
            if (name, category) in createdButtons: continue
            menuButtonText = self.getButtonTextForCategory(category)
            print category, menuButtonText
            index = menuButtonText.index(name)
            (width, height) = (self.width/(maxGamesInACategory+1), (self.height*17/20)/(len(categories)+1))
            y0 = self.height/4+self.height*3/4/len(categories)*categories.index(category)
            y1 = self.height/4+self.height*3/4/len(categories)*(categories.index(category)+1)
            cy = (y0+y1)/2
            x0 = (self.width-len(menuButtonText)*width)/(len(menuButtonText)+1)
            (x, y) = (x0*(index+1)+width*index, cy-height/2)
            color = self.colorsList[categories.index(category)][1]
            imgPath = gameData["Title Path"]
            # color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
            button = Button(x, y, color=color, width=width, height=height, callbackArgument=(name,category, imgPath),
                 callback=lambda data: self.drawSubmenu(data, self.fullscreen),
                 text=name, imgPath=gameData["Image Path"], fontSize=self.height/30)
            self.buttonsList.add(button)
            createdButtons.add((name, category))

    def createQuitButton(self):
        margin = 20
        (width, height) = (self.width/8, self.height/20)
        (x,y) = (margin, margin+self.height/5)
        quitGame = Button(x, y, color=self.quitButtonColor, width=width, height=height,
                 callback=lambda data: self.quit(), text="Quit", fontSize=self.height/30)
        self.buttonsList.add(quitGame)

    def createCalibrateButton(self):
        margin = 20
        (width, height) = (self.width/3, self.height/20)
        (x,y) = (self.width-margin-width, margin+self.height/5)
        calibrateButton = Button(x, y, color=self.calibrateButtonColor, width=width, height=height,
                 callback=lambda data: self.calibrate(), text="Remove Background Noise", fontSize=self.height/30)
        self.buttonsList.add(calibrateButton)

    def createAboutButton(self):
        margin = 20
        (width, height) = (self.width/20, self.height/30)
        (x,y) = (margin, margin)
        aboutButton = Button(x, y, color=self.aboutButtonColor, width=width, height=height,
                 callback=lambda data: self.licenceInfo(), text="About", fontSize=self.height/50)
        self.buttonsList.add(aboutButton)

    def getButtonTextForCategory(self, category):
        menuButtonText = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category:
                if gameData["Menu Button Text"] not in menuButtonText:
                    menuButtonText.append(gameData["Menu Button Text"])
        return menuButtonText

    def filterMenuItemsByKey(self, key, allowDuplicates=False):
        result = []
        for gameData in self.menuItems:
            if key not in gameData: raise Exception("Could not find key")
            if allowDuplicates: result.append(gameData[key])
            elif gameData[key] not in result: result.append(gameData[key])
        return result

    def drawButtons(self):
        print self.buttonsList
        self.buttonsList.draw(self.surface)

    ############################################################################
    # Draw Submenu Functions
    ############################################################################

    def drawSubmenu(self, data, fullscreen):
        self.shouldProcessEvents = False
        self.framesSinceSetshouldProcessEventsToFalse = 0
        self.startProcessingEventsAfter = int(0.5*self.fps) # 0.5 seconds
        (name, category, imgPath) = data
        self.textLog += "\nClicked on %s button" %(name)
        categories = self.filterMenuItemsByKey("Menu Category")
        color = self.colorsList[categories.index(category)][0]
        print data
        # Initialize the screen
        if fullscreen: self.surface = pygame.display.set_mode((self.width, self.height), pygame.FULLSCREEN, 32)
        else: self.surface = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("%s: %s" %(category, name))
        self.buttonsList = pygame.sprite.Group()
        # Draw the menu
        self.drawSubmenuBackground(category)
        self.drawSubmenuTitle(imgPath)
        self.drawInstructions(name, category)
        self.createBackButton(category)
        self.drawButtons()
        if "Picture That!" in name: self.createPictureThatSubmenuButtons(name, category, color)
        elif "Robotic Car" in name: self.createRoboticCarSubmenuButtons(name, category, color)
        elif "Volume Meter" in name: self.createVolumeMeterSubmenuButtons(name, category, color)
        elif "Fish Game" in name: self.createFishGameSubmenuButtons(name, category, color)
        elif "Fruit Basket" in name: self.createFruitBasketSubmenuButtons(name, category, color)
        else: self.createSubmenuButtons(name, category, color)
        self.drawButtons()
        self.drawSubmenuEllies(name, category)
        if name == "Picture That!": self.scrollerList.draw(self.surface)
        # Display the menu
        pygame.display.update()

    def drawSubmenuTitle(self, imgPath):
        print imgPath
        title = pygame.image.load(imgPath)
        (titleW, titleH) = title.get_rect().size
        width = self.width*8/10
        height = width*titleH/titleW
        title = pygame.transform.scale(title, (width, height))
        self.surface.blit(title, (self.width/2-width/2, self.height/8-height/2))

    def createSubmenuButtons(self, name, category, color):
        submenuButtons = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                submenuButtons.append(gameData)
        for index, gameData in enumerate(submenuButtons):
            (width, height) = (self.width/5, self.height/10)
            (x, y) = (self.width*(index+1)/(len(submenuButtons)+1)-width/2, self.height*11/20-height/2)
            fontSize = 50
            button = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: self.openGame(data), callbackArgument=gameData,
                 text=gameData["Instructions Menu Button Text"], fontSize=fontSize)
            self.buttonsList.add(button)

    def createFruitBasketSubmenuButtons(self, name, category, color):
        submenuButtons = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                submenuButtons.append(gameData)
        for index, gameData in enumerate(submenuButtons):
            (width, height) = (self.width/5, self.height/10)
            (x, y) = (self.width*(index+1)/(len(submenuButtons)+1)-width/2, self.height*10/20-height/2)
            button = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: self.openGame(data), callbackArgument=gameData,
                 text=gameData["Instructions Menu Button Text"], fontSize=50)
            self.buttonsList.add(button)
        # Add the Modes and Configuration options
        self.fruitBasketSelectedTimeLimitI = 1
        self.fruitBasketTimeLimitOptions = [("0.5", 30), ("1", 60), ("2", 120), ("5", 300), ("None", None)]
        self.fruitBasketMaxMissedFruitI = 1
        self.fruitBasketMaxMissedFruitOptions = [("1", 1), ("3", 3), ("5", 5), ("None", None)]
        fontHeight = self.height/30
        font = pygame.font.SysFont("Arial", fontHeight)
        margin = self.height/50
        x0 = self.width/4
        y0 = self.height*9/16
        categories = self.filterMenuItemsByKey("Menu Category")
        color = self.colorsList[categories.index(category)][0]
        selectedColor = self.colorsList[categories.index(category)][1]
        # Time Limit
        text = font.render("Time Limit (min)", 1, (0,0,0))
        self.surface.blit(text, (x0, y0))
        width, height = self.width/20, self.height/20
        timeLimitButtons = []
        i = 0
        for modeName, val in self.fruitBasketTimeLimitOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.fruitBasketSelectedTimeLimitI = i
                print i
                for i0 in xrange(len(timeLimitButtons)):
                    if i0 == i:
                        timeLimitButtons[i0].setColor(selectedColor)
                    else:
                        timeLimitButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.fruitBasketSelectedTimeLimitI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            timeLimitButtons.append(button)
        # Time Limit
        y0 += height*3/2
        text = font.render("Max Missed Fruit", 1, (0,0,0))
        self.surface.blit(text, (x0, y0))
        width, height = self.width/20, self.height/20
        maxMissedFruitsButtons = []
        i = 0
        for modeName, val in self.fruitBasketMaxMissedFruitOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.fruitBasketMaxMissedFruitI = i
                print i
                for i0 in xrange(len(maxMissedFruitsButtons)):
                    if i0 == i:
                        maxMissedFruitsButtons[i0].setColor(selectedColor)
                    else:
                        maxMissedFruitsButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.fruitBasketMaxMissedFruitI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            maxMissedFruitsButtons.append(button)

    def createFishGameSubmenuButtons(self, name, category, color):
        submenuButtons = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                submenuButtons.append(gameData)
        for index, gameData in enumerate(submenuButtons):
            (width, height) = (self.width/5, self.height/10)
            (x, y) = (self.width*(index+1)/(len(submenuButtons)+1)-width/2, self.height*11/20-height/2)
            button = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: self.openGame(data), callbackArgument=gameData,
                 text=gameData["Instructions Menu Button Text"], fontSize=50)
            self.buttonsList.add(button)
        # Add the Modes and Configuration options
        self.fishGameSelectedTimeLimitI = 1
        self.fishGameTimeLimitOptions = [("0.5", 30), ("1", 60), ("2", 120), ("5", 300), ("None", None)]
        fontHeight = self.height/30
        font = pygame.font.SysFont("Arial", fontHeight)
        margin = self.height/50
        x0 = self.width/4
        y0 = self.height*10/16
        categories = self.filterMenuItemsByKey("Menu Category")
        color = self.colorsList[categories.index(category)][0]
        selectedColor = self.colorsList[categories.index(category)][1]
        # Time Limit
        text = font.render("Time Limit (min)", 1, (0,0,0))
        self.surface.blit(text, (x0, y0))
        width, height = self.width/20, self.height/20
        timeLimitButtons = []
        i = 0
        for modeName, val in self.fishGameTimeLimitOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.fishGameSelectedTimeLimitI = i
                print i
                for i0 in xrange(len(timeLimitButtons)):
                    if i0 == i:
                        timeLimitButtons[i0].setColor(selectedColor)
                    else:
                        timeLimitButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.fishGameSelectedTimeLimitI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            timeLimitButtons.append(button)

    def createVolumeMeterSubmenuButtons(self, name, category, color):
        submenuButtons = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                submenuButtons.append(gameData)
        for index, gameData in enumerate(submenuButtons):
            (width, height) = (self.width/5, self.height/10)
            (x, y) = (self.width*(index+1)/(len(submenuButtons)+1)-width/2, self.height*11/20-height/2)
            button = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: self.openGame(data), callbackArgument=gameData,
                 text=gameData["Instructions Menu Button Text"], fontSize=50)
            self.buttonsList.add(button)
        # Add the Modes and Configuration options
        self.selectedTimeLimitI = 1
        self.timeLimitOptions = [("0.5", 30), ("1", 60), ("2", 120), ("3", 180), ("5", 300)] # Regular, 0-3 correspond to modes in PictureThatQuiz
        fontHeight = self.height/30
        font = pygame.font.SysFont("Arial", fontHeight)
        margin = self.height/50
        x0 = self.width/2
        y0 = self.height*10/16
        categories = self.filterMenuItemsByKey("Menu Category")
        color = self.colorsList[categories.index(category)][0]
        selectedColor = self.colorsList[categories.index(category)][1]
        # Time Limit
        text = font.render("Time Limit (min)", 1, (0,0,0))
        self.surface.blit(text, (x0, y0))
        width, height = self.width/40, self.height/20
        timeLimitButtons = []
        i = 0
        for modeName, val in self.timeLimitOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.selectedTimeLimitI = i
                print i
                for i0 in xrange(len(timeLimitButtons)):
                    if i0 == i:
                        timeLimitButtons[i0].setColor(selectedColor)
                    else:
                        timeLimitButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.selectedTimeLimitI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            timeLimitButtons.append(button)

    def createRoboticCarSubmenuButtons(self, name, category, color):
        self.isDisplayingPictureThatSubmenu = True
        submenuButtons = []
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                submenuButtons.append(gameData)
        for index, gameData in enumerate(submenuButtons):
            (width, height) = (self.width/5, self.height/10)
            (x, y) = (self.width/2*(index+1)/(len(submenuButtons)+1)-width/2, self.height*13/20-height/2)
            button = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: self.openGame(data), callbackArgument=gameData,
                 text=gameData["Instructions Menu Button Text"], fontSize=50)
            self.buttonsList.add(button)
        basePath = "games/img/PictureThatImages"
        folders, folderNames = self.getPictureThatFoldersAndWords(basePath)
        # Create FolderName Sprites
        foldersList = pygame.sprite.Group()
        x, y = 0, 0
        width, height = self.width*7/16, self.height/20
        # color0 = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        # colorDeviationMax, colorDeviationMin = 50, 25
        # color1 = (min(255, max(0, color0[0] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))),
        #           min(255, max(0, color0[1] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))),
        #           min(255, max(0, color0[2] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))))
        categories = self.filterMenuItemsByKey("Menu Category")
        color0 = self.colorsList[categories.index(category)][0]
        color1 = self.colorsList[categories.index(category)][1]
        i = 0
        maxCharInFolderName = 100
        for folder in folderNames:
            # TODO (amal): CHANGE THIS TO THE APPROPRIATE CALLBACK TO START THE GAME!!!
            def callback(data):
                print(data[1], "open game called!")
                self.isDisplayingPictureThatSubmenu = False
                self.openRoboticCarStopAndGoGame(data)
            foldersList.add(Word(x, y, color=(color0 if i%2 else color1),
                            deleteColor=(color1 if i%2 else color0), text=folder[:min(len(folder), maxCharInFolderName)]+" ("+str(len(folders[folder]))+")",
                            width=width, height=height, deleteCallback=callback,
                            deleteCallbackArgument=(gameData, folder, folders[folder]),
                            deleteButtonText="Start"))
            y += height
            i += 1
        folderScrollerX, folderScrollerY = self.width/2, self.height*66/128
        bottomMargin = self.height/20
        folderScroller = VerticalScroller(folderScrollerX, folderScrollerY, foldersList, width=width, height=self.height-folderScrollerY-bottomMargin, backgroundHeight=i*height)
        self.scrollerList.add(folderScroller)
        self.scrollerList.draw(self.surface)
        # Create Text On Top Of Folder scroller
        fontHeight = 50
        font = pygame.font.SysFont("Arial", fontHeight)
        text = font.render("Stop & Go", 1, (0,0,0))
        self.surface.blit(text, text.get_rect(centerx=folderScrollerX+width/2, y=folderScrollerY-fontHeight*5/4))


    def createPictureThatSubmenuButtons(self, name, category, color):
        self.isDisplayingPictureThatSubmenu = True
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name: break
        basePath = "games/img/PictureThatImages"
        folders, folderNames = self.getPictureThatFoldersAndWords(basePath)
        # print folders
        # raise Exception("")
        # TODO (amal): CHANGE THIS TO A SCROLLER, NOT A SET OF BUTTONS!  On one side have a scroller, on the other side have options like number of words, word first, picture first, both together, quiz or game
        # Add the Modes and Configuration options
        self.selectedGameModeI = 0
        self.gameModeOptions = [("Regular", None), ("Sign Quiz", 0), ("Spelling Quiz", 1), ("Speech Quiz", 2), ("Typing Quiz", 3)] # Regular, 0-3 correspond to modes in PictureThatQuiz
        self.selectedDisplayModeI = 0
        self.displayModeOptions = [("Word First", 0), ("Picture First", 1), ("Both", 2)]
        self.selectedNumTriesI = 0
        self.numTriesOptions = [("1", 1), ("3", 3), ("5", 5)]
        self.selectedNumWordsI = 1
        self.numWordsOptions = [("5", 5), ("10", 10), ("15", 15), ("All", None)]
        fontHeight = self.height/30
        font = pygame.font.SysFont("Arial", fontHeight)
        margin = self.height/50
        column1x0, column2x0 = self.width/10, self.width*13/40
        y0 = self.height*7/16
        categories = self.filterMenuItemsByKey("Menu Category")
        color = self.colorsList[categories.index(category)][0]
        selectedColor = self.colorsList[categories.index(category)][1]
        # Modes
        text = font.render("Mode", 1, (0,0,0))
        self.surface.blit(text, text.get_rect(centerx=column1x0, centery=y0))
        i = 0
        width, height = self.width/10, self.height/20
        column1ButtonWidth = width
        modeButtons = []
        for modeName, val in self.gameModeOptions:
            x, y = column1x0-width/2, y0+fontHeight+margin*(i+1)+height*i-height/2
            def callback(i):
                self.selectedGameModeI = i
                for i0 in xrange(len(modeButtons)):
                    if i0 == i:
                        modeButtons[i0].setColor(selectedColor)
                    else:
                        modeButtons[i0].setColor(color)
                if self.gameModeOptions[i][1] is None:
                    self.selectedDisplayModeI = 0
                elif self.gameModeOptions[i][1] == 0:
                    self.selectedDisplayModeI = 1
                elif self.gameModeOptions[i][1] == 1:
                    self.selectedDisplayModeI = 1
                elif self.gameModeOptions[i][1] == 2:
                    self.selectedDisplayModeI = 2
                elif self.gameModeOptions[i][1] == 3:
                    self.selectedDisplayModeI = 1
                for i0 in xrange(len(optionsButtons)):
                    if i0 == self.selectedDisplayModeI:
                        optionsButtons[i0].setColor(selectedColor)
                    else:
                        optionsButtons[i0].setColor(color)
                self.drawButtons()
            button = Button(x, y, color=selectedColor if i == self.selectedGameModeI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            modeButtons.append(button)
        # Options
        text = font.render("Options", 1, (0,0,0))
        self.surface.blit(text, text.get_rect(centerx=column2x0, centery=y0))
        i = 0
        width, height = self.width/10, self.height/20
        optionsButtons = []
        for modeName, val in self.displayModeOptions:
            x, y = column2x0-width/2, y0+fontHeight+margin*(i+1)+height*i-height/2
            def callback(i):
                self.selectedDisplayModeI = i
                for i0 in xrange(len(optionsButtons)):
                    if i0 == i:
                        optionsButtons[i0].setColor(selectedColor)
                    else:
                        optionsButtons[i0].setColor(color)
                self.drawButtons()
            button = Button(x, y, color=selectedColor if i == self.selectedDisplayModeI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            optionsButtons.append(button)
        # Number of Tries
        text = font.render("Num Tries", 1, (0,0,0))
        x0, y0 = column2x0-text.get_rect().width-margin/2, y0+fontHeight+margin*(i+1)+height*i-height/2
        self.surface.blit(text, (x0, y0))
        width, height = self.width/40, self.height/20
        numTriesButtons = []
        i = 0
        for modeName, val in self.numTriesOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.selectedNumTriesI = i
                print i
                for i0 in xrange(len(numTriesButtons)):
                    if i0 == i:
                        numTriesButtons[i0].setColor(selectedColor)
                    else:
                        numTriesButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.selectedNumTriesI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            numTriesButtons.append(button)
        # Number of Words
        text = font.render("Num Words", 1, (0,0,0))
        x0, y0 = column2x0-text.get_rect().width-margin/2, y0+fontHeight+margin*2
        self.surface.blit(text, (x0, y0))
        width, height = self.width/40, self.height/20
        numWordsButtons = []
        i = 0
        for modeName, val in self.numWordsOptions:
            x, y = x0+text.get_rect().width+margin*(i+1)+width*i, y0
            def callback(i):
                self.selectedNumWordsI = i
                print i
                for i0 in xrange(len(numWordsButtons)):
                    if i0 == i:
                        numWordsButtons[i0].setColor(selectedColor)
                    else:
                        numWordsButtons[i0].setColor(color)
                self.drawButtons()
                # pygame.display.update()
            button = Button(x, y, color=selectedColor if i == self.selectedNumWordsI else color, width=width, height=height,
                 callback=callback, callbackArgument=i,
                 text=modeName, fontSize=fontHeight*4/5)
            i += 1
            self.buttonsList.add(button)
            numWordsButtons.append(button)
        # Create FolderName Sprites
        foldersList = pygame.sprite.Group()
        x, y = 0, 0
        width, height = self.width*7/16, self.height/20
        # color0 = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        # colorDeviationMax, colorDeviationMin = 50, 25
        # color1 = (min(255, max(0, color0[0] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))),
        #           min(255, max(0, color0[1] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))),
        #           min(255, max(0, color0[2] + (random.randint(-1*colorDeviationMax,-1*colorDeviationMin) if random.randint(0, 1) else random.randint(colorDeviationMin,colorDeviationMax)))))
        color0 = color
        color1 = selectedColor
        i = 0
        maxCharInFolderName = 100
        for folder in folderNames:
            # TODO (amal): CHANGE THIS TO THE APPROPRIATE CALLBACK TO START THE GAME!!!
            def callback(data):
                print(data[1], "open game called!")
                self.isDisplayingPictureThatSubmenu = False
                self.openPictureGame(data)
            foldersList.add(Word(x, y, color=(color0 if i%2 else color1),
                            deleteColor=(color1 if i%2 else color0), text=folder[:min(len(folder), maxCharInFolderName)]+" ("+str(len(folders[folder]))+")",
                            width=width, height=height, deleteCallback=callback,
                            deleteCallbackArgument=(gameData, folder, folders[folder]),
                            deleteButtonText="Start"))
            y += height
            i += 1
        folderScrollerX, folderScrollerY = self.width-column1x0+column1ButtonWidth/2-width, self.height*7/16
        bottomMargin = self.height/20
        folderScroller = VerticalScroller(folderScrollerX, folderScrollerY, foldersList, width=width, height=self.height-folderScrollerY-bottomMargin, backgroundHeight=i*height)
        self.scrollerList.add(folderScroller)
        self.scrollerList.draw(self.surface)
        # self.scrollerList.draw(self.surface)
        # numberOfRows = 2 if len(folders) >= 4 else 1
        # maxButtonsPerRow = int(math.ceil(float(len(folders))/numberOfRows))
        # for rowNum in xrange(numberOfRows):
        #     numOfButtonsInRow = maxButtonsPerRow if (rowNum+1)*maxButtonsPerRow < len(folders) else len(folders)-rowNum*maxButtonsPerRow
        #     print rowNum, numOfButtonsInRow
        #     for buttonNum in xrange(numOfButtonsInRow):
        #         index = rowNum*maxButtonsPerRow+buttonNum
        #         (width, height) = (self.width/(numOfButtonsInRow+1), self.height/10)
        #         xMargin = (self.width-width*numOfButtonsInRow)/(numOfButtonsInRow+1)
        #         x = xMargin*(buttonNum+1)+width*buttonNum
        #         yMargin = 10
        #         y = self.height*10/20+(height+yMargin)*rowNum-height/2
        #         name = folderNames[index]
        #         button = Button(x, y, color=color, width=width, height=height,
        #          callback=lambda data: self.openPictureGame(data), callbackArgument=(gameData, folders[name]),
        #          text=name, fontSize=50)
        #         self.buttonsList.add(button)
        #         index += 1

    # returns dict: folderName -> list of ([word names], [picture paths]) tuples
    def getPictureThatFoldersAndWords(self, basePath):
        foldersAndWords = {}
        folderNames = []
        # Get the folders and words stored locally
        for path in os.listdir(basePath):
            path = basePath+"/"+path
            if os.path.isdir(path) and "SignBookCopy" not in path:
                folderName = os.path.basename(path)
                foldersAndWords[folderName] = []
                for root, subFolders, files in os.walk(path):
                    for fileName in files:
                        if "ds_store" in fileName.lower(): continue
                        foldersAndWords[folderName].append(([fileName[:fileName.find('.')]], [os.getcwd()+"/"+root+"/"+fileName]))
        folderNames.extend(foldersAndWords.keys())
        # Get the folders and words that were copied from SignBook
        signBookFoldersAndWords = self.getFolderAndWordsFromSignBookCopy()
        uniqueNewFolderNames = []
        for folder, words in signBookFoldersAndWords.iteritems():
            # exists = False
            # for folderName in foldersAndWords:
            #     if folder.strip().lower() == folderName.strip().lower():
            #         foldersAndWords[folderName].extend(words)
            #         exists = True
            #         break
            # if not exists:
            if folder in foldersAndWords:
                foldersAndWords[folder].extend(words)
            else:
                foldersAndWords[folder] = words
                uniqueNewFolderNames.append(folder)
        folderNames.extend(uniqueNewFolderNames)
        # Add an All Words Option
        foldersAndWords["All Words"] = [x for key, val in foldersAndWords.iteritems() for x in val]
        folderNames.insert(0, "All Words")
        return foldersAndWords, folderNames

    def getFolderAndWordsFromSignBookCopy(self):
        try:
            with open(self.speakUpRelativePathToObjs+'/entries.json', 'rt') as doc:
                entriesRaw = json.loads(doc.read())
            with open(self.speakUpRelativePathToObjs+'/folders.json', 'rt') as doc:
                foldersRaw = json.loads(doc.read())
            parents = set()
            accumulatedFolders = {}
            self.folderTraversalHelper(foldersRaw, entriesRaw, parents, accumulatedFolders)
            # remove folders with 0 words
            folderNames = accumulatedFolders.keys()
            for folderName in folderNames:
                if len(accumulatedFolders[folderName]) == 0:
                    del accumulatedFolders[folderName]
            return accumulatedFolders
        except:
            return {}

    def folderTraversalHelper(self, foldersRaw, entriesRaw, parents, accumulatedFolders):
        attributes = foldersRaw.keys()
        if "children" in attributes: # node
            name = foldersRaw["name"].encode('ascii', 'ignore')
            if name != "Folders": parents.add(name) # discard root of tree
            if name not in accumulatedFolders:
                accumulatedFolders[name] = []
            for child in foldersRaw["children"]:
                self.folderTraversalHelper(child, entriesRaw, parents, accumulatedFolders)
            if name != "Folders": parents.discard(name)
        else: # leaf
            imageKey = foldersRaw["id"][0:len(foldersRaw["id"])-3] if "0v" in foldersRaw["id"] else foldersRaw["id"] # remove the "ov2" at the end of the ID
            # Ensure that the word has both a name and an image
            if (imageKey in entriesRaw and
                "english" in entriesRaw[imageKey] and len(entriesRaw[imageKey]["english"]) > 0 and
                "images" in entriesRaw[imageKey] and len(entriesRaw[imageKey]["images"]) > 0):
                imageNames = [s.encode('ascii', 'ignore') for s in entriesRaw[imageKey]["english"]]
                imagePaths = [os.getcwd()+"/"+self.speakUpRelativePathToImgs+"/"+s.encode('ascii', 'ignore') for s in entriesRaw[imageKey]["images"]]
                # add a tuple of (image captions list, paths list) to every ancestor
                for folderName in parents:
                    accumulatedFolders[folderName].append((imageNames, imagePaths))

    # def createRickshawQuizSubmenuButtons(self, name, category, imgPath, color):
    #     for gameData in self.menuItems:
    #         if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name: break
    #     # check = Checkbox(200, 50, text="Checkbox")
    #     # self.buttonsList.add(check)
    #     print "Creating Words"
    #     columns = 3
    #     wordsPerColumn = 10
    #     wordsPerPage = wordsPerColumn*columns
    #     maxCharPerWord = 20
    #     quizWords = self.readQuizWords('games/quizWords.txt')
    #     selectedWords = [x for x in quizWords]
    #     pages = len(quizWords)/wordsPerPage
    #     pageW = self.width*2/3
    #     pageH = self.height*2/3
    #     pageX0 = self.width/6
    #     pageY0 = self.height*45/96
    #     ratioOfCheckBoxHeightToWhiteSpaceVert = 0.5
    #     whiteSpaceHoriz = 10
    #     currPage = 0
    #     def updateDrawing():
    #         # print "HI", selectedWords
    #         self.drawButtons()
    #         pygame.display.update()
    #     for i in xrange(currPage*wordsPerPage, (currPage+1)*wordsPerPage):
    #         currColumn = (i % wordsPerPage)/wordsPerColumn
    #         word = quizWords[i]
    #         width = (pageW/columns)-whiteSpaceHoriz
    #         height = pageH/((1+ratioOfCheckBoxHeightToWhiteSpaceVert)*wordsPerColumn-ratioOfCheckBoxHeightToWhiteSpaceVert)*(1/(1+ratioOfCheckBoxHeightToWhiteSpaceVert))
    #         x = pageX0 + currColumn*pageW/columns
    #         y = pageY0+(i % wordsPerColumn)*int(pageH/((1+ratioOfCheckBoxHeightToWhiteSpaceVert)*wordsPerColumn-ratioOfCheckBoxHeightToWhiteSpaceVert))
    #         # print "ASDF", word, i, currColumn, width, height, x, y
    #         check = Checkbox(x, y, width=width, height=height, text=word, updateDrawing=updateDrawing, isChecked=True, fontSize=30,
    #                      callback=lambda isChecked, word: selectedWords.remove(word) if isChecked else selectedWords.append(word), callbackArgument=word, checkmarkPath='games/tools/checkmark.png')
    #         self.buttonsList.add(check)

        # basePath = "games/img/PictureThatImages"
        # buttons = [{"Name":"All", "Path":basePath}]
        # for path in os.listdir("games/img/PictureThatImages"):
        #     print path, os.path.isdir(path)
        #     if os.path.isdir(basePath+"/"+path):
        #         buttons.append({"Name":os.path.basename(path), "Path":basePath+"/"+path})
        # print buttons
        # numberOfRows = 2 if len(buttons) >= 4 else 1
        # maxButtonsPerRow = int(math.ceil(float(len(buttons))/numberOfRows))
        # for rowNum in xrange(numberOfRows):
        #     numOfButtonsInRow = maxButtonsPerRow if (rowNum+1)*maxButtonsPerRow < len(buttons) else len(buttons)-rowNum*maxButtonsPerRow
        #     print rowNum, numOfButtonsInRow
        #     for buttonNum in xrange(numOfButtonsInRow):
        #         index = rowNum*maxButtonsPerRow+buttonNum
        #         (width, height) = (self.width/(numOfButtonsInRow+1), self.height/10)
        #         xMargin = (self.width-width*numOfButtonsInRow)/(numOfButtonsInRow+1)
        #         x = xMargin*(buttonNum+1)+width*buttonNum
        #         yMargin = 10
        #         y = self.height*10/20+(height+yMargin)*rowNum-height/2
        #         path = buttons[index]["Path"]
        #         name = buttons[index]["Name"]
        #         button = Button(x, y, color=color, width=width, height=height,
        #          callback=lambda data: self.openPictureGame(data), callbackArgument=(gameData, path),
        #          text=name, fontSize=50)
        #         self.buttonsList.add(button)
        #         index += 1

    def drawSubmenuEllies(self, name, category):
        categories = self.filterMenuItemsByKey("Menu Category")
        imgPath = self.submenuEllies[categories.index(category)]
        ellie = pygame.image.load(imgPath)
        if random.randint(0,1): ellie = pygame.transform.flip(ellie, True, False)
        (ellieW, ellieH) = ellie.get_rect().size
        if name == "Picture That!" or name == "Robotic Car":
            height = self.height/6
            centerx = self.width/4
        else:
            height = self.height/3
            centerx=self.width/2
        width = height*ellieW/ellieH
        ellie = pygame.transform.scale(ellie, (width, height))
        self.surface.blit(ellie, ellie.get_rect(centerx=centerx, y=self.height-height))

    def drawInstructions(self, name, category):
        """Draws game instructions in the submenu"""
        instructions = ""
        for gameData in self.menuItems:
            if gameData["Menu Category"] == category and gameData["Menu Button Text"] == name:
                instructions = gameData["Instructions"]
                break
        fontHeight = self.width/30
        charPerLine = fontHeight
        instructions = self.splitText(instructions, charPerLine)
        font = pygame.font.SysFont("Arial", fontHeight)
        y0 = self.height*10/32
        for index, line in enumerate(instructions.splitlines()):
            text = font.render(line, 1, (0,0,0))
            textpos = text.get_rect()
            textpos.centerx = self.width/2
            textpos.y = y0 + index*fontHeight
            print textpos.y
            self.surface.blit(text, textpos)

    def splitText(self, text, charPerLine):
        """Splits instructions into lines of a given length"""
        for charI in xrange(1,len(text)/charPerLine+1):
            charI = charI*charPerLine + charI - 1
            if "\n" in text[text.find("\n", charI-charPerLine)+1:charI]:
                continue
            splitI = text.find(" ", charI)
            text = text[:splitI+1] + "\n" + text[splitI+1:]
        return text.strip()

    def drawSubmenuBackground(self, category=None):
        """Draws the submenu background"""
        self.surface.fill((0xFF, 0xFF, 0xFF))

    def createBackButton(self, category=None):
        """Creates the bak button in the submenu"""
        margin = 20
        (width, height) = (self.width/8, self.height/20)
        (x,y) = (margin, margin+self.height/5)
        back = Button(x, y, color=self.quitButtonColor, width=width, height=height,
                 callback=lambda data: self.drawMenu(), text="Back", fontSize=self.height/30)
        self.buttonsList.add(back)

    ############################################################################
    # Button Callback Functions
    ############################################################################

    def openGame(self, gameData):
        """Opens any game but Picture That!  Uses information from the menuItems.txt
        file to determine what to improt nd what class to open."""
        # Load the game properties
        (gameName, importFile, gameClass) = (gameData["Name"], gameData["Import File"], gameData["Game Class"])
        self.textLog += "\nOpened %s" %(gameName)
        self.log()
        # Load the volume threshhold
        try:
            with open('BackgroundNoise.txt', 'rt') as doc:
                volumeThreshhold = float(doc.read())
        except IOError:
            volumeThreshhold = 50
        print volumeThreshhold
        # Load Min Expected Volume
        try:
            with open('MinExpectedNoise.txt', 'rt') as doc:
                minExpected = float(doc.read())
        except IOError:
            minExpected = 60
        print minExpected
        # Load Max Expected Volume
        try:
            with open('MaxExpectedNoise.txt', 'rt') as doc:
                maxExpected = float(doc.read())
        except IOError:
            maxExpected = 80
        print maxExpected
        if "Spaceships" in gameName: volumeThreshhold = minExpected
        if "Robotic Car" in gameName: volumeThreshhold = minExpected*3/4+maxExpected/4
        if "Fish Game" in gameName: volumeThreshhold = minExpected*3/4+maxExpected/4#volumeThreshhold/4+minExpected*3/4
        os.chdir('games/')
        # Check that you have not already improted the game
        if gameName not in self.importedGames:
            self.importedGames[gameName] = importlib.import_module(importFile)

        def callback():
            os.chdir('../')
            self.drawMenu()
        if gameClass == "VolumeSpeedometerGame":
            game = eval("self.importedGames[gameName]."+gameClass+"(callback, self.currentW, self.currentH, volumeThreshhold, minExpected, maxExpected, timeLimit=self.timeLimitOptions[self.selectedTimeLimitI][1])")
        elif gameClass == "FishGameWithArrows" or gameClass == "FishGame":
            game = eval("self.importedGames[gameName]."+gameClass+"(callback, self.currentW, self.currentH, volumeThreshhold, minExpected, maxExpected, timeLimit=self.fishGameTimeLimitOptions[self.fishGameSelectedTimeLimitI][1])")
        elif gameClass == "FruitBasketWithArrows" or gameClass == "FruitBasket":
            game = eval("self.importedGames[gameName]."+gameClass+"(callback, self.currentW, self.currentH, volumeThreshhold, minExpected, maxExpected, maxMissedFruits=self.fruitBasketMaxMissedFruitOptions[self.fruitBasketMaxMissedFruitI][1], timeLimit=self.fruitBasketTimeLimitOptions[self.fruitBasketSelectedTimeLimitI][1])")
        else:
            game = eval("self.importedGames[gameName]."+gameClass+"(callback, self.currentW, self.currentH, volumeThreshhold, minExpected, maxExpected)")
        game.run()

    def openRoboticCarStopAndGoGame(self, data):
        """Opens Robotic Car Stop and Go Game. It needs a separate function to open it
        because it needs an extra parameter, a dictionary of pcitures to display."""
        (gameData, folderName, wordsList) = data
        # limit the words list based on the number of words selected
        self.textLog += "\nIn Robotic Car Stop and Go"
        self.log()
        random.shuffle(wordsList)
        # Load the game properties
        # Hardcoded the values for the game, because the menu framework is currently not flexible enough to account for both
        (gameName, importFile, gameClass) = ("Robotic Car Stop And Go", "games.roboticCarStopAndGoGame", "RoboticCarStopAndGoGame")
        # Load the volume threshhold
        try:
            with open('BackgroundNoise.txt', 'rt') as doc:
                volumeThreshhold = float(doc.read())
        except IOError:
            volumeThreshhold = 60
        print volumeThreshhold
        # Load Min Expected Volume
        try:
            with open('MinExpectedNoise.txt', 'rt') as doc:
                minExpected = float(doc.read())
        except IOError:
            minExpected = 60
        print minExpected
        # Load Max Expected Volume
        try:
            with open('MaxExpectedNoise.txt', 'rt') as doc:
                maxExpected = float(doc.read())
        except IOError:
            maxExpected = 80
        print maxExpected
        # Check that you have not already improted the game
        self.textLog += "\nOpened %s with folderName %s" % (gameName, folderName)
        self.log()
        if gameName not in self.importedGames:
            self.importedGames[gameName] = importlib.import_module(importFile)
        os.chdir('games/')
        def callback():
            os.chdir('../')
            self.drawMenu()
        game = eval("self.importedGames[gameName]."+gameClass+"(callback, self.currentW, self.currentH, volumeThreshhold, minExpected, maxExpected, wordsList)")
        game.run()

    def openPictureGame(self, data):
        """Opens Picture That!  Picture That needs a separate function to open it
        because it needs an extra parameter, a dictionary of pcitures to display."""
        (gameData, folderName, wordsList) = data
        # limit the words list based on the number of words selected
        self.textLog += "\nIn Picture That, numWords = "+str(self.numWordsOptions[self.selectedNumWordsI][1])
        self.log()
        if self.numWordsOptions[self.selectedNumWordsI][1] is not None and self.numWordsOptions[self.selectedNumWordsI][1] < len(wordsList): # not All
            newWordsList = []
            indicesToExclude = set()
            while len(newWordsList) < self.numWordsOptions[self.selectedNumWordsI][1]:
                i0 = random.randint(0, len(wordsList)-1)
                if i0 not in indicesToExclude:
                    newWordsList.append(wordsList[i0])
                    indicesToExclude.add(i0)
            wordsList = newWordsList
        # Load the game properties
        # Hardcoded the values for the two picture that versions, because the menu framework is currently not flexible enough to account for both
        (gameName0, importFile0, gameClass0) = ("Picture That!", "games.pictureThat", "PictureThat")
        (gameName1, importFile1, gameClass1) = ("Picture That Quiz!", "games.pictureThatQuiz", "PictureThatQuiz")
        # Load the volume threshhold
        try:
            with open('BackgroundNoise.txt', 'rt') as doc:
                volumeThreshhold = float(doc.read())
        except IOError:
            volumeThreshhold = 60
        print volumeThreshhold
        # Check that you have not already improted the game
        if self.gameModeOptions[self.selectedGameModeI][1] is None: # Regular Game
            self.textLog += "\nOpened %s with folderName %s" % (gameName0, folderName)
            self.log()
            if gameName0 not in self.importedGames:
                self.importedGames[gameName0] = importlib.import_module(importFile0)
            os.chdir('games/')
            def callback():
                os.chdir('../')
                self.drawMenu()
            game = eval("self.importedGames[gameName0]."+gameClass0+"(callback, self.currentW, self.currentH, volumeThreshhold, wordsList, self.displayModeOptions[self.selectedDisplayModeI][1])")
            game.run()
        else: # Quiz Game
            self.textLog += "\nOpened %s with folderName %s, displayMode %d, gameMode %d, numTries %d" % (gameName1, folderName, self.displayModeOptions[self.selectedDisplayModeI][1], self.gameModeOptions[self.selectedGameModeI][1], self.numTriesOptions[self.selectedNumTriesI][1])
            self.log()
            if gameName1 not in self.importedGames:
                self.importedGames[gameName1] = importlib.import_module(importFile1)
            os.chdir('games/')
            def callback():
                os.chdir('../')
                self.drawMenu()
            game = eval("self.importedGames[gameName1]."+gameClass1+"(callback, self.currentW, self.currentH, volumeThreshhold, wordsList, self.displayModeOptions[self.selectedDisplayModeI][1], self.gameModeOptions[self.selectedGameModeI][1], self.numTriesOptions[self.selectedNumTriesI][1])")
            game.run()

    # def getPictureDictionary(self, path):
    #     """Gets a dictionary of picture names and paths for a given directory,
    #     to be passed into Picture That!"""
    #     pictureDictionary = {}
    #     for root, subFolders, files in os.walk(path):
    #         print root, subFolders, files
    #         for filePath in files:
    #             (fileName, fileExtension) = os.path.splitext(filePath)
    #             acceptableImageFormats = ['.png', '.jpg', '.gif']
    #             if fileExtension.lower() in acceptableImageFormats:
    #                 removePathComponent='games/'
    #                 basePath=root.replace(removePathComponent, "")
    #                 pictureDictionary[fileName] = basePath+"/"+filePath
    #     print pictureDictionary
    #     return pictureDictionary

    def calibrate(self):
        """Loads and opens the calibrate menu"""
        # Logs that the user calibrated text
        self.textLog += "\nClicked Remove Background Noise"
        self.log()
        # Check that you have not already improted the calibrate file
        if "Calibrate" not in self.importedGames:
            self.importedGames["Calibrate"] = importlib.import_module('calibrate')

        def callback():
            self.drawMenu()

        game = self.importedGames["Calibrate"].CalibrateMenu(callback, self.currentW, self.currentH)
        game.run()

    def licenceInfo(self):
        """Opens a menu with the MIT OpenSource License Info"""
        self.buttonsList = pygame.sprite.Group()
        self.drawMenuBackground()
        self.drawMenuTitle()
        margin = 20
        self.createBackButton()
        # Load the license text
        try:
            with open('licenseInfo.txt', 'rt') as doc:
                licenseText = doc.read()
        except IOError:
            licenseText = "Sorry, we cannot find the license agreement \
information.  If you would like to access the \
information, please email info@techbridgeworld.org"
        print licenseText
        fontHeight = self.width/50
        charPerLine = fontHeight
        instructions = self.splitText(licenseText, charPerLine)
        font = pygame.font.SysFont("Arial", fontHeight)
        y0 = self.height*9/32
        for index, line in enumerate(licenseText.splitlines()):
            text = font.render(line, 1, (0,0,0))
            textpos = text.get_rect()
            textpos.centerx = self.width/2
            textpos.y = y0 + index*fontHeight
            print textpos.y
            self.surface.blit(text, textpos)
        self.drawButtons()
        # Display the About Screen
        pygame.display.update()


    ############################################################################
    # Runtime + Logging Functions
    ############################################################################

    def log(self):
        """Logs information in the speakUpUsageLog.txt file"""
        with open("speakUpUsageLog.txt", "a") as logFile:
            logFile.write(self.textLog)
            self.textLog = ""

    def quit(self):
        """Quits the game"""
        # Logs that the user quit
        self.textLog += "\nQuit"
        self.log()
        pygame.quit()
        sys.exit()

    def run(self):
        """Runs the main game loop"""
        while True:
            events = pygame.event.get()
            if self.shouldProcessEvents:
                if self.isDisplayingPictureThatSubmenu:
                    # print "Drawing Scroller in Run"
                    self.scrollerList.update(event)
                    self.scrollerList.draw(self.surface)
                for event in events:
                    if event.type == MOUSEBUTTONDOWN:
                        print event.pos
                        self.buttonsList.update(*event.pos)
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            self.drawMenu(fullscreen=False)
                    if event.type == QUIT:
                        self.quit()
                pygame.display.update()
            else:
                self.framesSinceSetshouldProcessEventsToFalse += 1
                if self.framesSinceSetshouldProcessEventsToFalse >= self.startProcessingEventsAfter:
                    self.shouldProcessEvents = True
            self.clock.tick(self.fps)

menu = Menu()
menu.run()
