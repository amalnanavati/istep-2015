import audioop, pyaudio, numpy, math, wave, struct, analyse

class Listener(object):
    def __init__(self, timePerBlock=0.1):
        self.pa = pyaudio.PyAudio()
        #The frames per second of audio
        self.rate = int(self.pa.get_default_input_device_info()['defaultSampleRate'])
        #The length (seconds) per block of audio 
        self.timePerBlock = timePerBlock
        #The number of frames per block of audio
        self.framesPerBlock = int(self.rate*self.timePerBlock)
        self.errorCount = 0
        self.volumeBuffer = []
        self.maxBufferSize = 100
        self.initialSpikeLevel = 500
        self.spikeLevel = self.initialSpikeLevel
        self.numberOfPastSampleToCompare = 2
        self.isVocalizing = False

    def startStream(self):
        self.stream = self.pa.open(format=pyaudio.paInt16,
                                   channels=1,
                                   rate=self.rate,
                                   input=True,
                                   frames_per_buffer=self.framesPerBlock)
        self.stream.start_stream()

    def listen(self):
        try:
            data = self.stream.read(self.framesPerBlock)
            rms = audioop.rms(data, 2)
            volume = 20*math.log(rms, 10) if rms > 0 else 0
            # volume = audioop.rms(data, 2)
            # volume = self.calcVol(data)
            frequency = self.calcFreq(data)
            # self.detectVoice(volume)
            # print self.isVocalizing, volume
            print "a", (volume, frequency)
            return (volume, frequency)
            # normalFreq = self.normalizedFreq(volume, frequency)
            # print "n", (volume, normalFreq)
            # return (volume, normalFreq)
        except IOError, e:
            self.errorCount += 1
            print "%d Error: %s" %(self.errorCount, e)
            return (0,None)

    def detectVoice(self, volume):
        self.volumeBuffer.append(volume)
        while len(self.volumeBuffer) > self.maxBufferSize:
            self.volumeBuffer.pop(0)
        if len(self.volumeBuffer) > self.numberOfPastSampleToCompare+1:
            for i in xrange(-2, -3-self.numberOfPastSampleToCompare, -1):
                # print i, self.volumeBuffer
                if volume - self.volumeBuffer[i] > self.spikeLevel:
                    self.spikeLevel = (volume - self.volumeBuffer[i])/2
                    self.isVocalizing = True
                elif volume - self.volumeBuffer[i] < -1*self.spikeLevel:
                    self.spikeLevel = self.initialSpikeLevel
                    self.isVocalizing = False

    def calcFreq(self, data):
        dataArray = numpy.fromstring(data, 'Int16')
        freq = analyse.musical_detect_pitch(dataArray)
        if freq <= 45: freq = None # A hacky way to overcome the fact that Sound Analyze sometimes picks up a pitch of 41 even when there is no vocalization.  41 is lower than most humans can make (for example Amal's range is 45-65.)
        return freq

    def freqBufferAppend(self, frequency):
        self.freqBuffer.append(frequency)
        self.backgroundBlocksCount = 0
        while len(self.freqBuffer) > self.freqBufferSize:
            self.freqBuffer.pop(0)

    def closeStream(self):
        self.stream.close()
        self.pa.terminate()

# listener = Listener()
# listener.startStream()
# for i in xrange(1000): listener.listen()
# listener.closeStream()

# import pylab
# listener = Listener()
# listener.startStream()
# volumes = []
# numberOfSamples = 100
# for i in xrange(numberOfSamples): 
#     (volume, freq) = listener.listen()
#     volumes.append(volume)
# listener.closeStream()
# print sum(volumes)/len(volumes)
# pylab.plot(range(len(volumes)), volumes)
# pylab.xlabel("Sample Number")
# pylab.ylabel("Volume")
# pylab.show()
