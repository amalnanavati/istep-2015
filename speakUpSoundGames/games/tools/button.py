import pygame, os

class Button(pygame.sprite.Sprite):
    """A pygame Sprite that functions like a button"""

    def __init__(self, x, y, color=(0xFF, 0xCC, 0x00),
                 callback=lambda x: Button.defaultCallback(), text="Button",
                 width=100, height=20, callbackArgument={}, imgPath=None, fontSize=30):
        """Initializes the button"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Set the font
        self.fontSize = fontSize
        self.font = pygame.font.SysFont("Arial", fontSize)
        # Load the surface
        self.width, self.height = width, height
        self.image = pygame.Surface((width, height))
        self.color = color
        self.image.fill(color)
        self.imgPath = imgPath
        # Load the image, if it exists
        if imgPath != None:
            image = pygame.image.load(imgPath)
            # Scale the image
            (imageW,imageH) = image.get_rect().size
            margin = fontSize
            if float(imageW)/imageH >= float(width)/height:
                imageWidth = width-2*margin
                imageHeight = imageWidth*imageH/imageW
            else:
                imageHeight = height-2*margin
                imageWidth = imageHeight*imageW/imageH
            print margin, imageW, imageH, width, height, imageWidth, imageHeight
            image = pygame.transform.scale(image, (imageWidth, imageHeight))
            self.image.blit(image, image.get_rect(centerx=width/2, y=(height-imageHeight)/2))
        # Load text
        self.textWord = text
        self.text = self.font.render(text, 1, (0x00,0x00,0x00))
        textpos = self.text.get_rect()
        textpos.centerx = self.image.get_rect().centerx
        if imgPath == None: textpos.centery = self.image.get_rect().centery
        else: textpos.centery = (3*height+imageHeight)/4
        self.image.blit(self.text, textpos)
        # Set the sprit's attributes
        self.rect = pygame.Rect(x, y, width, height)
        # Set the callback
        self.callback = callback
        # Sets the game data
        self.callbackArgument = callbackArgument

    def update(self, x0, y0, *args):
        """Called every mouse click"""
        (x, y, w, h) = (self.rect.x, self.rect.y, self.rect.width, self.rect.height)
        print x0,y0,x,y,w,h
        if x0 > x and x0 < x + w and y0 > y and y0 < y + h:
            print "Button Clicked!"
            self.callback(self.callbackArgument)

    def setColor(self, color):
        # Load the surface
        self.image.fill(color)
        # Load the image, if it exists
        if self.imgPath != None:
            image = pygame.image.load(self.imgPath)
            # Scale the image
            (imageW,imageH) = image.get_rect().size
            margin = self.fontSize
            if float(imageW)/imageH >= float(self.width)/self.height:
                imageWidth = self.width-2*margin
                imageHeight = imageWidth*imageH/imageW
            else:
                imageHeight = self.height-2*margin
                imageWidth = imageHeight*imageW/imageH
            print margin, imageW, imageH, width, height, imageWidth, imageHeight
            image = pygame.transform.scale(image, (imageWidth, imageHeight))
            self.image.blit(image, image.get_rect(centerx=self.width/2, y=(self.height-imageHeight)/2))
        # Load text
        self.text = self.font.render(self.textWord, 1, (0x00,0x00,0x00))
        textpos = self.text.get_rect()
        textpos.centerx = self.image.get_rect().centerx
        if self.imgPath == None: textpos.centery = self.image.get_rect().centery
        else: textpos.centery = (3*self.height+imageHeight)/4
        self.image.blit(self.text, textpos)

    def setText(self, text):
        # Load the surface
        self.image.fill(self.color)
        # Load the image, if it exists
        if self.imgPath != None:
            image = pygame.image.load(self.imgPath)
            # Scale the image
            (imageW,imageH) = image.get_rect().size
            margin = self.fontSize
            if float(imageW)/imageH >= float(self.width)/self.height:
                imageWidth = self.width-2*margin
                imageHeight = imageWidth*imageH/imageW
            else:
                imageHeight = self.height-2*margin
                imageWidth = imageHeight*imageW/imageH
            print margin, imageW, imageH, width, height, imageWidth, imageHeight
            image = pygame.transform.scale(image, (imageWidth, imageHeight))
            self.image.blit(image, image.get_rect(centerx=self.width/2, y=(self.height-imageHeight)/2))
        # Load text
        self.text = self.font.render(text, 1, (0x00,0x00,0x00))
        textpos = self.text.get_rect()
        textpos.centerx = self.image.get_rect().centerx
        if self.imgPath == None: textpos.centery = self.image.get_rect().centery
        else: textpos.centery = (3*self.height+imageHeight)/4
        self.image.blit(self.text, textpos)

    def setCenterPos(self, centerx, centery):
        self.rect = pygame.Rect(centerx-self.rect.width/2, centery-self.rect.height/2, self.rect.width, self.rect.height)

    def setPos(self, x, y):
        self.rect = pygame.Rect(x, y, self.rect.width, self.rect.height)

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

    @staticmethod
    def defaultCallback():
        print "Button Clicked!"
