################################################################################
# Class to create Bezier Curve from a series of points
# Authors: 
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import sys, Tkinter, math

class Vector(object):
    """A class to add vectors and multiply by scalars"""
    def __init__(self, coordinates=(0,0)):
        """Creates a vector with the given coordinates"""
        self.coordinates = coordinates

    def __repr__(self):
        return str(self.convertToTuple())

    def add(vec1, vec2):
        """Adds two vectors entrywise"""
        (c1, c2) = (vec1.coordinates, vec2.coordinates)
        if len(c1) != len(c2):
            raise Exception("Vectors must be the same length")
        resultCoordinates = tuple(c1[i]+c2[i] for i in xrange(len(c1)))
        return Vector(resultCoordinates)

    def subtract(vec1, vec2):
        """Subtract vector 2 from vector 1 entrywise"""
        (c1, c2) = (vec1.coordinates, vec2.coordinates)
        if len(c1) != len(c2):
            raise Exception("Vectors must be the same length")
        resultCoordinates = tuple(c1[i]-c2[i] for i in xrange(len(c1)))
        return Vector(resultCoordinates)

    def mult(vec1, scalar):
        """Multiplies every coordinate in a vector by the scalar"""
        c = vec1.coordinates
        resultCoordinates = tuple(c[i]*scalar for i in xrange(len(c)))
        return Vector(resultCoordinates)

    def normalizedVector(vec):
        length = vec.vectorLength()
        c = vec.coordinates
        normalizedCoordinates = tuple(c[i]/length for i in xrange(len(c)))
        return Vector(normalizedCoordinates)

    def vectorLength(vec):
        sumOfSquares = 0
        for i in xrange(len(vec.coordinates)): 
            sumOfSquares += vec.coordinates[i]**2
        length = sumOfSquares**0.5
        return length

    def convertToTuple(vec):
        """Converts a vector to a tuple"""
        return vec.coordinates

class BezierCurve(object):
    """A class to convert between bezier curve control points (based on how
        Adobe Photoshop renders bezier curves) to rectangular coordinates."""

    ############################################################################
    # Public Functions
    # To be used by games as they interact with the class
    ############################################################################

    # NOTE: Each control point will be a 3-tuple, of the form 
    # ((x,y), (dx,dy), r).  (x,y) is the location of the control point in 
    # rectangular coordinates, (dx,dy) is the direction (the tangent at that 
    # point,) and r (radius) is effectively the "strength" of that control point,
    # proportional to the curvature of the curve.  However, this is merely a 
    # simplified version of control points (based on Adobe Photoshop's Pen 
    # tool,) that we implemented to ensure smoother curver.  The control points 
    # used in the bezier curve formula are (x,y), and two points r units away 
    # in the +/- (dx, dy) direction.  The conversion between these two notions
    # of control points is done in the 
    # convertControlPointThreeTupleToThreeVectors() method.

    def __init__(self, controlPoints=[]):
        """Initializes a Bezier Curve with the given """
        self.setInitialControlPoints(controlPoints)

    def setInitialControlPoints(self, controlPoints):
        if len(controlPoints) != 0 and len(controlPoints) < 2: 
            raise Exception("Need at least 2 control points for a cubic bezier curve")
        # Convert the coordinates in the control point to vectors
        self.controlPoints = []
        for point in controlPoints:
            vecPoint = (Vector(point[0]), Vector(point[1]), point[2])
            self.controlPoints.append(vecPoint)

    # def __str__(self):
    #     string = "["
    #     for point in self.controlPoints:
    #         string += str(point.convertToTuple())+","
    #     string = string[0:-1]+"]"
    #     return string

    def addControlPoints(self, controlPoints):
        if len(self.controlPoints) == 0:
            self.setInitialControlPoints(controlPoints)
        else:
            for point in controlPoints: 
                print point
                vecPoint = (Vector(point[0]), Vector(point[1]), point[2])
                self.controlPoints.append(vecPoint)
        # if len(points) == 0: return
        # # If there are no control points
        # if len(self.controlPoints) == 0:
        #     if (len(points)-1)%3 != 0: 
        #         raise Exception("Need 3x+4 control points for a cubic bezier curve")
        #     # Convert all control points to vectors
        #     self.controlPoints = []
        #     for point in self.controlPoints:
        #         self.controlPoints.append(Vector(point))
        # # If there are already some control points
        # if (len(points)+1)%3 != 0: raise Exception("Must add 2+3x points")
        # # To make the curve continuous, we add a point at the end so the tangent
        # # line matches.  (Like Adobe Photoshop's Pen tool.)
        # pivot = self.controlPoints[-1]
        # point = self.controlPoints[-2]
        # mirror = Vector.add(pivot, Vector.subtract(pivot, point))
        # self.controlPoints.append(mirror)
        # for point in points:
        #     self.controlPoints.append(Vector(point))

    def removeControlPoint(self, point):
        if point in self.controlPoints: self.controlPoints.remove(point)

    def shiftPathOverBy(self, dPoint):
        dPoint = Vector(dPoint)
        for i in xrange(len(self.controlPoints)):
            point = self.controlPoints[i]
            point = (Vector.add(point[0],dPoint), point[1], point[2])
            self.controlPoints[i] = point

    def getApproxYForX(self, x):
        (p1, p2) = self.findX(x)
        # print "p", p1, p2
        slope = (p2[1]-p1[1])/(p2[0]-p1[0])
        y = p1[1]+slope*(x-p1[0])
        return y

    def getApproxSlopeForX(self, x):
        (p1, p2) = self.findX(x)
        # print "p", p1, p2
        slope = (p2[1]-p1[1])/(p2[0]-p1[0])
        return slope

    def iteritems(self):
        for i in xrange(len(self.controlPoints)): 
            yield (self.controlPoints[i], i)

    def getBezierCurveXYCoordinates(self, intervals=20, isInt=True):
        """Takes in the control points passed to the function and converts it to """
        controlPoints = self.getIntermediateControlPoints()
        # Intervals refers to the number of intervals per cubit bezier curve.  
        # There are ((number of points)+1)/4 curves in a set of control points.
        curvePoints=[]
        # Iterate over control points 4 at a time, moving by 3
        for points in self.iterateOverControlPoints(controlPoints):
            formula = self.bezierCurveFormula(points)
            for t in xrange(intervals+1):
                t = float(t)/intervals
                curvePoints.append(formula(t))
        # Convert vectors back to tuples
        for i in xrange(len(curvePoints)):
            curvePoints[i] = curvePoints[i].convertToTuple()
            if isInt:
                curvePoints[i] = tuple(round(curvePoints[i][k]) for k in xrange(len(curvePoints[i])))
        # print curvePoints
        return curvePoints

    ############################################################################
    # Private Functions
    # To only be called internally by the class
    ############################################################################

    def findX(self, x):
        points = self.getBezierCurveXYCoordinates()
        points = sorted(points, cmp=lambda pt1, pt2: self.compareX(pt1, pt2))
        if (points[0][0] > x or points[-1][0] < x):
            raise Exception("%d not in range of Bezier Path" %(x))
        for i in xrange(len(points)-1):
            if (points[i][0]-x <= 0 and points[i+1][0]-x >= 0):
                 return (points[i], points[i+1])

    def compareX(self, point1, point2):
        if point1[0] < point2[0]: return -1
        if point1[0] == point2[0]: return 0
        if point1[0] > point2[0]: return 1

    def getIntermediateControlPoints(self):
        """Converts the control points that were passed to the class to a list 
        of rectangular coordinates that are the locations of the actual controlPoints
        points"""
        controlPoints = []
        for i in xrange(len(self.controlPoints)):
            controlPoint = self.controlPoints[i]
            points = self.convertControlPointThreeTupleToThreeVectors(controlPoint)
            if i != 0: controlPoints.append(points[0])
            controlPoints.append(points[1])
            if i != len(self.controlPoints)-1: controlPoints.append(points[2])
        return controlPoints

    def convertControlPointThreeTupleToThreeVectors(self, controlPoint):
        rectangularPoints = []
        p0 = controlPoint[0]
        slope = Vector.normalizedVector(controlPoint[1]) # Vector(dx, dy)
        r = controlPoint[2]
        for direction in xrange(-1, 2):
            point = Vector.add(p0, Vector.mult(slope, direction*r))
            rectangularPoints.append(point)
        return rectangularPoints

    def iterateOverControlPoints(self, controlPoints):
        """Iterate over control points, yielding 4 at a time (necessary to draw
            cubic bezier curve,) and moving through the control points by twos"""
        for i in xrange((len(controlPoints)-1)/3):
            yield controlPoints[3*i:3*(i+1)+1]

    @staticmethod
    def combination(n, r):
        return math.factorial(n)/(math.factorial(r)*math.factorial(n-r))

    def bezierCurveFormula(self, point):
        """Returns a parametric formula based on the four control points that takes 
        in a parameter, t, and returns the (x,y) coordinates cirresponding to it."""
        if len(point) != 4: raise Exception("Need four control points for \
                                                    a cubic bezier curve")
        # The formula to take a parameter in [0,1] and return the corresponding xy coordinate
        def formula(t):
            t = float(t)
            resultVector = Vector()
            for i in xrange(len(point)):
                p = point[i]
                coeff = BezierCurve.combination(3, i)*(1-t)**(3-i)*t**i
                resultVector = Vector.add(resultVector, Vector.mult(p, coeff))
            return resultVector
        return formula

# bc = BezierCurve([(0,50),(25,100),(75,100),(100,50)])
# bc.addPoints([(175,0),(200,50)])
# print bc.getBezierCurveXYCoordinates()
