import math

# The below method is intended for scaling a continuous spectrum.  For example,
# take pitch.  We would expect most students' pitch to be in a certain
# range, but students' pitch might be more or less.  However, in these extreme
# circumstances we don't want the character to behave haywire (e.g. move off of
# the screen.)  Hence, we scale it.  The below method takes a value,
# minExpectedValue, and maxExpectedValue, and returns a float between 0 and 1.
# It is built so that a value at the minExpected will return 0.119, a value at
# maxExpected will return 0.881, and a value at the average expected value
# returns 0.5.  Higher and lower values asymptotically approach 1 and 0.

def scaledValueInRange(value, minExpectedValue, maxExpectedValue):
    valueRange = maxExpectedValue - minExpectedValue
    average = float(maxExpectedValue + minExpectedValue)/2
    scaledValue = 1.0/(1+math.exp(4.0*(-value+average)/valueRange))
    return scaledValue

def inverseScaledValueInRange(scaledValue, minExpectedValue, maxExpectedValue):
    valueRange = maxExpectedValue - minExpectedValue
    average = float(maxExpectedValue + minExpectedValue)/2
    value = average - (valueRange*math.log(1.0/scaledValue-1.0))/4.0
    return value

# for i in xrange(100):
#     scaledValue = scaledValueInRange(i, 50, 80)
#     value = inverseScaledValueInRange(scaledValue, 50, 80)
#     print i, scaledValue, value, i==int(value)
