from Tkinter import *
from tools.listener import *
import math
import random

class BalloonGame(object):
    @staticmethod
    def rgb(red, green, blue):
        return "#%02x%02x%02x" % (red, green, blue)

    def __init__(self, width=500, height=500):
        self.width = width
        self.height = height
        self.timerDelay = 50
        self.volumeThreshold = 50

    def initAnimation(self):
        self.radius = 25
        self.maxRadius = random.randint(150,200)
        self.theta = math.pi/5
        # scaleFactor is necessary because if data is streamed faster than we 
        # read it, we get an IOError.  Since Tkinter timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(4)/3
        self.listener = Listener(float(self.timerDelay)*scaleFactor/1000)
        self.listener.startStream()
        self.drawBalloon()

    def onTimerFired(self):
        (volume, freq) = self.listener.listen()
        if (volume > self.volumeThreshold): self.detectedVoice()
        if self.timerDelay == None: return
        self.canvas.after(self.timerDelay, self.onTimerFired)

    def detectedVoice(self):
        self.radius += 1
        self.updateBalloon()
        if (self.radius >= self.maxRadius):
            self.explosion()
            self.timerDelay = None

    def drawBalloon(self):
        color = BalloonGame.rgb(255, 128, 0)
        (cx,cy, r) = (self.width/2, self.height/2, self.radius)
        (x0,y0) = (cx-r, cy-r)
        (x1,y1) = (cx+r, cy+r)
        self.canvas.create_oval(x0, y0, x1, y1,fill=color, outline="white", 
                                tag="balloon")
        (x0,y0) = (cx-r*math.sin(self.theta), cy+r*math.cos(self.theta))
        (x1,y1) = (cx+r*math.sin(self.theta), cy+r*math.cos(self.theta))
        (x2,y2) = (cx, cy+r/math.cos(self.theta))
        self.canvas.create_polygon(x0,y0,x1,y1,x2,y2,fill=color, 
                                   tag="balloon")

    def updateBalloon(self):
        self.canvas.delete("balloon")
        self.drawBalloon()

    def explosion(self):
        self.canvas.delete("balloon")
        explosionSpikes = 14
        explosionInnerRadius = self.maxRadius*3/4
        explosionOuterRadius = self.maxRadius*5/4
        rand = 20
        (cx,cy) = (self.width/2, self.height/2)
        bbox = []
        for i in xrange(2*explosionSpikes):
            theta = i*math.pi/(explosionSpikes)
            if i%2 == 0: #every other spike is on the cirlce
                rI = explosionInnerRadius
                (x,y) = (int(cx+rI*math.sin(theta)), int(cy-rI*math.cos(theta)))
                bbox.append(random.randint(x-rand, x+rand))
                bbox.append(random.randint(y-rand, y+rand))
            else:
                rO = explosionOuterRadius
                (x,y) = (int(cx+rO*math.sin(theta)), int(cy-rO*math.cos(theta)))
                bbox.append(random.randint(x-rand, x+rand))
                bbox.append(random.randint(y-rand, y+rand))
        self.canvas.create_polygon(*bbox, fill="red", outline="black", 
                                   tag="explosion")



    def quit(self):
        self.listener.closeStream()
        self.root.quit()

    def run(self):
        self.root = Tk()
        self.canvas = Canvas(self.root, width=self.width, height=self.height)
        self.canvas.pack()
        self.initAnimation()
        self.onTimerFired()
        self.root.mainloop()

game = BalloonGame()
game.run()