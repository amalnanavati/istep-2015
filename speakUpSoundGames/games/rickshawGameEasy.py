################################################################################
# Medium difficulty rickshaw game, where the rickshaw has to drive for longer
# and there are mild hills.
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random, copy, math
from pygame.locals import *
from tools.listener import *
from tools.bezierCurve import *
from tools.button import *
from tools.misc import *

class Rickshaw(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y):
        """Initializes sprite1"""
        (x,y) = (int(x), int(y))
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        rick = pygame.image.load('img/rickshaw.png')
        # Scale the image
        (rickW,rickH) = rick.get_rect().size
        width = 250
        height = width*rickH/rickW
        self.unrotatedImage = pygame.transform.scale(rick, (width, height))
        self.image= self.unrotatedImage
        self.mask = pygame.mask.from_surface(self.image)
        self.unrotatedRect = pygame.Rect(x-width/2, y-height, width, height)
        self.rect = copy.deepcopy(self.unrotatedRect)
        self.rotatedRickBbox = ((x-width/2, y-height), (x+width/2, y-height), (x+width/2, y),(x-width/2, y))
        self.theta = 0
        # print self.rotatedRickBbox
        self.speedMultiplier = 1 # acceleration/deceleration

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def shiftBy(self, dPoint):
        (dx,dy) = dPoint
        (dx,dy) = (int(dx),int(dy))
        self.unrotatedRect.x += dx
        self.rect.x += dx
        self.unrotatedRect.y += dy
        self.rect.y += dy
        # print self.rect
        rr = self.rotatedRickBbox
        self.rotatedRickBbox=((rr[0][0]+dx, rr[0][1]+dy),(rr[1][0]+dx, rr[1][1]+dy),(rr[2][0]+dx, rr[2][1]+dy),(rr[3][0]+dx, rr[3][1]+dy))
        # print self.rotatedRickBbox

    def rotateByRadians(self, theta):
        self.theta = theta
        (w,h) = (self.unrotatedRect.width, self.unrotatedRect.height)
        (x3,y3) = self.rotatedRickBbox[3]
        (x2,y2) = (x3+math.cos(theta)*w, y3-math.sin(theta)*w)
        (x1,y1) = (x2-math.sin(theta)*h, y2-math.cos(theta)*h)
        (x0,y0) = (x3-math.sin(theta)*h, y3-math.cos(theta)*h)
        self.rotatedRickBbox = ((int(x0),int(y0)),(int(x1),int(y1)),(int(x2),int(y2)),(int(x3),int(y3)))
        # print self.rotatedRickBbox
        self.image = pygame.transform.rotate(self.unrotatedImage, math.degrees(theta))
        self.mask = pygame.mask.from_surface(self.image)
        self.rect=self.image.get_rect()
        # print "rect", self.rect, (min(x0, x3), min(y0, y1), max(x2, x2)-min(x0, x3), max(y2, y3)-min(y0, y1))
        self.rect.x = min(x0, x3)
        self.rect.y = min(y0, y1)

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Ground(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, twiceScreenWidth, screenHeight):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((twiceScreenWidth,screenHeight), flags=pygame.SRCALPHA, depth=32)
        self.image.set_colorkey((0x00,0x00,0x00))
        self.image = self.image.convert_alpha()
        self.rect = pygame.Rect(0, 0, twiceScreenWidth, screenHeight)
        self.groundY = screenHeight-50
        self.loadPath()
        self.drawPath()

    def loadPath(self):
        """Load initial view elements"""
        pathControlPoints = [((0,self.groundY),(1,0),1), ((self.rect.width/2-1,self.groundY),(1,0),1)]
        bumpControlPoints = self.generateBumpInInterval(self.rect.width/2, self.rect.width)
        for point in bumpControlPoints: pathControlPoints.append(point)
        self.path = BezierCurve(pathControlPoints)

    def generateBumpInInterval(self, startX, endX, margin=50, maxBumpHeight=40, minRad=20, groundBumpiness=4):
        x = random.randint(startX+margin, endX-margin)
        y1 = random.randint(self.groundY-maxBumpHeight, self.groundY)
        y2 = random.randint(self.groundY-groundBumpiness, self.groundY+groundBumpiness)
        rad = random.randint(minRad, min(x-startX, endX-x))
        points = [((x,y1),(1,0),rad),((endX, y2),(1,0),margin)]
        return points

    def drawPath(self):
        """Draws the path for the rickshaw to drive on"""
        self.image.fill((0xFF,0xFF,0xFF,0))
        polygonPoints = [(0, self.rect.height)]
        pathPoints = self.path.getBezierCurveXYCoordinates(intervals=40)
        margin = 50
        for point in pathPoints:
            # Only add points that are on the screen
            if (point[0] >= -margin and
                point[0] <= self.rect.width+margin and
                point[1] >= -margin and
                point[1] <= self.rect.height+margin):
                polygonPoints.append(point)
        polygonPoints.append((self.rect.width, self.rect.height))
        pygame.draw.polygon(self.image, (0xCC, 0x99, 0x00), polygonPoints)
        self.mask = pygame.mask.from_surface(self.image)

    def shiftOverBy(self, dPoint):
        self.path.shiftPathOverBy(dPoint)
        toRemove = []
        for point, index in self.path.iteritems():
            # print point
            x = point[0].convertToTuple()[0]
            if x < -1*self.rect.width: toRemove.append(point)
        for point in toRemove:
            self.path.removeControlPoint(point)
        self.drawPath()

    def extendPath(self):
        self.path.addControlPoints(self.generateBumpInInterval(self.rect.width/2, self.rect.width))

    def getApproxYForX(self, x):
        return self.path.getApproxYForX(x)

    def getApproxSlopeForX(self, x):
        return self.path.getApproxSlopeForX(x)


    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Background(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y, height, screenWidth):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        imgPath = 'img/rickshawBackground2.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        (spriteW,spriteH) = sprite.get_rect().size
        width = height*spriteW/spriteH
        backgroundImg = pygame.transform.scale(sprite, (width, height))
        # Set the sprit's attributes
        self.numberOfImages = max(int(math.ceil(float(2)*screenWidth/width)), 4)
        print self.numberOfImages
        self.image = pygame.Surface((self.numberOfImages*width, height))
        for i in xrange(self.numberOfImages):
            self.image.blit(backgroundImg, (width*i, 0))
        self.rect = pygame.Rect(x, y, self.numberOfImages*width, height)

    def shiftOverBy(self, dPoint):
        self.rect.x += dPoint[0]
        self.rect.y += dPoint[1]
        if self.rect.x <= -2*self.rect.width/self.numberOfImages: self.rect.x = -self.rect.width/self.numberOfImages

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class House(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        imgPath = 'img/MathruHostel.png'
        # Load the image
        sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        width = 400
        (spriteW,spriteH) = sprite.get_rect().size
        height = width*spriteH/spriteW
        image = pygame.transform.scale(sprite, (width, height))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x-width, y-height, width, height)
        self.mask = pygame.mask.from_surface(self.image)

    def shiftOverBy(self, dPoint):
        self.rect.x += dPoint[0]
        self.rect.y += dPoint[1]

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self):
        """Moves the sprite"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class RickshawGameEasy(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=70, minExpectedVolume=60, maxExpectedVolume=80):
        print volumeThreshold
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold, minExpectedVolume, maxExpectedVolume):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1200, 500)
        # widthHeightRatio=float(defaultW)/defaultH
        # actualRatio = float(currentW)/currentH
        # if actualRatio > widthHeightRatio:
        #     self.screenHeight = min(currentH, defaultH)
        #     self.screenWidth = int(widthHeightRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentW, defaultW)
        #     self.screenHeight = int(self.screenWidth/widthHeightRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH
        self.screenPos = (0,0) # Where the screen is in relation to the game world
        self.maxScreenSpeed = 25
        self.screenSpeed = 5
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.elapsedTicks = {} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()
        self.minExpectedVolume, self.maxExpectedVolume = minExpectedVolume, maxExpectedVolume
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Retrive Best Time Info
        try:
            with open('RickshawGameEasyBestTime.txt', 'rt') as doc:
                self.bestTime = int(doc.read())
        except IOError:
            self.bestTime = None

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            if "end" in self.elapsedTicks and self.elapsedTicks["end"] > self.delayBeforeClosingGame*self.fps:
                self.quit()
        else:
            self.time += 1
            self.adjustRickToBeOnCurve()

            # (bottomLeft, bottomRight) = self.getRickPositionOnPath()
            # print bottomLeft, bottomRight
            # Calls every sprite's update method
            self.allSpritesList.update()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (volume > self.volumeThreshold or (pitch != None and pitch > 50)):
                self.detectedVoice(volume, pitch)
            else:
                self.noVoice(volume, pitch)
            self.checkCollision()
        # Update the view
        self.update()

    def checkCollision(self):
        """Checks whether an enemy has collided with a bullet and/or with the
        shooter"""
        if pygame.sprite.collide_mask(self.rick, self.house):
            self.gameOver()

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        x3 = self.rick.rotatedRickBbox[3][0]
        x2 = self.rick.rotatedRickBbox[2][0]
        x3Slope = self.ground.getApproxSlopeForX(x3)
        x2Slope = self.ground.getApproxSlopeForX(x2)
        # print x3Slope, x2Slope
        slopeMultiplier = 20 # Slopes < 1/4 are flat, higher slopes cause movement
        x3Slope = int(x3Slope*slopeMultiplier)
        x2Slope = int(x2Slope*slopeMultiplier)

        scaledVoice = scaledValueInRange(volume, self.minExpectedVolume, self.maxExpectedVolume)
        self.targetSpeed = int((self.maxScreenSpeed-1)*scaledVoice)+1+(x3Slope+x2Slope)/2
        if self.screenSpeed < self.targetSpeed: self.screenSpeed += 2
        elif self.screenSpeed > self.targetSpeed: self.screenSpeed -= 2
        # self.targetSpeed = self.maxScreenSpeed
        # if self.screenSpeed < self.targetSpeed: self.screenSpeed += 1
        self.moveScreen()

    def noVoice(self, volume, pitch):
        """Called whenever volume below the volumeThreshold is detected"""
        x3 = self.rick.rotatedRickBbox[3][0]
        x2 = self.rick.rotatedRickBbox[2][0]
        x3Slope = self.ground.getApproxSlopeForX(x3)
        x2Slope = self.ground.getApproxSlopeForX(x2)
        # print x3Slope, x2Slope
        slopeMultiplier = 25 # Slopes < 1/4 are flat, higher slopes cause movement
        x3Slope = int(x3Slope*slopeMultiplier)
        x2Slope = int(x2Slope*slopeMultiplier)
        self.targetSpeed = int(x3Slope + x2Slope)
        if self.screenSpeed < self.targetSpeed: self.screenSpeed += 2
        elif self.screenSpeed > self.targetSpeed: self.screenSpeed -= 2
        self.moveScreen()

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Sets attributes for game world
        baseGameWorldWidth, deviance = 8000, 2000
        self.gameWorldWidth = random.randint(baseGameWorldWidth-deviance, baseGameWorldWidth+deviance)
        self.gameWorldHeight = self.screenWidth
         # Loads the path for the rickshaw to drive on
        self.groundY = self.screenHeight*9/10
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.OrderedUpdates()
        self.buttonsList = pygame.sprite.Group()
        self.background = Background(0,0,self.screenHeight, self.screenWidth)
        self.allSpritesList.add(self.background)
        self.house = House(self.gameWorldWidth, self.groundY)
        self.allSpritesList.add(self.house)
        self.ground = Ground(self.screenWidth*2, self.screenHeight)
        self.allSpritesList.add(self.ground)
        self.rick = Rickshaw(self.screenWidth/2, self.groundY)
        self.allSpritesList.add(self.rick)
        self.distanceSinceExtended = 0
        self.distanceRemaining = self.gameWorldWidth-self.screenWidth
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.time = 0

    def adjustRickToBeOnCurve(self):
        self.setRickBackWheelPositionOnPath()
        (x2,y2) = self.rick.rotatedRickBbox[2]
        (x3,y3) = self.rick.rotatedRickBbox[3]
        distanceToWheel = 10
        x2 -= math.cos(self.rick.theta)*distanceToWheel # Get approx bottom of wheel
        y2 -= 4 # Get Rickshaw on ground
        y3 -= 4 # Get Rickshaw on ground
        # print "x", x2
        yNew = self.ground.path.getApproxYForX(x2)
        theta = -1*math.atan((yNew-y3)/(x2-x3))
        # print "rotate", yNew, y3, math.degrees(theta)
        self.rick.rotateByRadians(theta)
        self.setRickBackWheelPositionOnPath()

    def setRickBackWheelPositionOnPath(self):
        (x3,y3) = self.rick.rotatedRickBbox[3]
        distanceToWheel = 10
        x3 += math.cos(self.rick.theta)*distanceToWheel # Get approx bottom of wheel
        y3 -= 4 # Get Rickshaw on ground
        yNew = self.ground.getApproxYForX(x3)
        # print (x3, yNew)
        self.rick.shiftBy((0,yNew-y3))

    def gameOver(self):
        """Called when the game is over."""
        print "Game Over!"
        # self.elapsedTicks["end"] = 0
        self.delayBeforeClosingGame = 2 #seconds
        if self.bestTime is None or self.time < self.bestTime:
            try:
                with open('RickshawGameEasyBestTime.txt', 'wt') as doc:
                    doc.write(str(self.time))
            except IOError:
                pass
            isBestTime = True
        else:
            isBestTime = False
        self.createGameOverText(isBestTime=isBestTime)
        self.finished = True

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the font
        self.fontHeight = 30
        self.font = pygame.font.SysFont("Arial", self.fontHeight)
        # Load the View
        self.createMenuButton()
        self.updateTimeText()
        self.createDistanceRemainingText()
        self.updateBestTimeText()
        # Display the view
        self.update()

    def moveScreen(self):
        self.distanceSinceExtended += self.screenSpeed
        self.distanceRemaining -= self.screenSpeed
        self.createDistanceRemainingText()
        self.ground.shiftOverBy((-self.screenSpeed, 0))
        if self.distanceSinceExtended > self.screenWidth:
            self.ground.extendPath()
            self.distanceSinceExtended = 0
        self.background.shiftOverBy((-self.screenSpeed, 0))
        self.house.shiftOverBy((-self.screenSpeed, 0))

    def updateTimeText(self):
        """Updates the score text with the current score"""
        seconds = int(self.time*self.timeBetweenFrames)
        # print seconds
        minutes = seconds/60
        seconds %= 60
        self.timeText = self.font.render("Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))

    def updateBestTimeText(self):
        if self.bestTime is None:
            self.bestTimeText = self.font.render("Shortest Time: "+str(self.bestTime), 1, (0,0,0))
        else:
            seconds = int(self.bestTime*self.timeBetweenFrames)
            # print seconds
            minutes = seconds/60
            seconds %= 60
            self.bestTimeText = self.font.render("Shortest Time: %d:%02d" %(minutes, seconds), 1, (0,0,0))

    def createGameOverText(self, isBestTime):
        """Updates the score text with the current score"""
        font = pygame.font.SysFont("Arial", 70)
        if isBestTime:
            text = "Game Over. Best Time!"
        else:
            text="Game Over"
        self.gameOverText = font.render(text, 1, (0,0,0))

    def createDistanceRemainingText(self):
        """Updates the score text with the current score"""
        self.distanceRemainingText = self.font.render("Distance Remaining: "+str(int(round(float(self.distanceRemaining)/1000)))+" km", 1, (0,0,0))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        # self.drawPath()
        self.allSpritesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        self.updateTimeText()
        self.surface.blit(self.timeText, (0, 0))
        self.surface.blit(self.distanceRemainingText, (0, self.fontHeight*3/2))
        self.surface.blit(self.bestTimeText, (0,self.fontHeight*3))
        if self.finished:
            self.surface.blit(self.gameOverText, self.gameOverText.get_rect(centerx=self.screenWidth/2, centery=self.screenHeight/3))
        pygame.display.update()

# game = RickshawGameEasy(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
