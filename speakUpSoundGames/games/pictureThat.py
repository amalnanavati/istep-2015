################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
#       - Maya Lassiter (mlassite@andrew.cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random, os
from pygame.locals import *
from tools.listener import *
from tools.button import *
from tools.GIFImage import *

class Image(pygame.sprite.Sprite):
    """Game Token class description"""

    def __init__(self, x, y, width, height, imgPath):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        if ".gif" in imgPath:
            image = pygame.Surface((width, height))
            image.fill((0xFF, 0xFF, 0xFF))
            print "imgPath: ", imgPath
            img = GIFImage(imgPath, width, height)
            img.render(image, (width/2, height/2))
        else:
            image = pygame.Surface((width, height))
            image.fill((0xFF, 0xFF, 0xFF))
            img = pygame.image.load(imgPath)
            # Scale the image to the desired width
            (imageW,imageH) = img.get_rect().size
            scaleFactor = min(float(width)/imageW, float(height)/imageH)
            imgWidth, imgHeight = int(imageW*scaleFactor), int(imageH*scaleFactor)
            img = pygame.transform.scale(img, (imgWidth, imgHeight))
            image.blit(img, img.get_rect(centerx=width/2, centery=height/2))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)

    def update(self, *args):
        """This method is called every clock tick"""
        pass

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Mask(pygame.sprite.Sprite):
    """Game Token class description"""

    def __init__(self, x, y, width, height):
        """Initializes Token"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        # Load the image
        image = pygame.Surface((width, height))
        image.fill((0xFF, 0xFF, 0xFF))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)
        self.alpha = 255

    def update(self, *args):
        """This method is called every clock tick"""
        print self.alpha,
        self.alpha -= 1 if self.alpha > 175 else 3
        if self.alpha < 0: self.alpha = 0
        print self.alpha
        self.image.set_alpha(self.alpha)

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        pass

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class PictureThat(object):
    """The Game"""

    # displayMode: 0 = word first, 1 = picture first, 2 = word and picture together
    def __init__(self, returnToMenu, currentWidth, currentHeight, volumeThreshold=60, wordPictureList={}, displayMode=1):
        self.returnToMenu = returnToMenu
        self.initController(currentWidth, currentHeight, volumeThreshold)
        self.initModel(wordPictureList, displayMode)
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1200, 900)
        # defaultRatio = float(defaultW)/defaultH
        # actualRatio = float(currentWidth)/currentHeight
        # if actualRatio > defaultRatio:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = int(defaultRatio*self.screenHeight)
        # else:
        #     self.screenWidth = min(currentWidth, defaultW)
        #     self.screenHeight = int(self.screenWidth/defaultRatio)
        self.screenWidth = currentW
        self.screenHeight = currentH

        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.setClockRelated()
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()

    def setClockRelated(self):
        self.ticksBetweenEnemies = 80

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # If game is over
        if self.finished:
            pass
        else:
            self.sendUpdates()
        # Update the view
        self.update()

    def sendUpdates(self):
        #To make sprite updates depend on volume, move this line to detectedVoice
        #self.allSpritesList.update(self.screenWidth, 50)
        (volume, pitch) = self.listener.listen()
        if (volume > self.volumeThreshold or pitch != None):
                self.detectedVoice()

    def detectedVoice(self):
        """Called whenever volume above the volumeThreshold is detected"""
        if len(self.imageList) > 0:
            if self.displayMode == 1:
                self.titleAlpha += 1 if self.titleAlpha > 75 else 2
                if self.titleAlpha < 0: self.titleAlpha = 0
            else:
                self.mask.update()


    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            #self.removeEnemy()
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self, wordPictureList, displayMode):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.imageGroup = pygame.sprite.Group()
        self.maskGroup = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        self.displayMode = displayMode
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        # Create Image Dictionary and ImageCaptions list
        self.imageList = wordPictureList
        random.shuffle(self.imageList)
        print self.imageList
        self.imageIndex = 0
        # Create image and mask
        self.createImageAndMask()

    def moveImage(self, dIndex):
        if len(self.imageList) > 0:
            self.imageIndex += dIndex
            self.imageIndex %= len(self.imageList)
            self.createImageAndMask()

    def createImageAndMask(self):
        """Creates a player token and adds it to the Sprite List"""
        self.imageGroup = pygame.sprite.Group()
        self.maskGroup = pygame.sprite.Group()
        if len(self.imageList) > 0:
            caption = random.choice(self.imageList[self.imageIndex][0])
            img = random.choice(self.imageList[self.imageIndex][1])
            self.image = Image(x=0, y=0, width=self.screenWidth, height=self.screenHeight, imgPath=img)
            self.imageGroup.add(self.image)
            self.currentText = caption
            self.mask = Mask(0,0,self.screenWidth, self.screenHeight)
            self.maskGroup.add(self.mask)
        else:
            self.currentText = "No pictures in the selected folder"
            self.mask = Mask(0,0,self.screenWidth, self.screenHeight)
            self.maskGroup.add(self.mask)


    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption("Image Now!")
        if self.displayMode == 1: self.titleAlpha = 0
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.drawBackground()
        self.createMenuButton()
        self.createNextButton()
        self.createRepeatButton()
        self.createPreviousButton()
        self.detectedVoice()

    def drawBackground(self):
        self.surface.fill((0xFF, 0xFF, 0xFF))

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def createNextButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = (self.screenWidth-margin-width, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda dIndex: self.moveImage(dIndex), callbackArgument=(1), text="Next")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def createRepeatButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = ((self.screenWidth-width)/2, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda dIndex: self.moveImage(dIndex), callbackArgument=(0), text="Repeat")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def createPreviousButton(self):
        margin = 20
        (width, height) = (120, 50)
        (x,y) = (margin, self.screenHeight-margin-height)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda dIndex: self.moveImage(dIndex), callbackArgument=(-1), text="Previous")
        self.buttonsList.add(menu)
        # self.allSpritesList.add(menu)

    def drawTitleText(self):
        # Create Text
        margin = self.screenHeight/20
        fontHeight = 50
        font = pygame.font.SysFont("Arial", fontHeight)
        pictureName = font.render(self.currentText, 1, (0,0,0))
        pictureSurface = pygame.Surface((pictureName.get_rect().width, pictureName.get_rect().height), pygame.SRCALPHA, 32)
        # pictureSurface.fill((0xFF, 0xFF, 0xFF, 0x00))
        pictureSurface.convert_alpha()
        pictureSurface.blit(pictureName, (0, 0))
        if self.displayMode == 1: pictureSurface.set_alpha(self.titleAlpha)
        pictureSurfaceRect = pictureSurface.get_rect(centerx=self.screenWidth/2, centery=margin+fontHeight/2)
        self.surface.blit(pictureSurface, pictureSurfaceRect)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.drawBackground()
        # displayMode: 0 = word first, 1 = picture first, 2 = word and picture together
        if self.displayMode == 0:
            self.imageGroup.draw(self.surface)
            self.maskGroup.draw(self.surface)
            self.drawTitleText()
        elif self.displayMode == 1:
            # self.maskGroup.draw(self.surface)
            self.imageGroup.draw(self.surface)
            self.drawTitleText()
        elif self.displayMode == 2:
            self.imageGroup.draw(self.surface)
            self.drawTitleText()
            self.maskGroup.draw(self.surface)
        # self.enemiesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = PictureThat(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
