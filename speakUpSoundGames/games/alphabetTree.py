################################################################################
# Letter Tree sound game for Sound Games suite
# Using template.py for sound game development
#
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
#
# Copyright 2015 TechBridgeWorld
################################################################################

from template import *
import random, string

class Tree(Token):
    def __init__(self, width, cx, cy, speed = 6, imgPath = 'img/tree1.png'):
        """Initializes tree Token"""
        Token.__init__(self, width=width, x=cx, y=cy, speed = 3, imgPath = imgPath)
        self.rect.y -= self.rect.height/2
        self.rect.x -= self.rect.width/2
        self.defaultx = self.rect.x
        self.defaulty = self.rect.y

    def move(self, screenWidth):
        """Creates a shake effect with the Tree"""
        dx, dy = random.randint(-3, 3), random.randint(-1, 1)
        self.rect.x = self.defaultx + dx
        self.rect.y = self.defaulty + dy

    def update(self, screenWidth, *args):
        """This method is called every clock tick"""
        self.move(screenWidth)

class Letter(Enemy):
    def __init__(self, x, y, health, fontsize, letter, color, speed):
        pygame.sprite.Sprite.__init__(self)
        self.fontsize = fontsize
        font = pygame.font.SysFont("Arial", self.fontsize)
        self.letter = letter
        self.color = color
        text = font.render(self.letter, 1, self.color)
        image = pygame.Surface((text.get_rect().width, text.get_rect().height), flags=pygame.SRCALPHA, depth=32)
        image = image.convert_alpha()
        image.blit(text, image.get_rect(x=0, y=0))
        self.image = image
        # self.mask = pygame.mask.from_surface(self.image)
        self.rect = text.get_rect(x=x, y=y)
        self.speed = speed
        self.health = health

    def gotHit(self):
        if self.health < 0:
            return
        else:
            self.health -= 1
            centerx,centery = self.rect.centerx, self.rect.centery
            pygame.sprite.Sprite.__init__(self)
            self.fontsize = int(self.fontsize*1.07)
            font = pygame.font.SysFont("Arial", self.fontsize)
            text = font.render(self.letter, 1, self.color)
            image = pygame.Surface((text.get_rect().width, text.get_rect().height), flags=pygame.SRCALPHA, depth=32)
            image = image.convert_alpha()
            image.blit(text, image.get_rect(x=0, y=0))
            self.image = image
            # self.mask = pygame.mask.from_surface(self.image)

    def move(self):
        """Only fall if the health has been diminished"""
        tick = 0
        if self.health < 0:
            tick += 1
            self.rect.y += self.speed * (tick*tick)

class AlphabetTree(Game):
    """ Falling Letter game """
    def __init__(self, returnToMenu, currentWidth, currentHeight, volumeThreshold=65, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(volumeThreshold, currentWidth, currentHeight)
        self.pitch = None
        self.maxVolume = 80
        self.initModel()
        self.initView()

    def setClockRelated(self):
        self.ticksBetweenEnemies = 50
        self.elapsedTicks = {'enemy':10}

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.enemiesList = pygame.sprite.Group()
        self.listOfEnemies = []
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        #create player game token
        self.createToken()

    def createToken(self):
        self.leaves = Tree(width=800, cx=self.screenWidth/2, cy=self.screenHeight/2-125, speed=6, imgPath = 'img/leaves.png')
        self.trunk = Tree(width=450, cx=self.screenWidth/2, cy=self.screenHeight/2+175, speed=6, imgPath = 'img/treetrunk.png')
        self.allSpritesList.add(self.leaves)
        self.allSpritesList.add(self.trunk)

    def createEnemy(self):
        letter = random.choice(string.ascii_letters+string.digits)
        color = random.choice([(148,0,211),(75,0,130),(0,0,255),(0,255,0),(255,255,0),(255,127,0),(255,0,0)])
        width = 15
        speed = random.randint(3, 7)
        health = random.randint(20, 40)
        y = random.randint(0, (self.leaves.rect.height/2))
        x = random.randint(0, self.leaves.rect.width) if y > self.leaves.rect.height/4 else random.randint(self.leaves.rect.width/3, (2*self.leaves.rect.width)/3)
        print "Creating letter"
        letter = Letter(x = x+self.leaves.rect.x, y = y+self.leaves.rect.y, fontsize=30,
                        health = health, color = color, letter=letter, speed=speed)
        self.enemiesList.add(letter)
        self.listOfEnemies.append(letter)
        self.allSpritesList.add(letter)

    def sendUpdates(self):
        self.enemiesList.update()
        (volume, pitch) = self.listener.listen()
        if (volume > self.volumeThreshold or pitch != None):
                self.detectedVoice(volume, pitch)

    def detectedVoice(self, volume, pitch):
        self.pitch = pitch
        scale = volume/self.maxVolume
        if scale > 1:
            scale = 1
        effect = int(scale*len(self.enemiesList))
        self.leaves.update(self.screenWidth)
        for letter in self.listOfEnemies[:effect]:
            letter.gotHit()

    def removeEnemy(self):
        """Remove enemies after they fall off of the screen"""
        numPopped = 0
        for i in xrange(len(self.listOfEnemies)):
            letter = self.listOfEnemies[i-numPopped]
            if letter.rect.y > self.screenHeight:
                letter.destroy()
                self.listOfEnemies.pop(i-numPopped)
                numPopped += 1


    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()
        #start with some letter in tree
        self.createEnemy()
        self.createEnemy()

    def update(self):
        """Updates the view, by drawing the back`ground, sprites, and score"""
        self.surface.fill((0xF1, 0xCC, 0x66))
        self.allSpritesList.draw(self.surface)
        self.enemiesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# AlphabetTree = AlphabetTree(lambda: 1, 1000, 1000, 55)
# AlphabetTree.run()
# pygame.quit()
# sys.exit()
