################################################################################
# Template for making games in the Sound Games Package
# Authors:
#       - Amal Nanavati (amaln@cmu.edu)
#       - Maya Lassiter (mlassite@andrew.cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *

class Enemy(pygame.sprite.Sprite):
    """The enemy sprite. Enemy sprites keep track of their own health and move
    themselves. Drawing the sprite is done in Game.update()

    This basic template creates an enemy that moves downwards with each tick"""
    def __init__(self, x, y, speed, health, width, imgPath):
        pygame.sprite.Sprite.__init__(self)
        enemy = pygame.image.load(imgPath)
        (enemyW, enemyH) = enemy.get_rect().size
        height = width*enemyH/enemyW
        image = pygame.transform.scale(enemy, (width, height))

        self.image = image
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed
        self.health = health

    def update(self, *args):
        """This method is called every clock tick from
        ShooterGame.onClockTick().  It moves the enemy."""
        self.move()

    def move(self):
        """Moves the enemy."""
        self.rect.y += self.speed

    def gotHit(self):
        """Decrements the health by 1.  If enemy has no health left, destroy it.
        This method is called from ShooterGame.checkCollision() whenever a
        bullet hits the enemy."""
        self.health -= 1

    def getHealth(self):
        """Returns enemy health, called from ShooterGame.checkCollision()"""
        return self.health

    def destroy(self):
        """Destroys the enemy."""
        self.kill()

class Token(pygame.sprite.Sprite):
    """Game Token class description"""

    def __init__(self, x, y, width = 100, speed=6, imgPath='img/jackfruit.png'):
        """Initializes Token"""
        # Initialize the sprite
        #print x, y, speed, imgPath
        pygame.sprite.Sprite.__init__(self)
        #imgPath = 'img/'
        # Load the image
        self.sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        width = width
        (spriteW,spriteH) = self.sprite.get_rect().size
        height = width*spriteH/spriteW
        image = pygame.transform.scale(self.sprite, (width, height))
        # Set the sprit's attributes
        self.image = image
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed

    def update(self, *args):
        """This method is called every clock tick"""
        self.move(args[0])

    def move(self, screenWidth):
        """Moves the sprite horizontally with self set speed"""
        if (self.rect.x < 0 or self.rect.x > screenWidth-self.rect.width):
            self.speed *= -1 #Flip token direction at edge of screen
        self.rect.x += self.speed

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Game(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=65, minExpectedVolume=60, maxExpectedVolume=80):
        self.returnToMenu = returnToMenu
        self.initController(volumeThreshold, currentW, currentH)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, volumeThreshold, currentW, currentH):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        # (defaultW, defaultH) = (1000, 1000)
        # defaultRatio = float(defaultW)/defaultH
        # actualRatio = float(currentWidth)/currentHeight
        # if actualRatio > defaultRatio:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        # else:
        #     self.screenHeight = min(currentHeight, defaultH)
        #     self.screenWidth = min(currentWidth, defaultW)
        self.screenWidth = currentW
        self.screenHeight = currentH
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.setClockRelated()
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()

    def setClockRelated(self):
        self.ticksBetweenEnemies = 80
        self.elapsedTicks = {'enemy':10} # Keeps track of elapsed time since events

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems():
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished:
            pass
        else:
            if self.elapsedTicks['enemy'] > self.ticksBetweenEnemies:
                self.createEnemy()
                self.elapsedTicks['enemy'] = 0
            self.sendUpdates()
        # Update the view
        self.update()

    def sendUpdates(self):
        #To make sprite updates depend on volume, move this line to detectedVoice
        self.allSpritesList.update(self.screenWidth, 50)

        (volume, pitch) = self.listener.listen()
        if (volume > self.volumeThreshold or pitch != None):
                self.detectedVoice(volume, pitch)

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        pass

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            self.removeEnemy()
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score,
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.allSpritesList = pygame.sprite.Group()
        self.enemiesList = pygame.sprite.Group()
        self.buttonsList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0
        #create player game token
        self.createToken()

    def createToken(self):
        """Creates a player token and adds it to the Sprite List"""
        self.token = Token(x=0, y=self.screenHeight-75, width=75, speed=6) #x, y, speed
        self.allSpritesList.add(self.token)

    def createEnemy(self):
        """Create an enemy and add to list"""
        seed = random.randint(0, 10)
        if seed < 5: #create enemy type 1 with 1/2 chance
            (width, speed, health, imgPath) = (60, 1, 10, 'img/enemy1.png')
        else:
            (width, speed, health, imgPath) = (60, 1, 15, 'img/enemy2.png')
        enemy = Enemy(random.randint(0, self.screenWidth - width), 0, width = width,
                            speed = speed, health = health, imgPath = imgPath)
        self.enemiesList.add(enemy)
        self.allSpritesList.add(enemy)

    def removeEnemy(self):
        """Remove enemies after they fall off of the screen"""
        for enemy in self.enemiesList:
            if enemy.rect.y > self.screenHeight:
                enemy.destroy()

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Sound Game Template')
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(175,255), random.randint(175,255), random.randint(175,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)
        self.allSpritesList.add(menu)

    def update(self):
        """Updates the view, by drawing the back`ground, sprites, and score"""
        self.surface.fill((0xF1, 0xCC, 0x66))
        self.allSpritesList.draw(self.surface)
        self.enemiesList.draw(self.surface)
        self.buttonsList.draw(self.surface)
        pygame.display.update()

# game = Game(lambda: 1, 1280, 800)
# game.run()
# pygame.quit()
# sys.exit()
