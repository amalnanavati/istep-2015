################################################################################
# Template for making games in the Sound Games Package
# Authors: 
#       - Amal Nanavati (amaln@cmu.edu)
# Copyright 2015 TechBridgeWorld
################################################################################

import pygame, sys, Tkinter, random
from pygame.locals import *
from tools.listener import *
from tools.button import *

class Sprite(pygame.sprite.Sprite):
    """Sprite1 class description"""

    def __init__(self, x, y):
        """Initializes sprite1"""
        # Initialize the sprite
        pygame.sprite.Sprite.__init__(self)
        imgPath = 'img/'
        # Load the image
        sprite = pygame.image.load(imgPath)
        # Scale the image to the desired width
        width = 100
        (spriteW,spriteH) = sprite.get_rect().size
        height = width*spriteH/spriteW
        image = pygame.transform.scale(sprite, (width, height))
        # Set the sprit's attributes
        self.image = image
        self.rect = pygame.Rect(x, y, width, height)
        self.speed = speed

    def update(self, *args):
        """This method is called every clock tick"""
        self.move()

    def move(self):
        """Moves the sprite"""
        self.rect.y += self.speed

    def destroy(self):
        """Destroys the sprite"""
        self.kill()

class Game(object):
    """The Game"""

    def __init__(self, returnToMenu, currentW, currentH, volumeThreshold=50):
        self.returnToMenu = returnToMenu
        self.initController(currentW, currentH, volumeThreshold)
        self.initModel()
        self.initView()

    ############################################################################
    # Controller
    ############################################################################
    # The controller controls the clock and audio of the game.  It also controls
    # all pygame events, and is the interface between the model and view.

    def initController(self, currentW, currentH, volumeThreshold):
        """Initializes the game controller"""
        # Sets attributes for game screen size
        (defaultW, defaultH) = (800, 800)
        widthHeightRatio=float(defaultW)/defaultH
        actualRatio = float(currentW)/currentH
        if actualRatio > widthHeightRatio:
            self.screenHeight = min(currentH, defaultH)
            self.screenWidth = int(widthHeightRatio*self.screenHeight)
        else:
            self.screenWidth = min(currentW, defaultW)
            self.screenHeight = int(self.screenWidth/widthHeightRatio)
        # Initialize pygame
        pygame.init()
        # Initialize the game clock
        self.timeBetweenFrames = 0.050 #seconds
        self.fps = int(1/self.timeBetweenFrames)
        self.clock = pygame.time.Clock()
        # Set clock-related attributes
        self.elapsedTicks = {} # Keeps track of elapsed time since events
        # Sets the volume threshold, the level of background noise
        self.volumeThreshold = volumeThreshold
        # scaleFactor is necessary because if data is streamed faster than we 
        # read it, we get an IOError.  Since PyGame timing is not exact,
        # we increase the size of the listening buffer.
        scaleFactor = float(1.5)
        # Initializes the audio listener to listen once per frame
        self.listener = Listener(self.timeBetweenFrames*scaleFactor)
        self.listener.startStream()

    def onClockTick(self):
        """Called every clock tick, which is once per frame"""
        # Increments all tick counters by one
        for key, value in self.elapsedTicks.iteritems(): 
            self.elapsedTicks[key] = value+1
        # If game is over
        if self.finished: 
            pass
        else:
            # Calls every sprite's update method
            self.allSpritesList.update()
            # Listen for audio
            (volume, pitch) = self.listener.listen()
            if (volume > self.volumeThreshold or pitch != None): 
                self.detectedVoice(volume, pitch)
        # Update the view
        self.update()

    def detectedVoice(self, volume, pitch):
        """Called whenever volume above the volumeThreshold is detected"""
        pass

    def quit(self):
        """Quits the game"""
        pygame.event.post(pygame.event.Event(QUIT, {}))

    def run(self):
        """Runs the main game loop"""
        self.endLoop = False
        while True:
            self.onClockTick()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight))
                if event.type == QUIT:
                    self.listener.closeStream()
                    self.returnToMenu()
                    self.endLoop = True
                    break
                if event.type == MOUSEBUTTONDOWN:
                    self.buttonsList.update(*event.pos)
            self.clock.tick(self.fps)
            if self.endLoop == True: break

    ############################################################################
    # Model
    ############################################################################
    # The model keeps track of all view-independent elements, such as score, 
    # sprites, and whether the game is over or not

    def initModel(self):
        """Initializes the game model"""
        # Keeps track of game sprites
        self.buttonsList = pygame.sprite.Group()
        self.allSpritesList = pygame.sprite.Group()
        # Sets game attributes
        self.finished = False # Whether the game has finished
        self.score = 0

    ############################################################################
    # View
    ############################################################################
    # The view deals with all the graphical aspects of the game, including 
    # drawing the sprites and the text.

    def initView(self):
        """Initializes the game view"""
        # Initialize the screen
        self.surface = pygame.display.set_mode((self.screenWidth, self.screenHeight), pygame.FULLSCREEN)
        pygame.display.set_caption('Shoot the Enemies')
        # Load the view elements
        self.loadView()
        # Display the view
        self.update()

    def loadView(self):
        """Load initial view elements"""
        self.createMenuButton()

    def createMenuButton(self):
        margin = 20
        (width, height) = (100, 50)
        (x,y) = (self.screenWidth-margin-width, margin)
        color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
        menu = Button(x, y, color=color, width=width, height=height,
                 callback=lambda data: pygame.event.post(pygame.event.Event(QUIT, {})), text="Menu")
        self.buttonsList.add(menu)

    def update(self):
        """Updates the view, by drawing the background, sprites, and score"""
        self.surface.fill((0xFF, 0xFF, 0xFF))
        self.buttonsList.draw(self.surface)
        self.allSpritesList.draw(self.surface)
        pygame.display.update()

game = Game(lambda: 1, 1280, 800)
game.run()
pygame.quit()
sys.exit()